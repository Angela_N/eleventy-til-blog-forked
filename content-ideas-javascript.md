basic javascript notes


## Global variables 
```

var showMeANumber = 3;

function helloThere() {
	var showMeANumber = 5;
	console.log(showMeANumber); // will output 5	
	console.log(window.showMeANumber); // will output 3, because it's from the global slate. 	

}

console.log(showMeANumber) // will output 3 

```

## Two phases

Creation phase - set up memory space

variables. Then functions. 
The JS compile go line by line for variable and functions. 
It will just define the item - not the value. 
So if you create a `var greet = hello` it will assign `greet = undefined`. 

When the JS compiler gets to the line again, it will THEN assign the value. 


Execution phase - run the code

How it looks - It's a call stack.
It will pull code into it as needed, and then quickly eject it. 
It will first search for the local data. If it's not there, it will look in the global space. 

## Call stack

It grabs the function into the code. Then pulls the variables. And after invoking, it kicks it out of the stack.

There's a global memory space as well. This is where functions get stored. 


## call a function from a param. 
## First class functions. Higher order functions. 


```
function greet() {
	console.log("hello world");
}

function welcome(invokeMe){
	invokeMe();
}

welcome(greet); // this will fire off the greet function. 
```


A better example. 
When I invoke outputNewArray, i pass the name of the function that I want to use. 
```

function outputNewArray(arr, func) {
	const output = []; 
	
	for (let i = 0; i < arr.length; i++) {
		output.push(func(arr[i]));	
	}

	return output; 
}

function multiplyByFive(num) {
	return num * 5;
}

const input = [1, 2, 3, 4, 5];
outputNewArray(input, multiplyByFive);  // 5, 10, 15, 20, 25
```

## Callback vs higher order

(CHECK ONLINE FOR ACTUAL DEFINITION)

Higher order function passes a function.

Callback is a function that calls another function. It will be called later. 

## function statements and expressions

```
// statement 
// so these get hoisted to the top.
function hi(){
	console.log("HI");
}

// expression 
// this will not. 

const hey = function(){
	console.log("HEY");
}

```


## Currying 

https://dev.to/heytulsiprasad/what-the-heck-is-currying-anyway-p83


currying is a technique of converting a function that takes multiple arguments into a sequence of functions that each take a single argument. Now whether you're a JavaScript fanboy or did basic algebra in high school that'll totally make sense to you.

```
function add(a) {
  return function (b) {
    return function (c) {
      return a + b + c;
    };
  };
}

console.log(add(1)(2)(3)); // 6
```

More advance way: 
```
const items = [
  { name: "Mango", type: "Fruit" },
  { name: "Tomato", type: "Vegetable" },
  { name: "Strawberry", type: "Fruit" },
  { name: "Potato", type: "Vegetable" },
  { name: "Turnip", type: "Vegetable" },
  { name: "Banana", type: "Fruit" },
  { name: "Carrot", type: "Vegetable" },
];


const isType = obj => type => obj.type === type;

const isFruit = item => isType(item)("Fruit");
const isVegetable = item => isType(item)("Vegetable");

const fruits = items.filter(isFruit);
const vegetables = items.filter(isVegetable);

```

## Set of Objects 

Question 2: A Set of Objects
Consider the following Set of objects spread into a new array. What gets logged?

const mySet = new Set([{ a: 1 }, { a: 1 }]);
const result = [...mySet];
console.log(result);
Answer and Explanation
Answer: [{a: 1}, {a: 1}]

While it’s true a Set object will remove duplicates, the two values we create our Set with are references to different objects in memory, despite having identical key-value pairs. This is the same reason { a: 1 } === { a: 1 } is false.

It should be noted if the set was created using an object variable, say obj = { a: 1 }, new Set([ obj, obj ]) would have only one element, since both elements in the array reference the same object in memory.

https://buttondown.email/devtuts/archive/10-javascript-quiz-questions-and-answers-to/

## Deep object mutability 

Question 3: Deep Object Mutability
Consider the following object representing a user, Joe, and his dog, Buttercup. We use Object.freeze to preserve our object and then attempt to mutate Buttercup’s name. What gets logged?

const user = {
  name: 'Joe',
  age: 25,
  pet: {
    type: 'dog',
    name: 'Buttercup',
  },
};

Object.freeze(user);

user.pet.name = 'Daffodil';

console.log(user.pet.name);
Answer and Explanation
Answer: Daffodil

Object.freeze will perform a shallow freeze on an object, but will not protect deep properties from being mutated. In this example, we would not be able to mutate user.age, but we have no problem mutating user.pet.name. If we feel we need to protect an object from being mutated “all the way down,” we could recursively apply Object.freeze or use an existing “deep freeze” library.
https://buttondown.email/devtuts/archive/10-javascript-quiz-questions-and-answers-to/