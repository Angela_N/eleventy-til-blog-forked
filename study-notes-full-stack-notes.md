How to clip images and make the text wrap around it. 

https://i.imgur.com/9sq8pEP.png



How to provide placeholder text in inputs. 
https://i.imgur.com/jfE5OhR.png
https://i.imgur.com/P5Jb9aF.png

estelle css githubt
https://github.com/estelle/input-masking/tree/master/css


## Full stack thing

echo $0 
-bash

Kernel -> system -> terminal -> gui


TCP -> It sends info. Did you get it? THen it connects. 

UDP -> is a one way blast. It's faster. And the client responds.

### this opens up a manual
man traceroute


traceroute google.com
It's map every single server you move to get to that end point. 

### Vim has 3 modes

Insert mode
command mode
select mode -- shift:

:q!

vi simpleServer.js

```
const http = require('http');

http.createServer(function (req, res) {

    res.write('Hello, World!');
    res.end();
}).listen(8080);

console.log('Serer started! Listening on port 8080');

```

Runnig your own server
https://try.digitalocean.com/frontend-masters/



How to add the ssh bit into your keychain on a server.

ssh-add -K ~/.ssh/fsfe


The step to srver setup

1. update software
2. Create new user. 
3. Make the user a superuser
4. Enable Login
5. Disable root login



1. updating
apt update
apt upgrade

2. create new user
adduser $USERNAME

3. make user a superuser
usermod -aG sudo $USERNAME
su $USERNAME
sudo cat /var/log/auth.log // check sudo access

4. exit

ssh $USERNAME@IP_ADDRESS

give your user acess keys
chmod 664 ~/.ssh/authorized_keys

5. Disable root login

sudo vi /etc/ssh/sshd_config

In the daemon config, look for 

PermitRootLogin yes -> no


## Installing nginx
ngnix does a lot of cool shit under the hood.

sudo apt install nginx
sudo service nginx start

domain -> IP -> nginx -> webpage


you should be able to visit your ip and see a nginx page. 

## Show nginx configuration 
sudo less /etc/nginx/sites-available/default 

Nginx - better to google and read blog posts on it than the docs. 

base directory --> `/var/www/html` 
location block: https://i.imgur.com/jfDZ6cU.png

## Modifying nginx configuration 

sudo vi /var/www/html/index.html -> hello world 

nginx is looking for a index.html. 


## Creating a application server (via Node.js)

node - single threaded js engine for V8 Chrome. 
Most languages, requests blocked. Like Python. If it's grabbing a image in london, it'll wait until it does.

go java rust is faster. But nodejs handles a lot of things. 

Node is asyncronist. this is the event loop. 

Put this stuff on your server
-> sudo apt install nodejs npm 
-> sudo apt install git // just doublechecking

Add version control so you can wrap it up. 

Architecture -> 
difference between junior/senior
juniors can code. 
senior thinks long term. 

### Change ownership to the current user 

sudo chown -R $USER:$USER /var/www //-R recusive

you can echo $USER and see what it says 
You do this so you don't have to keep sudoing the file. 

### Create application directory 

mkdir /var/www/app
 
### move app directory and initialize

cd /var/www/app && git init 

mkdir -p ui/js ui/html ui/css 
touch app.js 
npm init 

npm i express --save
vi app.js 

Then use the starter --> https://expressjs.com/en/starter/hello-world.html

From there, all websites point to port 80. 
But your express app is running on port 3000.

So www.site.com -> port 80 -- the default index.html content
but www.site.com:3000 -> port 3000 -- the actual express stuff. 
 
domain -> ip -> nginx -> express 

`services` is a nice wrapper over `system_ctl`. It's a lot easier and cleaner. 





## Typescript 

REPO: https://github.com/mike-works/typescript-fundamentals
NOTES section is where the magic is. 


* Open-source
* typed, syntactic superset of JS by Microsoft
* It compiles into readable JS
* It's the language, the language server (which is the hinter), and a compiler
* Works seamlesslly with Babel 7 - it takes 

### Power of it: 

* It puts intent IN the code.
If a function is looking for a string... you can force that. 

There's nothing stopping you from putting a comment into a function that says... ONLY STRINGS. Or adding an assertion -- where it checks if it's a string or not. But that leans into defensive programming -- where you don't trust your inputs. You add a overhead - both performance and cognitively, where someone has to look at your code and piece by piece mentally put it together. 


* Catch common mistakes. 
Spelling errors. incomplete refactors. 

* It moves runtime errors to compile time.
It moves errors before you run your code. 

* Provide a great developer experience. 

### Crash Course Overview 


### JSDoc style commenting with typescript 
```
/**
 * The language server is pushing this data for us. 
 * It's inert, a la JS tags
 */
const myName = "Rocky";

```

It pulls the comment right into the tooltip! 
https://i.imgur.com/jQ7InM3.png



### Variable declarations 

// This creates the ANY type. 
let z;
z = 41;
z = "abc"; // (6) oh no! This isn't good


// This forces it to be a number 
let zz: number;
zz = 41;
zz = "abc"; // 🚨 ERROR Type '"abc"' is not assignable to type 'number'.


### tuple 
array with a fixed length. 
For example -- for addresses -- 
```
let address: [number, string, string, number] = [
  123,
  "Fake Street",
  "Nowhere, USA",
  10110
];
```

const xx = [32, 31]; // number[];
const yy: [number, number] = [32, 31];



### defining objects in Typescript 

```
/**
 * (11) object types can be expressed using {} and property names
 */
let cc: { houseNumber: number; streetName: string };
cc = {
  streetName: "Fake Street",
  houseNumber: 123
};
```
```
// BELOW -- will give you a error because it expects houseNumber. 

cc = {
  streetName: "Fake Street",
}

// to fix: add question mark to notate it's optional 
let dd: { houseNumber: number; streetName?: string };

dd = {
  streetName: "Fake Street",
};
```

### Interface 
Interface is just assigning a type to a value. 
He'll come back to it. 


### How type systems work in other places. 

So in places like Java - to verify that the input is correct, it expects the input to be a class instance of a type named HTMLInputElement. 

That's not how Javascript works. It only cares about the shape of the object. 
https://i.imgur.com/A3g2vOT.png


### Wide vs Narrow 
The wide surface area, vs a narrow surface area. 

https://i.imgur.com/QIOw0m8.png

Wider is anything. 
Narrower can fit through a wider hole. 


### Type annotations within a function 
// (1) function arguments and return values can have type annotations
function sendEmail(to: HasEmail): { recipient: string; body: string } {
  return {
    recipient: `${to.name} <${to.email}>`, // Mike <mike@example.com>
    body: "You're pre-qualified for a loan!"
  };
}

// (2) or the arrow-function variant
const sendTextMessage = (
  to: HasPhoneNumber
): { recipient: string; body: string } => {
  return {
    recipient: `${to.name} <${to.phone}>`,
    body: "You're pre-qualified for a loan!"
  };
};


### Function SIgnature Overloading

What is lexical scope - 
Lexical scope is asking -- what is the value of `this`. 


You can set it at the top so it meets this constraints. 



function sendMessage(
  this: HasEmail & HasPhoneNumber,
  preferredMethod: "phone" | "email"
) {
  if (preferredMethod === "email") {
    console.log("sendEmail");
    sendEmail(this);
  } else {
    console.log("sendTextMessage");
    sendTextMessage(this);
  }
}
const c = { name: "Mike", phone: 3215551212, email: "mike@example.com" };

function invokeSoon(cb: () => any, timeout: number) {
  setTimeout(() => cb.call(null), timeout);
}

// 🚨 this is not satisfied
invokeSoon(() => sendMessage("email"), 500);

// ✅ creating a bound function is one solution
const bound = sendMessage.bind(c, "email");
invokeSoon(() => bound(), 500);

// ✅ call/apply works as well
invokeSoon(() => sendMessage.apply(c, ["phone"]), 500);



### Typescript terms that he uses 

* Interface 
* Lazy Initialization 
* ES5 getters 
* Generics 

### How to concert JS -> TS 

A reminder that code is developer intent. 


1.  Compiling in "Loose mode"

Start with tests passing. 
Rename files .js to .ts 

2. Explicit Any 
Start with test passing 
ban any implicit any ( ) 

3. Squash explicit anys, enable strict mode 

Enable strict mode 
replace explicit anys with more appropriate types 
avoid unsafe casts 


### Use Cases of Generics 
Types that explode in complexity is often the result of abuse of Generics 

Question the blog articles you see out there tht a lot of content ets wrong. 

## Typescipt Class

Class -- class is just a factory to produce instances. 
