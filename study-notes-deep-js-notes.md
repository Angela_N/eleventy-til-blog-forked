# NOTES FROM KYLE SIMPSON


## DEEP JAVASCRIPT 

11/22: Get to 5:59... before Advanced Scope 
11/23: Goal -- finish

## INTRODUCTION

* Whenever there's a divergence between what your brain thinks is happening, and what the computer does, that's where bugs enter teh code.

Getify law 17

--------
[00:05:56]
If you ask a room of C++ developers, have you ever read any part of the specification for C++? Most hands will go up, same for Java. But if I ask that same question, and I do, all over the world I ask this question almost every time, how many of you have read any part of the JavaScript spec?

[00:06:12]
And I hardly ever get any hands. And I think part of what I'm getting at here is that many developers in the JavaScript community seem to think that JavaScript with its low barrier to entry, should be so automatically understandable, so intuitive, that if it's not immediately intuitive, therefore it was poorly designed.

[00:06:32]
As if we don't own a responsibility to learn how our tool works. So if you're looking for any sort of overarching theme to what I teach and why I teach and specifically this course, it is that that's not good enough as developers for us to assume something, our assumption to be incorrect.

[00:06:53]
And then when we get bitten by it we shift the blame to someone else. Instead of most developers in other communities saying, well, the fault is my own. I didn't understand it enough, okay? So here, when I experienced it coming back as the number 5 instead of the string 5, I didn't say, stupid JavaScript, how could you be so poorly designed? 

Kyle Simpson - 

https://www.ecma-international.org/ecma-262/9.0/index.html#sec-postfix-increment-operator-runtime-semantics-evaluation

----------------------------
## TYPES

### Primative Types
"Javascript, everything is a object." False. 
The joke is... 'false' is not an object. 

Things can behave like objects. But does not MAKE it an object. 

https://www.ecma-international.org/ecma-262/9.0/index.html#sec-ecmascript-language-types

Specs show that there are primative types. 

Objects -- not objects: https://i.imgur.com/j199VEC.png


### typeof 
undefined - 
	NOT: Doesn't have a value yet.
	MEANS: Does not currently HAVE a value.


### Undefined vs undeclared 

typeof v; // undefined 

But it doesn't exist!

Undeclared should mean it doesn't exist. 
Undefine should represent that there's no value YET. 

The third is -- unitilizaed - aka TDZ -- temporal dead zone

### NaN 
Doesnt' mean Not a Number, but we ought to refer to it as a invalid number. 
NaN == Invalid Number
It IS a Number. Just... not there. 

https://i.imgur.com/FWXTM7e.jpg

The number zero is NOT a way to identify a placeholder for the absence of a number. 

NaN bug: 
	notice in line 6 --> NaN is NOT equal to itself. 
	notice in line 10 --> "Strings ARE NaN". It coerces values into a number. 
	Dont use it! 

	With ES6 -- JS did a better job.
	notice in line 12 & line 13 --> isNaN.

Use Numbers.isNaN for poper checking.


negative zero is a thing. (-0)
You can also use Object.is for checking -0.
https://i.imgur.com/YWI5WRG.png

Use case of -0 vs 0. 
https://i.imgur.com/HliBNM6.png


### Exercise 

You're creating your own polyfill to test if something is 0 or -0.
// TODO: define polyfill for `Object.is(..)`

// first, override the original since it already exists.
if (!Object.is || true) {
	Object.is = function ObjectIs(x, y) {
		var xNegZero = isItNegZero(x);
		var yNegZero  = isItNegZero(y);

		if (xNegZero || yNegZero) {
			return xNegZero && yNegZero; 
		}		
		else if (isItNan(x) && isItNan(y)) {
			return true; 
		}
		else {
			return x === y;
		}
		
		// ***** HOW TODO
		// 1 / Infinity = Infinity;
		// -1 / Infinity = -Infinity; // negative infinity
		// using this, we can build a function.
		
		function isItNegZero(v) {
			return v == 0 && (1/v) == -Infinity; 
		}
		
		// check for NaN 
		function isItNan(v) {
			// NaN doesn't equal itself
			return v !== v; 
		}
	}
}



// tests:
// should all be true
console.log(Object.is(42,42) === true);
console.log(Object.is("foo","foo") === true);
console.log(Object.is(false,false) === true);
console.log(Object.is(null,null) === true);
console.log(Object.is(undefined,undefined) === true);
console.log(Object.is(NaN,NaN) === true);
console.log(Object.is(-0,-0) === true);
console.log(Object.is(0,0) === true);

console.log(Object.is(-0,0) === false);
console.log(Object.is(0,-0) === false);
console.log(Object.is(0,NaN) === false);
console.log(Object.is(NaN,0) === false);
console.log(Object.is(42,"42") === false);
console.log(Object.is("42",42) === false);
console.log(Object.is("foo","bar") === false);
console.log(Object.is(false,true) === false);
console.log(Object.is(null,undefined) === false);
console.log(Object.is(undefined,null) === false);


### Funademantal Objects 

aka Built-in Objects, or Native Functions 


### RANDOM QUOTES/NOTES IN THIS SECTION 

Instead of whine and complain about the bug, you learn it. 

Bugs come from when you use tools and don't know how to use them. 


## Cohesion 

https://www.ecma-international.org/ecma-262/9.0/index.html#sec-abstract-operations
How to deal with type conversion. (aka cohesion)

When you think of cohersion and CONVERSION - it's the same. 

### ToPrimitive 

ToPrimative(hint); 

What does `hint` mean? I would like it to be a number, or string. 

### ToString 
https://www.ecma-international.org/ecma-262/9.0/index.html#sec-tostring

https://i.imgur.com/tmp0XeK.png
If you toString a array -- It sort of stringifies it. 
But empty strings... it confuses things. 


### ToNumber 

Notice the hexidecimal -- it converts it to a number. 

But notice this as well - the golden bits: 
https://i.imgur.com/u26u2nI.png
https://i.imgur.com/ATTrwo5.png

See how it turns it to 0?

Empty string is the root of all Javascript evil. Because there's some times where it returns a zero, but why? Because it can sometimes convert it to a empty string. 

Remember that Javascript has to always 100% work. OLD JS should work with today's JS. 

### ToBoolean

LIST: https://i.imgur.com/mCT84P8.jpg

If it's not on the falsy list... IT IS TRUTHY.

ToBoolean an empty array... what happens? 

It does not invoke the ToPrimative algorithm. https://www.ecma-international.org/ecma-262/9.0/index.html#sec-toprimitive


It just looks up within this table!!!!
LIST: https://i.imgur.com/mCT84P8.jpg

Since it's not on there... it's TRUE. It's a straight look up.


### Coercion -- You're already usnig it. 
You claim to avoid coercision because it's evil and you should avoid it, but...
you're already doing it. 


For example - template literals. 
If you're dropping in values, it's already doing it!
https://i.imgur.com/YXqfQ15.png

Another example: When you're using + or -. 
When you're using msg1 + THING + msg2?
It's calling the ToNumber operation behind the scenes. 

https://www.ecma-international.org/ecma-262/9.0/index.html#sec-addition-operator-plus

It happens automatically right here with this code. 
So you can implicity make it a string. 
https://i.imgur.com/B61dWOI.png


Finally - when you're using Falsy vs Truthy? 
https://i.imgur.com/tSLUFx8.jpg
This is using toBoolean!

If you're in the habit of using one thing and not the other, you're hypo

Be more explicit. 


### Boxing 
https://i.imgur.com/5UyXFmv.png

studentNameElem.value.length -- how is this working? 
studentNameElem.value is a primative string. How are we using built-in object functions on it? 

Because it explicity cohersion right into an object! It's because of Boxing! 

### Corner Cases 

All whitespace is 0, or false. 
https://i.imgur.com/5UyXFmv.png


Using numbers for math -- can cause false. 
https://i.imgur.com/PfZ4NdA.jpg
Which is bad. You want zero, and it's better to search NaN or undefined. 

## Phiosophy of Coercion


### Intentional Coercion 

Adaopt a coding style that makes value types plain and obvious. 

You're making more problems by not doing it. 

Which mean, if you're relying on (===), you're just papering over future problems. 

Design functions that are very clear on what they take in. Function only takes string, or numbers, or whatever. 

We can control what polymorphed. 

And the freedom to chose how you identify it. 


[00:01:17]
When you receive a code review and some junior developer did something dumb, like they didn't do a coercion correct and maybe there's some corner case they didn't know about, you don't reject the code review and say, you're dumb. You say, hey, come sit with me for a minute, let me just talk to you about this corner case that you weren't aware of, and if you did this thing instead we'd avoid that corner case entirely.

[00:01:40]
Now you learned better and they learned better. This is a complete bunk argument, it's a lazy excuse for not wanting to actually do what we ought to do on our developer teams, which is promote that everybody should be learning, everybody should be headed upward. I don't care where you are, I care what your direction is, are you headed upward?

[00:01:56]
Are you learning more about your tool? This is not an excuse for not using the tools well. It is also not an excuse for being clever, right. Some people say, well the slippery slope argument here. I can do all this clever bit-wise math, right. I'm using the tool and I'm being clever, and I'm doing all of my programming on one line of code, the way we used to do in Perl.

[00:03:23]
Yeah, the junior devs gonna need to learn some stuff, but that's what happens in every job and in every industry, you have to learn some stuff. You don't see an architect saying, well we're not gonna design that building well cuz we've got an intern on the job. You teach the intern how to build the building well.

[00:03:40]
That's what we need to be doing. We need to be doing with all parts of JavaScript, but especially the parts that everybody tells you to ignore like types.

### Implicit Coercion

Implicit != Magic 
Implicit != Bad 

They think it's bad. 

Should be considered -- 
Implicit == Abstracted

Abstracted doesn't mean great. Some is bad but most is good - so you can think about code correctly. 

Example: Abstract is fine. The edge cases are minimal. This is acceptable and clear to read.
https://i.imgur.com/QPJ9ggH.png

Example: This implicit can be okay too. 
But lead to some funky edge case things. 

Line 4 is a safeguarding.

Line 8 is fine beacuse the first variable is a number, and the `<` will make the right side a number for comparison too. 
https://i.imgur.com/FC35Q8K.jpg

RULE OF THUMB: 
Is showing the reader the exta type details helpful or distracting?

### Understanding features

If a feature is sometimes useful and sometimes dangerous and if there is a better option then always use the better option" "The Good Parts", Crockford 

KYLE disagrees. 
What does this even mean? It's used... but it's not useful. 

Useful== When a reader is focused on what's important. 
Dangerous === when the reader can't tell what will happen 
Better == When the reader understands the code. 

It is irrespondable to knowing avoid usage of a feature that can improve code readability. 


### Exercise Coercion
Define `isValidName(...)` 
	* 1 param. name
	* Returns true if all the following match. 
		* Must be string 
		* must be non-empty 
		* must contain non-whistespace of at least 3 chars 
		
Define `hoursAttended(...)`
	* 2 params `attended` and `length`
	* either param must only be string/number 
	* both params shold be treated as numbers 
	* both must be 0 or higher 
	* both must be whole numbers 
	* `attended` must be less than or equal to length. 



// FUNCTION 

function isValidName(name) {

	if (typeof name == "string" 
	&& name.trim().length >= 3 ) {
		return true;
	} 
	
	return false; 

}

function hoursAttended(attended, length) {


	// check if string 
	if ( typeof attended == "string"
		&& attended.trim() != "" // check if it's not the number 0
	) {
	
		attended = Number(attended); 
	}

	if ( typeof length == "string"
		&& length.trim() != "" // check if it's not the number 0
	) {	
		length = Number(length); 
	}

	// check if number 
	if ( 
		typeof attended == "number" && 
		typeof length == "number" &&
		attended >= 0 &&
		length >= 0 &&
		Number.isInteger(attended) && 
		Number.isInteger(length) && 
		attended <= length
		) {
			return true
		}
		
	return false; 
}


// TODO: write the validation functions



// tests:
console.log(isValidName("Frank") === true);
console.log(hoursAttended(6,10) === true);
console.log(hoursAttended(6,"10") === true);
console.log(hoursAttended("6",10) === true);
console.log(hoursAttended("6","10") === true);

console.log(isValidName(false) === false);
console.log(isValidName(null) === false);
console.log(isValidName(undefined) === false);
console.log(isValidName("") === false);
console.log(isValidName("  \t\n") === false);
console.log(isValidName("X") === false);
console.log(hoursAttended("",6) === false);
console.log(hoursAttended(6,"") === false);
console.log(hoursAttended("","") === false);
console.log(hoursAttended("foo",6) === false);
console.log(hoursAttended(6,"foo") === false);
console.log(hoursAttended("foo","bar") === false);
console.log(hoursAttended(null,null) === false);
console.log(hoursAttended(null,undefined) === false);
console.log(hoursAttended(undefined,null) === false);
console.log(hoursAttended(undefined,undefined) === false);
console.log(hoursAttended(false,false) === false);
console.log(hoursAttended(false,true) === false);
console.log(hoursAttended(true,false) === false);
console.log(hoursAttended(true,true) === false);
console.log(hoursAttended(10,6) === false);
console.log(hoursAttended(10,"6") === false);
console.log(hoursAttended("10",6) === false);
console.log(hoursAttended("10","6") === false);
console.log(hoursAttended(6,10.1) === false);
console.log(hoursAttended(6.1,10) === false);
console.log(hoursAttended(6,"10.1") === false);
console.log(hoursAttended("6.1",10) === false);
console.log(hoursAttended("6.1","10.1") === false);

## Equality Check 

It's important to think LIKE Javascript

Common Thoughts
== check value (loose)
=== checks value and type (strick)


7.2.14 Abstract Quality Comparison 
https://www.ecma-international.org/ecma-262/9.0/index.html#sec-abstract-equality-comparison


The main difference between the two - the difference is if they allow cohersion. 
THIS: https://i.imgur.com/ucS950J.jpg

https://i.imgur.com/cxHy4Mo.png
The argument that Javascript doesn't compare the value inside objects. Most languages don't! When comparing objects, it's just checking if it's referencing the same point in memory. 


So the bigger question is -- when using double/triple equals... 
Do I want to allow coercision or not? 


[00:00:38]
In other words, you need to be critical, analytical thinkers, and look at your code. And decide what you can know about your code and what you can't. And essentially the decision about double equals and triple equals is a trailing indicator of whether you actually understand your program. Let me say that again.

[00:00:58]
Your choice to use triple equals, it's not just because some guy wrote in a book that you should always use triple equals. Your actual choice to use triple equals is a trailing indicator which points back to the fact that you don't know something about the types in that comparison and so you have to protect yourself.


### It's clearer to use null == undefined
if (workshop1.topic == null && workshop2.topic == null) it clearer. 

### == vs ===
Why not let this happen?
https://i.imgur.com/I9MxUng.png

### Double Equals walkthrough
ONLY primatives
https://i.imgur.com/NoTcMlS.png
Even if this works... doesn't mean
Don't make comparisons with numbers and arrays of numbers.
The most important takeaway is
the fix is not to use triple equals. Triple equals hides the fact that you're making a terrible comparison in the first place. What you should do is fix it so your comparisons make sense.
The example is that people are making contrive comparisons and blaming double equals. When double equals is not at fault. Triple equals only covers it up.

### == Summary
if the types are the same: ===
if null or undefined: equal
if non-primatives: ToPrimative
Prefer: toNumber

### edge cases
[] == ![] // WAT?
First, not a real use case.
Anyways...
This is how we see it:
https://i.imgur.com/95zdgYV.png
What's happening under the hood.
https://i.imgur.com/X9kqnlF.png

### A case for ==
== and === can co-exist in your code.
Only use the == if you know the type. Make it a smaller surface for comparison.
if you KNOW the Type already --
go with ==.
If you know the type is different already... then === is already going to fail!
Now... if you DONT KNOW the type in a comparison... then you fully don't understand that code. The best is to refactor it to make it more clearer.
THe most obvious signal for === is to let the reader know that you DONT know the type.
And even if === would always be equivaent to == in your code, using it everywhere sends a wron semantic signal: "Protecting myself since I on't know/trust the types".


## Typescript, Flow, and Type-aware linting 

The problem that these solve is that they're reducing the surface of comparison. 

what they are doing: 
1. catch type-related mistakes 
2. communicate type intent 
3. provide IDE. By chgoosing to opt into the system. 


Caveats: 

1. inference is best-guess, not a gauranteee. 
2. annotations are optional. Devs need to identify it. 
3. Any part of the program that isn't typed... is still unknown. 

Infering means that based on the value, JS determines the type. 

`var teacher = "Kyle"` infers that this is type string.
`var teacher: string = "Kyle"` is the typescript version. 

### Validating operand types 

It doesn't let you check the operations. 

So you can't subtract a string from a number. 

### Typescript Cons
You can also use code comments to identify the code. 

You have to use a non-JS syntax.
JS doesn't have a future of doing stronger types. 

The syntax explodes in complexity - ramps up the barrier to entry.l

They focus on "static types", than vlaue types .


### Summary 
The problem with === is that by pretending it saves you from needing to know types, which then systemically perpetuates bugs. 

You cannot know the types involved in the operations. 

----------------------------
## SCOPE

Nested Scope
Hoisting
Closure
Modules 

Scope is where to look for things. 

Javascript is NOT a interpret language, it's a compiled language. 
It does not go line-by-line, even though it acts that way. 
It's not... compiled... it's parse. There's a processing step. 


HOW TO EXPLAIN?
How does javascript know there's an error on like 10, unless it processed line 1-9? It already knows that. 

In a true scripting language like BASH, it fires line 1-9, then breaks. 

Marble Sorting - the identifiers in the right places through the 'parsing' stage, the processing step. 

Some say compiled is like Java. It parses and turns it into bytecode. 
Javascript is the same. Parses and hands it off to the JS virtual machine. 

It's a two-pass system -- 2 steps: 

Step 1 - Compile time.

Scope manager: All the scopes are determined at compile time. 
It's declaration code. It's statically storing it. 

When it was creating the abstract tree...
target/source. 
LABEL and VALUE. 

It's creating the blueprint -- the plan.


Things are being assigned in memory.


Step 2 -- Execution. 
Then run time. Nothing changes. It allows it to be more efficient. 
The virtual machine. 

It now goes through each action. And it goes back and ask the scopes if they exist. Your function... Hey global scope, you there? YES? Cool... it grabs the value (or the function within the value) and fires it off. 

Then, it goes through the plan to execute it all. 
When it's go time... it puts it in the stack. 



### Dynamic global values 

This is where shit gets real dangerous. 
https://i.imgur.com/RUZzD1q.png

1. So we know that `teacher` is global. 

2. In the function, `teacher` is called. Since it doesn't exist in the function's execution context, it goes up to the next parent. Which is global. 

3. `teacher` does exist. So it takes that value. 

4. What about `topic`? Doesn't exist in this function's execution context. So it goes up. BUT WAIT, it doesn't exist in global either? 

5. Ugly part of Javascript... it AUTO-CREATES IT. Where? In GLOBAL. 
This doesn't happen in strict mode. 

6. But now it's asking... what does line 11 result in? `teacher == suzy` 

7. What about line 12? `topic == react`. 

"use strict" -- will give you a Reference error.

ERRORS: 
Type Error: Execute a function, access a property on a undefined null...
Reference Error: It says... I can't find the value! 

If you're using a transpiler like babel, they always put "strict mode" in there. Running vanilla JS directly from file to browser will not do that. 

undefined vs undeclared
undefined - it exists, but not defined. memory is allocated. It used to have value, or not. 
undeclared - no memory allocated. Never formally declared. 

### Function expressions and Function Declarations in scarp

Function Declaration = it's in the current scope. 
`function stuff() { }`

Function expression = Put their marble in their own scope. Also, things inside is read-only. 
`var stuff = function innerStuff() { }`

### Using a named function expression 
100% ALWAYS WANT IT -- 

1. The name creates a reliable function self-reference (recusion) 
2. More debuggable stack traces. Even in minified 
3. More self-documenting code. 

QUOTE: The purpose of code is to communicate more clearly your intent. 

https://i.imgur.com/e0lAxYw.png
Kyle's opinion and rules: 
1. function Names should explain what's happening, not reading the body the code. 

So looking at line 1 - it's not super clear. While line 2, the function works. 
Same with the other.  

More consise code does not make more readable code. 


### Advance Scope 
Lexical scope - The decision I made at author time. Fixed at author time. 
It's decided at Compile time. Not run time. 

Bash is dynamically scoped, not lexical scope. 
Dynamic code is it's decided on compile time based on what the compiel found. 


Principle of Lease exposure/lease prilent  -- Defensive posture. 

### IIFE

Immediately invoked function 
What he's trying to solve -- 
There's a collision where he wants to use teacher, but it was already declared somewhere else. So maybe if he scoped it by wrapping it in a function - he can then return that value without issues... right? 
https://i.imgur.com/ryw3WCk.png
`( teacher )();` is a self calling function. 
Why not just avoid pointing to the funciton and jsut put the whole funciton it it? (Like below)
https://i.imgur.com/27QiqCo.png
IIFE then just fires. 
### Block Scope 
Blocks only generate their own scope when you use let/const.
{ 
    let teacher = "Suzy"; 
    console.log(teacher);
}

Block scoping --- 
why not do it like other languages?
https://i.imgur.com/WgMIbka.png
NOTES: questionable. 
Using explcit let blocks to clearly identify that these let blocks ONLY exist within this function. 

### const 
what people think it means -- 
a variable that doesn't change. 
WHat const actually means -- a variable that cannot be reassigned. 
You can mutate the content inside a array with const. You just can't change the array to an object.
StackOverflow has a lot of content on const issues (?). 
KYLE RECOMMENDATION: 
Recommendation - use const for immutable things.
Const - strings, numbers, and boolean. 
Uses a shallow freeze on the object to make the object immutable. 

### Hoisting 
The JS engine does not actually do it. Hoisting is made up, to discuss lexical scope. 
Example: 
https://i.imgur.com/lVIucB1.png
Hoisting is shorthand metaphor to describe something.
TDZ error -- temporal dead zone error. 
TDZ is for const. Because you aren't supposed to get a value for const until you actually define it.
13.3.1 -- in the spec
During stage 1 - compile time...
Var says create a teacher variable, but make it undefined.
When let does it, create a location called teacher, but don't uninitilize. 
Stage 2 - run time is when things get ASSIGNED. 
So stage 1 defines the variable, and then it assigns the value. 

### Closure 
Doug Crawford - it takes a full generation before the idea catches on. Essentially, the old guard had to die. For closure, it must be really great... it took two generations!
What is closure? 
Closure when a function is able to "remember" the lexical scope even when the function is executed outside that lexical scope. It's "the backpack". The preveration is retained... that's closure! 
Assume closure is a scoped-based function. So if you have a function with a huge data... it sits in memory. 
Don't think of closure as CAPTURING VALUES. It's preserving variables. It is Preservation of the linkage to the vairable. 
Example: 
https://i.imgur.com/PdNQ6Au.png
This is not capturing the value, otherwise the result is 1, 2, 3. 
Instead it's 4 4 4. Because it's preserving the variable created. 
If you change it up like this--
https://i.imgur.com/YabZW5x.png
it'll work. Because again, it's not capturing the value... it's a variable. 

### Module Pattern
It requires encapsulation. 
Encapsulation - hides data. 
The idea of a module - there's a public api, and things you cannot touch. 
Module Encapsulate data and behavior (methods) together. The state (data) of a module is held by its methods via closure.
https://i.imgur.com/9DdZrtr.png
For node: you have to use `.mjs`, because node and npm had a bunch of problems with the TC39. 
Modules create your own file. 
It's a singleton -- it gets cached and you can call it a bunch of times. 
https://i.imgur.com/AVya4XW.png



-------------------
## OBJECTS (oriented) -- not object oriented.

* this 
* class {}
* Prototypes
* "Inheritance" vs "behavior Delegation" (00 vs OLOO) (objects linked to other obejcts)

### this 

A function's this reference the execution context for that call, determined enteirely by HOW the function was called. 

So how it was called is what matters. It's Javascripts version of dynamic scope. 

Example: https://i.imgur.com/RnAqXQu.png

There's 4 different ways to invoke a function. 

METHOD 1 - Implicit Binding 

A namespace pattern. 
https://i.imgur.com/deyoXNk.png
The ask function is being called within the namespace.

METHOD 2 - Dynamic Binding -> Sharing 
The function is passed to these objects. And now they're being called. This time, it's binded to the object. 
https://i.imgur.com/WuxX4vo.png

METHOD 2b - Explicit Binding -- the .call method. 
This is calling the binding 

https://i.imgur.com/SSeIE6A.png


METHOD 2c - Hard binding -- so this is a subset of number 3. 

Look at line 8 - it lost it's binding. 
Think of `workshop.ask` looking like cb(). It's not calling the workshop.ask anymore, it's pointing to setTimeout. This is pointing to the global state. 

Where line 11 reconnects the binding to 'this', it says always point to the value here.
https://i.imgur.com/nadnG1N.png

If you want flexible dynamicism, you `this`. 
If you want predictability, use closures, use lexical scope. 

### new Keyword 

METHOD 3 - It's the third way to invoke a function. 

It's goal is to invoke a new function, pointing to a whole new object. 

How does it do it?

1. Create a brand new empty object 
2. It LINKS the object to another object. 
3. It calls the function with this set to the new object. 
4. The function does not return an object, it assumes return on this. 


### Fallback default binding 

METHOD 4 - default binding 

Look at line 12 -- 
It fires and looks in global. 
https://i.imgur.com/MUXWAO3.png

Line 15 -- in strict mode, it tries to access undefined. 
It wants to let you know that you created a this that is pointing to global!

`this` cannot look at the function. 
You have to look at HOW it was called. 

If you ever need to know what `this` is pointing to...here's the method to determine

1. Is the function called with `new`? Then it's 'new'.
2. Is the function called by call() or apply()? (`bind()` uses `apply()`)
3. Is the function called on a context object? (Like workshop.ask?)
4. Then it goes into default Global object, except in strict mode. 


### What about lexical this and arrow functions

https://i.imgur.com/MUXWAO3.png

The mental modal that the arrow function is a hardbound function to the parents this. 
An arrow function does NOT define a 'this' keyword at all. It will lexically resolve it. It will go up one scope. 

If you have 5 nestled arrow functions with this... it will go up and up and up until you find it. 
https://www.ecma-international.org/ecma-262/9.0/index.html#sec-arrow-function-definitions-runtime-semantics-evaluation



### Misconception about arrow function 

https://i.imgur.com/A9lZOFU.png

This code -- why isn't my arrow function getting teacher? 
Don't get confused that brackets {} automaticlaly create scope. 
Because objects don't have a scope! `this` is pointing to global. 
Object Properties aren't scoped at all... it's a member of a identifier. There is no scope within a object. 

`this` never points to `this` -- do not use `var self = this`. 
`this` is supposed to point to the parent. 


### ES6 class keyword 

class don't have to be statements, they can be expressions. 
It's syntatic sugar and benefits. They're going to get their own features.

https://i.imgur.com/Gqfu5hy.png

You can also use extend which gives you methods from the class. 
super lets you refer to the parent's values! 



### Prototypes 

Class is syntatic sugar over prototypes. So let's see how it works in use. 

It doesn't work like C++ and Java. 

The class system fundamentally COPIES. 

CS 101 -- 
Class is the blueprint, and the instant is using the blueprint to build something. 

The Class Design pattern -- it COPIES into the instance of the class. Instatiating...
You draw the buildprint of the building. 
If you delete a wall for the blueprint, does it affect the building?
If you add a window, will a new window magically appear in the buiilding?
All of this stuff only exists on Instantiation! 
Hence -- it's a copy of the class. 


Inheritance is also a copy -- generics metaphor. Kyle's son has his DNA. 
Kyle's son -- if his leg breaks, Kyle's leg doesn't break.

They will flatten the instance for performance - just copy it! 

How it looks: https://i.imgur.com/gwWKk4P.jpg

__prototype__ -- shortcut is double underline prototype, or dunderproto. 

### Prototypal Inheritance 

This is classical C++ Java -- 
https://i.imgur.com/r4bRyDX.png

This is how JS Prototype Inheritance
https://i.imgur.com/ymKufCl.png

This is the Javascript "Behavior Delegation".
It's system is not a class sytem, it's a delegation system. 

There's a class system design pattern, an a delegation system pattern. 


Object Link Other Object -- https://i.imgur.com/gy0Hmvk.jpg 

### Delegation: Design pattern 



-----------------------------------

### REVIEW OF THIS
Kyle Simpson is very...
Neil Degrass Tyson - "Well Actually..."

------------------------------

## Javascript: The Recent Parts

### Introduction 

#### History of JS

IE6 refused to evolve so for a few years, it didn't get any updates. 

2009-2015 - Javascript didn't change and then suddenly it changed.
Not 6 years, but 16 years. 

The healthiest thing for JS is to move away from versioning, and make tiny increments. So it went from ES5, ES6, to ES2016... ES2020. 

There are 5 stages. If it makes it to stage 4 of the ECMAscript community, then it's in. Proposals come in independently. 

Backward compatiability, once it's in... it's in. They don't go back because it can break websites. 

#### Declartive language 

More declartive code explains better. Communicative way. 

It's about doing 'the why'. 

There's degrees of imperitive vs Declarative. 

#### Browser Support & Transpilers

Transpiler - compiles an older version 
Polyfill - converts new shit to old shit. 

### Course Overview

ES6, ES2015...2019

Features
* templtate stringsstring padding/trimming
* destructring
* Array find()/include
* Array flat/flatMap
* Iterators, Generators
* RegEx Improvements
* async... await
* asyc*... yield await

### Strings 

Template Strings -> Interpolation 


Tag functions - 
Example - you have a variable 3.12 but want it in dollar signs and in your template literal. 

It will iterpolate - 
https://i.imgur.com/i5e5Ksk.png
A more advanced form of template literals are tagged templates.

```
let person = 'Mike';
let age = 28;

function myTag(strings, personExp, ageExp) {
  let str0 = strings[0]; // "That "
  let str1 = strings[1]; // " is a "

  // There is technically a string after
  // the final expression (in our example),
  // but it is empty (""), so disregard.
  // let str2 = strings[2];

  let ageStr;
  if (ageExp > 99){
    ageStr = 'centenarian';
  } else {
    ageStr = 'youngster';
  }

  // We can even return a string built using a template literal
  return `${str0}${personExp}${str1}${ageStr}`;
}

let output = myTag`That ${ person } is a ${ age }`;

console.log(output);
// That Mike is a youngster
```

Template tags are powerful because it doesn't have to return a string. It returns the code. So you can literally build your own language inside of it. Someone made JSX interpreter. 

### Padding and Trimming 

String.Prototype - 

padding - 
trimming - 

left pad and right pad are inaccurate because internationally - some languages are RTL. 
So instead, it's more accurate to say beginning, and end. 

https://i.imgur.com/LhVzWs1.png
```
var str = "Hello"

str.padStart(5); // Hello 
str.padStart(8); // "   Hello"
str.padEnd(8); // "Hello   "

str.padStart(8, "12345"); // "123Hello"
str.padEnd(8, "12345"); // "Hello123"


Trim Method -- 
str.trimStart(); 
str.trimEnd(); 

-----------------
### Array Destructuring 

Decomposing a structure into its individual parts 


Code like this takes a lot of mental process. 
https://i.imgur.com/cqJVbwv.png

This is destructuring - https://i.imgur.com/fRCVkqp.png

This is not a Array! This is a pattern -- a pattern we are expecting to get from the element. 

It's describing the JS Declarative

* Go make ma variable called firstName 
* In the email, it says get me that firstEmail, and if it doesn't exist... default it to this other one. 

### Destructuring Pattern 
```
// Default 

function data() {
	return [1, , 3, 4, 5];
}

/* ORIGINAL WAY
var tmp = data(); 
var first = tmp[0];
var second = tmp[1];
var third = tmp[2];
var fourth = tmp.slice(3);
*/


var [
	first, 
  second = "two", //defaults
  third, 
  ... fourth 
] = data(); 

console.log("second", second)
console.log("fourth", fourth)
```

NOTES: 
1. Destructuring is about the assignment, not about declaring. 
So you could 
`var first, second, third, forth` if you want. 

2. Also notice that `tmp` is not included in the second one. 
You can have it pass data to a temp. 

`var [] = tmp = data();`

3. You can add a default if the value doesn't exist. Notice `two` as a return value. 

4. You can also pass very specific array indexes here. 
https://i.imgur.com/vONFl7t.png


#### Comma Separation 

```
// Default 

function data() {
	return [1, , 3, 4, 5];
}

var temp = data(); 
var first = temp[0];
/* var second = temp[1]; */
var third = temp[2];
var fourth = temp.slice(3);

var newTemp; 

var [
	first, 
	, /*   second,  */
  third, 
  ...forth ] = newTemp = data(); 

/* console.log("second", second)
console.log("fourth", fourth) */

console.log("temp", temp)
console.log("NewTemp", newTemp)
```

You can use commas to create 'space'. 

#### Swapping 
var x = 10; 
var y = 20; 

{
  let temp = x; 
  x = y; 
  y = temp; 
}

// DESTRUCTURED VERSION
var newX = 10; 
var newY = 20; 
[y, x] = [x, y];

#### Fallbacks of failure

So what if data() doesn't return an array, but something else? 
It'll have a type error. 

Do a fallback: 
`var temp = data() || [];`


### Object Destructuring

```
// Default 
function data() {
  return { a: 1, b: 2, c: 3} ;
}


var temp = data(); 
var first = temp.a;
var second = temp.b; 
var third = temp.c; 

console.log({first})
console.log({second})
console.log({third})

var newTemp = data(); 

// PATTERN IS --> source:target
var {
	a: newFirst, 
  b: newSecond, 
  c: newThird
} = newTemp; 


console.log({newFirst})
console.log({newSecond})
console.log({newThird})
```

NOTES: 
1. Pattern is the original source, and the target.

2. The order doesn't matter 

3. You can also set up defaults the same way. (source, target, default)
Why is it using source/target? 
The reason why they do it -- 

```
var object = {
	prop: value, 
	target: source
}

When you're trying to destructuring: 
var {
	source: target
} = object;

Why? Because the prop has always been on the left. So when you're destructuring, it's passing it to target. 
```

4. You can also pass a OR option as a fall back. 

```
{
	b: second, 
	a: first
} = data() || {}; 
```

5. If the target and name is the same -- you can just use this

```
{
	a, // the same as a: a, 
	b  // the same as b: b
}
```

6. ALWAYS declare a fallback. `{ //destructuring } = {}`

#### OBJECT ASSIGNMENT DESTRUCTURING 

If you do this: 

```
var first, second; 

{
	b: second, 
	a: first
} = data(); 
```

Javascript will think this is a block. Which is not accurate. 
YOU NEED TO WRAP IT IN PARENS.

```
({
	b: second, 
	a: first
} = data()); 
```

To avoid all of that - you can pass it cleaner this way. 
https://i.imgur.com/OIvu4zz.png


Parameters: 
```
// ONE WAY
function data(tmp = {}) {

	var {
  a, 
  b
  } = temp; 
  
}

// A CLEANER WAY
function data({a, b} = {})
```


You can also target multiple locations within object destructuring!
https://i.imgur.com/ba46so2.png


### Named Arguments 

It's a pattern called Named Arguments with destructuring. 

The problem with this: 

`function lookRecord(store = "person-records", id = -1) { ... }`

You'll have to remember the order. 

`function lookupRecord({ STUFF }) { ... }`

If it has 3 or more inputs, it's better to use Named Arguments. 


#### Destructuring and Restructuring

https://i.imgur.com/I4Yv6YZ.png
So this is using the `_.extend()` which is part of the underscore library. 

It's putting in the defaults over the settings. 


### Array Methods 

array .find(...)

https://i.imgur.com/iDjLq3O.png
You can add a callback and gives you a true/false. 
The find method cannot give you a if they FOUND the value, or if the value contains 'undefined', which is perplexing.

Array .findIndex(..)
This one gives you a better one so it checks if it exists. If not, it'll give you -1 if it doesn't exist. 

Array .includes(...)
https://i.imgur.com/zu0a1qj.png

It will give you a better clearer picture. 

Array .flat (...)
https://i.imgur.com/S8rLuo0.png

There's different levels of flattening. 
Notice in line 10 - it flattens EVERYTHING. 

Array .flatMap (...) 
https://i.imgur.com/TOpWg80.png

Line 1 - it maps the function. But notice it gives you a three arrays.
Line 6 - it maps your function, and then chains a flat, which flattens everything out.
Line 11 - it flats and maps. 

You can also do a conditional within it!
So you can check for like... odd numbers!
https://i.imgur.com/jwcn3BG.png

So if you get a blank array, it gets removed. Which is freakin' dope. 


### Iterators & Generators 

Generators give you a Declarative way to iterate 

A data source to consume it one at a time...
You use a iterator. You construct a controller that controls the data source. Then call `.next` over and over again. 

https://i.imgur.com/b9aCCq5.png
Notice - it's contructing a `Symbol`. 

It gives a iterator result. 
It gives you a `value` and a `done`. 
If the value gives you undefined, then it's done: true. 

1. ALL DATA POINTS ARE NOW ITERATOR. 
2. Iterator can only go forwards. 

For of loop takes iterables!
https://i.imgur.com/s1Ak794.png

Check it out -- it goes one, and then moves forward, then goes two.

The ... operator also consumes iterators!

```
var str = "Hello"; 

var letters = [...str];
letters;

```

It's a first-class protocol. 


#### What about data taht doesn't have iterators

Objects don't have iterators. 

This is a good code for this: 
https://i.imgur.com/vGPOH0p.png
But note - it's a very imperative way to do this. 

We would prefer a declarative way to code. 
Notice the `generator`, `yield`. 
https://i.imgur.com/ZjGzHb2.png

It's considered a bad practice to RETURN a value in a generator. 

This is a better way to create Objects with Iterators. 
https://i.imgur.com/Tnd0bSe.png

USING a iterator

1. It's common to put it in as a method within the object. 
2. In the console.log, notice that the method is being called with a parameter. 

```
var numbers = {

	*[Symbol.iterator]({
  	start = 0, 
    end = 100,
    step = 1
  } = {}) {
  	for (let i = start; i <= end; i += step) {
    	yield i;
    }
  }  
}


// print 6...30 by 4 steps
console.log(`My lucky numbers are: ${
    [...numbers[Symbol.iterator]({
    start: 6, 
    end: 30, 
    step: 4
    })]
 	}`);

```

### Regular Expressions 

https://i.imgur.com/MOWSjRP.png
When I have a assertion, I only want to match things 

assertion - 
^ (caret sign) beginning of string anchor 
$ end of string anchor

Line 3 - with this assertion - it's pulling two items. 

Line 6 - by adding a $, it wants only the last thing.

Look aheads --
Look behinds -- has been hacked into it. 
https://i.imgur.com/gv1F8ve.png
But now it's a added feature. 

(...) -

### Async Await 

Promise chains (.then chains) suck.

// BEFORE 
sync-async with generators. 
https://i.imgur.com/3OQrdhC.jpg
co, koa, bluebird is a lot cleaner. 

yield until the promises have finished. 

People started move to from promise chains to async await fied. 

// NOW 
This is a lot cleaner. No need to have another library. 
https://i.imgur.com/j1x7pk5.jpg

### Async iterator 

for, map, foreach -- Syncronis iterators -- They don't know to stop. 

### Async PROBLEMS

1. await Only Promises 

It only works 

2. Scheduling - starvation - 

A promise can accidentally keep adding things to micro-task queue. 


3. External Cancelation - 

You cannot cancel. 

### Wrap up 

--------------------------------------------------------------------------------
### Random shit 

#### JSDOCs style for code commenting. 
https://jsdoc.app/about-getting-started.html

JSDoc 3 is an API documentation generator for JavaScript, similar to Javadoc or phpDocumentor. You add documentation comments directly to your source code, right alongside the code itself. The JSDoc tool will scan your source code and generate an HTML documentation website for you.

/**
 * Represents a book.
 * @constructor
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 */
function Book(title, author) {
}

#### Factory Function - 

is something that is like a blueprint and then when you use it, you create it. 

#### imperative way vs  Declarative way - 
“Imperative programming is like how you do something, and declarative programming is more like what you do.” 

#### tuple - two element array 

#### what is a thunk - ? 

#### javascript generators -- ???

#### javascript yield 

#### The event loop - 

micro-queue - 

#### what is a runner? 


#### https://en.wikipedia.org/wiki/Communicating_sequential_processes


### What is a callback

https://i.imgur.com/dmYbCjW.png

getPerson(function onPerson(person) {
	return renderPerson(person);
});

You pass a argument, that gets sent to another function, that's then send to another funciton. 

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Functional Programming (light) -- V3 
https://frontendmasters.com/courses/functional-javascript-v3/functional-programming-journey/

https://github.com/getify/Functional-Light-JS

### Introduction

Light, not deep. 
It's not top down approach. Less math.
It's more bottom up. 

No weird terms.
Monad. 

The curse of the monad - is that once you understand it, you can't teach it. ~Doug Crawford. 

The prolem with current Functional Programming is that it starts simple, and then as you get deeper, it starts throwing in random assumptions of a deep math background that mkaes it difficult. 

Imperative - HOW DO TO SOMETHING. 
Declarative - DEFINING THE OUTCOME - HOW IT DOES SOMETHING. The What.

For example: using For loops. 
The future reader has to read ALL the code in orderstand what it means. They have to infer from the code what it does. 

It forces the reader to do something that they're not naturally gifted to do it. If they have to execute it in their head, that's where the problem. 

Code comments: Why are you iterating by one? Focus on why. 

Imperative vs Declarative - it's perspective!

Someone who codes in bytecode, will go... WOW, your forloops is so Declarative!


BASICS OF FUNCTIONAL PROGRAMMING
closure:  (map and reduce are options)
But you cannot do functional programming without closure. 

Immutability is very important to functional programming. Managing state and state machines is a key tool is functional programming. 

Observables. 
FS libraries. 



------
Functional Programming is mostly math. 
You never had to formuly prove that 1+1=2. There's a mathematically formula to get to this. But we never had to question it. We can just trust it. 

Funtional programming is so compelling because: 
If you had 10,000 lines of code...
and 9900 code is proven mathematical formulas, 
and 100 lines of code is business logic... you'll be much happier about it! 



### Functional Purity


#### Functions vs procedures 

Terminology: 
Procedures don't have a `return` statement.
Functions only call functions. 
If functions that call procedures if it is a procedure. 
A function is a relationship between the input and the output. 

Things that look like functions and functional APIs that aren't functions. 

This is not functional programming https://i.imgur.com/IUWkSXl.png

A tuple -- it's 2-elemenet array. [1, 2]
```
function tuple(x, y) {
	return [x + 1, y - 1];
}

var [a, b] = tuple(... [5, 10]);

a; // 6
b; // 11
```

It matters if the relationship between the input and the output.
The goal is create a relationship between the data. 

"Function the semantic relationship[ beween input and computed output."

```
function shippingRate(size, weight, speed) {
	return ((size + 1) * weight) + speed; 
}
```

#### What are side effects

https://i.imgur.com/BLSTMwy.png
It has a input, output.. and it accomplished our end goal! 
This code doesn't have a return. It's not a function.
Unfortunately, this is creating side effects.

If anything is indirect, it can create a side effect. 

Instead, make a function WITH params. 

You need PROVEABILITY. 
That it's doing the thing that you want to tdo. 
It's direct import, direct output.  


Things that create sideeffects:
* I/O (console, messing with files, etc)
* Database Storage
* Network Calls
* DOM
* Timestamps
* Random Numbers

It's impossible to avoid side effects. It's to minimize side effects.
Because your function creates -- 
CPU Head/CPU Time delay. 

Be intentional when you get side effects. 
Make them obvious! 
If you organize it well enough -- if there is a bug, it's often fro mone of those side effects. 

#### Pure Functions 
Input, output... no side effects. 

Example: https://i.imgur.com/7uUa9zN.png

It's about Functional of purity --> it means a level of Confidence. How confident are we at this function? 

It's not binary yes or no. It's "I have a high degree of confidence" or a low.

#### OPTIONS TO FIX IT

1. Write it pure from the beginning.

2. Refactor to be pure. 

3. Extract impurities so it lives in the outer scope. 

This is a IMPURE function. 
https://i.imgur.com/eJE2zYC.png

This is it refactored. 
https://i.imgur.com/Q9sWpqh.png

It doesn't eliminate the impurity. It extracts it into pure functions, and leaves the impure stuff outside in the outer function. 

4. Wrapping values in it's own scope, so not to bring it into the global scope. 

5. An 'adapter' function. 
In a function which will create a side effect...
CLONE the state. Capture the new changed state. 
Then return the new state as the original state. 
It's essentially taking the original function, modifying it so it's a bit more pure... then eexports it. 
https://i.imgur.com/5kx5Ypq.png

Look at `getStudentsByName()`
It's taking the global `students`, slices it into a local scope students. Then fires the other function. 

--------
### Argument Adapters

parameter -- x and y
argument -- is the value. 

function thing(x, y) <--- parameter
thing("joe", "sarah"); <-- argument 


#### Higher order function 

tl;dr: 
If it receives OR returns functions. 
So if params are functions. 

If it doesn't receive functions -- it's called a single order function. 

If you have 2 functions that don't fit (like lego), the functional programmer goes, "I have to make a higher order function to reshape things" You make a shape adjusted adapter. 


https://i.imgur.com/oovmjrT.png
Let's break this down. 
1. line 1 - `unary()` accepts a single param. 
2. line 13 - `f()` is a function, that all it does is return the arguments. 
3. line 17 - you pass `unary(f)` to a variable `g`. 
4. line 20 - `g(1, 2, 3, 4)` gets called. `g` is calling the f function, and passes all the parms in. Unary then fires, returning a function `one();` Remember the rules of scope? fn is in the scope. So it fires and returns the value. 


Another adapter function -- 
This is a flipped adapt function. 
https://i.imgur.com/d0Alnlz.png

Seeing flip, reverse, it's like seeing + and minus. 
It flips the arguments. 

I want to look for the lego piece that works. 

Why shouldn't you create a bunch of ad-hoc utilities? 
You want to use the known common functional programming. They want to see the same lego everywhere. recognizable. Use the standard ones. 
Get real familiar of Ramda... you'll find a bunch of patterns are the same.

### Point Free Definitions 

CLOSURE: 
Make a function with another function. 
https://i.imgur.com/dmYbCjW.png

```
getPerson(function onPerson(person) {
	return renderPerson(person);
});
```
You pass a argument, that gets sent to another function, that's then send to another funciton. 

But it's the same shape. Why not just do this?
```
getPerson(renderPerson); 
```

This is called Equational Reasoning. If they have the same shape, they're interchangable. 
The onPerson function is the same as renderPerson. 

https://i.imgur.com/DPq3qAy.png
Notice the `isEven(v)` 

You could have return v % 2 == 0. But by doing it this way, you've created a relationship. That isEven is the opposite of isOdd.


In functional programming - it's BETTER to be repetitive. 
Another option - is using the `not()`.
https://i.imgur.com/In3yLtR.png

It's a readability gain! The declaritive relationship between the two functions. The `not` does one thing, and one thing well. It flips the return value. 


Point Free Example 

https://i.imgur.com/dCyeHQG.png

Look at how this was written. 
```
function when (fn) {
	return function(predicate) {
		return function(...args){
			if (predicate(...args)) {
				return fn( ... args);
			}
		}
	}
}
```

to call this: 
when(output)(isShortEnough)(msg1); 


### Closure

Closure -- quick example: https://i.imgur.com/IUDu86z.png

the `counter` is in the backpack. 


Lazy vs Eager Execution

LAZY --- 
https://i.imgur.com/mVvX97t.png

Where does the "AAAAA" happen? Line 7, or Line 9?
Line 9! It happens only when you call the funciton. 

Why would you use a Lazy (or deferred) Execution? 
PROS: 
* It only does it when it happens. 
CONS: 
* You have to keep calling it. And it destroys the result. So if't computationally heavy, you'll doing it over and over again.

EAGAR ---=
https://i.imgur.com/Oh0taPT.png
This fires on Line 8! 

PROS: 
* This time, you're putting it in the backpack. It catches.
CONS: 
* If you never get called, you did all the work. 



Memorization: 
best of both worlds. 
https://i.imgur.com/BlC1a2h.png

It's essentially cached. 

But --- This no longer functionally pure within the innards. 
Yet, actually... it IS functionally pure. 

Remember the rules?
If the input/output will always be the same. 
This code has a low-degree of confidence. 

A utility called memoize.
https://i.imgur.com/gIqPGgt.png

The cost of memorization. It's maintaining a internal cache. Is this function likely to be benefit? 

Referential Transparency - a function call can be replcaed with it's return value without affecting things. The benefit is not the memory... it's the reader. 

If they see that same exact function call, they can referneially swap out the result in their head. They know. 


Generalized to Specialized
https://i.imgur.com/tf0w6VQ.png

The name of a function call describes it's purpose. 

General -> Specific 
```
// This is a pointed definition. Super General.
function ajax(url, data, cb) {....}

ajax(CUSTOMER_API, {id: 42 }, renderCustomer);

// This is a lot more clearer. 
function getCustomer(data, cb) {
	return ajax(CUSTOMER_API, data, cb); 
}

getCustomer( { id:42}, renderCustomer); 

// THis is even more detailed. Also notice that you could ajax function over it. But by doing it this way... the relationship is much more clear. 
function getCurrentUser(cb) {
	return getCustomer({id: 42}, cb); 
}

getCurrentUser(renderCustomer); 

```


#### Partial Application & Currying

They're like cousins. 
They both do the same way. 

METHOD 1 - Partial Application
```
function ajax(url, data, cb) { ... }

var getCustomer = partial(ajax, CUSTOMER_API);

var getCurrentUser = partial(getCustomer, {id: 42}); 

getCustomer({id: 42}, renderCustomer);
getCurrentUser(renderCustomer); 
```

METHOD 2 - Currying: 
Comes form Haskell. 
Named after the guy who made it. 

```
function ajax(url) {
	return function getData(data) {
		return function getCB(cb) {
			...
		}
	}
}

ajax(CUSTOMER_API)({id:42})(renderCustomer);

var getCustomer = ajax(CUSTOMER_API);
var getCurrentUser = getCustomer({id: 42}); 
```

The other method of currying (https://i.imgur.com/CdEnMZf.png)
It's similar to the haskell style and mathmatical style. 


Both methods -- same outcome. Curry is more common. 
All functional programming languages - eeverything wants to be curried. 

Partial Application vs Currying: 

1. Both are specialistion techniques
2. Partial Application presents some arguments now, receive rest on the next call.
3. Currying doesn't preset any arguments. Recieves each argument one at at time. 

Strict Currying vs Loose Currying 

Each time I called the import, you call it one at a time. Strict Currying.
If you have 5 params, but you only know like 3 arguments... this is loose currying. All libraries use loose currying. 

If currying is generally preferred.. 


Specialization Adapts Shape: 
You typically have a library that uses `curry`. 
https://i.imgur.com/tprGiXd.png

### Composition

Illustrate the idea of composition

When one function call is routed to another function call. 

One time, it fires to another. 
To a point where you go "AH HA! That's a composition!"

```
function minus2(x) { return x - 2; }
function triple(x) { return x * 3; }
function increment(x) { return x + 1; }

// METHOD 1 - add shipping rate
var tmp = increment(4);
tmp = triple(tmp); 
totalCost = basePrice + minus2(tmp); 
```

Abstraction isn't about HIDING. 
It's about separating. 
So you can separate them, and make them easier to understand.

The question you want is -- "that tmp variable is just adding weight and confusion.";

```
// METHOD 2 - add shipping rate
totalCost = basePrice + minus2(triple(increment(4))); 
```

A visual image: 
The first method --- it's conveyer belt style, moving variable to variable to variable. 
The second method --- it's removing the belts, and stacking them on top. 
This third method --- remove all the wires and hide everything around the box. I've created a 'abstraction' around this composition. 

```
// third method -- function shipments: 
https://i.imgur.com/WfVGzqe.png
```

Suddenly, this is the abstraction! 
They don't care what it looks like. 


#### Declarative Data flow 

You now have a new problem -- they don't want to just use the minus2, triple, increment functions. They want to be able to shove any function in there!

This is the next level shit. 
https://i.imgur.com/ARUenop.png

And how to reuse it with our code from before...?
https://i.imgur.com/XFu0eGB.png

Be aware that this is also right to left. 

You increment, then triple, then minus2.

// COMPOSE -- RIGHT-TO-LEFT
// PIPE -- LEFT-TO-RIGHT  

Why? Anecdotals.
fn(A)(B)(C)... C goes to B, then B goes to A.


#### Composition with Currying

The easiest natural way to create unary functions... one result. 
If you have binary functions (they require 2 params)...

You want to shape it to be a single unary result.

### Immutability

Immutability doesn't means -- you don't want state change. 
It means -- how do we CONTROL change.

assignment mutability -- 

```
const shippingCost = 1.00
shippingCost *= 1.04 // not allowed! 
```

This is not really 'changing', this is reassigning.

```
var basePrice = 12.00
basePrice += 5.00 // allowed 
```

It's not changing basePrice to now become 17.00, it's reassigning it to the hardcoded 17.00. 


Temporaal Dead zone -- it's to stop you from assigning const to undefined, THEN something else. You can't! 

Functional Programming tries to avoid assignments anyways. They don't want to mutate  -- they want to pass values to something else. 

const can change the inner array, and the inner object.

In fact, people were so confused with const -- Java deprecated const and moved to `final` keyword. 

WITH CONST: 
it doesn't solve the problems it needs to solve.
1. scope already controls things. And if you're creating globals, already we're hitting a problem. 
2. If you're assigning things... there was intention to it. 
3. The things that you WANT to be immutable, like objects... aren't! https://i.imgur.com/Mng53ks.png
4. Doesn't matter if it's a string/number/boolean. But if it's an array, object... you're confusing everybody. 

We want a 'this value is read-only'. 

`Object.freeze` -- shallow freeze. It only makes it. 

Things that are good for -- json data. APi calls. Configs. 

#### Don't Mutate, Copy 

Always assume you're NOT allowed to mutate things.
Instead, make a copy of the object. 

https://i.imgur.com/CsYfuqG.png


One side of the issue: assume to the reader you don't want things to change. Use object.freeze.
On the other side: when you receive data, make changes to your local copy. 

#### Immutable Data Structure 

When you need a mutabile data structure, what you actually need a immutable data structure.

You need a structured data structure. 

It is a representation of a data, but a api. 

The big question is -- how can that be performant? 
If i'm creating a bunch of items over and over again... creating copies over again... how is that performant? 

This is the real power in mind -- of immtable data structure. 

Immutabile Data structure is like git repo. Changes are just diffs. You can make changes, compares, etc. It cracks it open, makes changes, and can roll back if needed.

JS doesn't have immutable data structure just yet. 
check out Immutable.js via Facebook. (Mori -- ClosureScript is similar to Javascript - yoinked it from there.)



### Recursion
Recusion is a declarative approach
I'm not concerned HOW it happens... I'm concerned about the outcome. 


Count a number of vowels.

```
function isVowel(char) {
	return ["a", "e", "i", "o", "u"].includes(char);
}

function countVowels(str) {
	var count = 0;
	
	for (var i = 0; i < str.length; i++) {
		
		if (isVowel(str[i])) {
			count++;
		}
	}
	
	return count; 
}
```

Ask yourself a real question - 
-> how can you solve this in a recursive way?
-> Can you solve the sub-problem? Can you solve it by reducing the project by minus 1?
-> How can I reduce my problem set to be something smaller?

How to solve the problem: 

How many vowels are here?

HOW TO ANSWER: 
	Define a base condition -- that's the stopper to avoid loops, like loops. 
	is there a way to solve the problem in smaller problem?
	Can I do a divide and conquer?
	
Recusion for loop -- https://i.imgur.com/8NYBzPQ.png


TERM: 
A stack frame (memory frame) is whenever a function is called, and save it. The stack data structure, as something else is called... it gets on top of it. Stacks. 
Typically - stack-- It's a couple hundred bytes... less than a k. 

StackOverflow -- doesn't often exist. Typically when your thing shuts down, it's a out of memory error. JS built from the beginning knowing... range error. 

Recursive -- they always think about it. In practice, we're limited by memory. But not today. But it can be. 

Tail Calls -- If a function is at the end (the tail), and we don't need the function anymore... we can throw it away. We don't have a O(n) Memory usage. That only works at the end of the tail. It has to be on the 
	It's not actually FASTER. Performance wise. But it's optimization for memory. 
	
	PTC - Proper Tail Calls -- This is what ES6 has been proposed. We want this. Standardize it so JS engines apply it to their engine.
	TCO - Family of optimizations on top of PTC. Not the same. 
	

20,000 21000 Stack before a memory out. 
There is a limit of Tail calls. Javascript does not have it. 


Your function has to be in a proper tail position. 

function decrement(x) {
	return sub(x, 1); 
	// not a proper tail call unless it's in the return statement.
}

function sub(x, y) {
	return x - y; 
}

As long as it doesn't contribute to the call stack, it's fine. If it added more to the call stack and can create a range error, then that's the problem. 

This is NOT a proper tail call. https://i.imgur.com/bDt3NFP.png
Because this does something to the stack(?).

#### Continuation-Passing Style 
This is CPS functional programming -but not really. 

It's how machines would write. 

Instead, use the Trampolines style. 
It's a funny way to describe bouncing up and down. 
So, we're never building a stack at all. (Like pancakes.)
Instead, we call a function, bounce up and down, then call another.. bouncing higher and higher. Stack depth never goes up. 

function trampoline(fn) {
	return function trampolined(....args) {
		var result = fn(...args);

		while (typof result == "function") {
			result = result(); 
		}
	return result; 
}

#### Identity Function 

v => v i



### List Operations

functor - a value (or collection of values) that can be mapped. 
Anything we can define a map function means it's a functor. 

Map is a transformation operation. 

Filter: Execlusion - a strainer (like for spaghetti)
Strainer - I'm excluding the water OUT of the noodles. 
Filter is more of a inclusion. You filter IN. 


When a function return a true/false, it's called a predicate. 
Predicate in general meaning is a statement about something that is either true or false. In programming, predicates represent single argument functions that return a boolean value.
http://zetcode.com/java/predicate/#:~:text=Predicate%20in%20general%20meaning%20is,that%20return%20a%20boolean%20value.


### Transduction

What about filters and reducers?
How do we put this together?

When you see 3 things (filters, reducers, map) 

you can't put filter and mapper together because it creates incomtabile shapes. 
It's a mathematical transformation of the function.

Transduction is composition of reducers.

He talks a lot about shaping. And modeling the result to shape something.
So a function outputs a boolean. That's the 'shape' of the result. 

Transducer -- it's a 'higher order' reducer. 


### Data Structure Operations 


Monad Data Structure -- FP Data structure.
Monad is a way to make a function data. 

Any value where you can map on it... it's a functor. 


Monad: A monoid in the category of endofunctors

Monad: A pattern for pairing data with a set of predictable behaviors that let it intereact with other data+ behavior pairings (monads)

The 3 Monadic laws -- not a math person so he didn't have an answer. 

#### The maybe monad. 
It won'd throw a exception if something is missing.

function Nothing() {
	return { map: Nothing, chain: Nothing, ap: Nothing };
}

One day you MAY use Monads. 
You may see someone use this tool and it's working. 
Don't be afraid and say "I have nothing to contribute."

Category theory math. 

### Async

#### Sync version of this 

This is known as syncronous, eager. 
It consumes whatever.
```
var a = [1, 2, 3];

var b = a.map(function double(v) {
	return v * 2; 
	}); 
	
b; 
```	

In order for `b` to exist, it needs `a`. All of it. Won't it be cool if there was a lazy method?

So only if you call `a`, does `b` get compiled?
```
a[0] // 1
b[0] // 2
```

That's called Observable. 

#### Obeservable 

Metaphor -- using a excel sheet. 
If you add 2 cells together, it gives you a result. 
If you change one of the cells... the result changes. 

A mapping that maps the value. 

Rx -- Reactive Programming. 
rx is a library from .net, and has been moved around to various places. 


Streams of data that flows from one thing to another. 

Functional programming of Event oriented programming. It's what reactive programming is... it's reacting to an event. 

Adapting your programming to the 'time' side of programming. 
https://rxjs-dev.firebaseapp.com/api

### Functional JS Utils

Lodash/FP -- 
It's NOT lodash. It's `lodash/FP`, the functional programming flavor. 

If you don't use a lot of lodash, I can't recommend using it. 

Ramda --- 
Recommendation for a library to start with. 
More popular the functional programming library. 
https://ramdajs.com/docs/
https://ramdajs.com/repl/


A style of programming called 'name-arguments'. Objective-C and scala. 

https://medium.com/@afontcu/cool-javascript-9-named-arguments-functions-that-get-and-return-objects-337b6f8cfa07
https://blog.bitsrc.io/javascript-why-named-arguments-are-better-than-positional-arguments-9b15ab3155ef

### Wrapup 
The truth is, that even several years into this journey of learning function and adapting it to my JavaScript code, I still don't start out writing a functional program. I definitely avoid some problems that I've gotten so comfortable with, that from the very beginning I can write. But for the most part, the way I still write my programs, is I write the imperative form and then I go back and I refactor it to be more functional.

And I do that little, by little, by little. And then I'll write another piece of my app and it's very imperative and there's bugs and weird stuff. And little, by little, by little, as I refactor it, I try to make it better and better and better with the principles of functional.

And my journey points to the idea that you can't really just go top down at functional. If you learned as an imperative programmer, the way all of us did, you really have to go bottom up. There's no other way to climb the cliff, than one step at a time, you can't just jump to the top.

You gotta climb this cliff one step at a time. And I hope what you've gotten out of this course, is now you're not scared of the equipment. Now you understand what the equipment does and as you climb up the mountain, as you figure out that journey, make sure to leave a map for other people to see how you're going as well.

### Random shit 


Functional library --  Ramda - and lodash 



Stack vs Heap in Javascript?
JS doesn't have a check if you exhaust the heap. It does for the Stack. 




RxJS - https://rxjs-dev.firebaseapp.com/guide/overview





