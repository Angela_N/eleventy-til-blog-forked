# 100 things about Javascript

########################
## Beginner Level JS
########################

## Javascript Basics

basic variables (var, let, const)
loops
conditionals
    - if/else
    - switch
    - teirnary

arrays
objects


## Objects and array, map, and set

how to declare it


## Messing with strings

## Basic terms
mutation

## Debugging

debugger
console.log

with(*)

## Javascript Engines
Javascript browser and node

https://stackoverflow.com/questions/46169376/whats-the-difference-between-a-browser-engine-and-rendering-engine
https://www.reddit.com/r/learnjavascript/comments/czhnj8/what_is_the_difference_between_a_browser_engine/
https://www.thecoderworld.com/difference-between-a-rendering-engine-and-browser-engine/
https://nodejs.dev/learn/differences-between-nodejs-and-the-browser
https://medium.com/swlh/what-is-node-js-and-how-does-it-differ-from-a-browser-ddebef00cbd9

## "Everything is an object"


## Searching for a match
Regex

regex, finding things. 

## The Stack

## Frameworks over the years

2010 

2020

## Compiling, tooling, and mordern development

## ECMAScript and what is it

## JS Engines

## Exception handling

Try/Catch
finally


## Promises


## Enumerations


## functions
parameters
arguments
param objects
return statements

## ES6 stuff? 
filter, map, 

########################
## Intermediate Level JS
########################

## Classes

super
constructor

extending
prototype

## Algorithm

### Searching

(include ES6 version)
Binary search
Fast Linear Search
Interpolation Search
Linear Search

### Sorting

Bubble Sort
Counting Sort
Merge Sort
Quicksort
Radix Sort


## Iterators
Simple Generator
Reverse Generator

########################
## Advance Level JS
########################

## Design patterns


## Currying

## Memoization

## First-order functions


