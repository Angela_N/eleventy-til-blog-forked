

## Refactoring a component with slots

### How to have the parent component see the data of the child component
In the `Book.vue` component,
how do you use the local bookTitle data in  `Library.vue`?

For example: data gets computed within the component, that mutates it.
But you also want that data to be seen by the parent prop, so you can do some other magic to it.

BEFORE:
```vue
// book component - Book.vue
<template>
  <div class="book">
    <slot name="title"></slot>
  </div>
</template>
<script>
export default {
  data() {
    return {
      bookTitle: 'Child Providing Data'
    }
  }
}
</script>

// Library parent component - Library.vue

<template>
  <Book>
    <template v-slot:title>
      <!-- How do we get the bookTitle from Book.vue? -->
    </template>
  </Book>
</template>

```

AFTER:
```vue
// book component - Book.vue
<template>
  <div class="book">
    <slot name="title" :bookTitle="bookTitle"></slot>
  </div>
</template>
<script>
export default {
  data() {
    return {
      bookTitle: 'Child Providing Data'
    }
  }
}
</script>

// Library.vue
<template>
  <Book>
    <template v-slot:title="slotProps">
      {{ slotProps.bookTitle }}
    </template>
  </Book>
</template>
```

NOTE: This does not work
```vue

// Library.vue
<template>
  <Book>
    <template v-slot:title="slotProps">
      <h1>{{ slotProps.bookTitle }}</h1>
    </template>
  </Book>
</template>
<script>
export default {
  computed: {
    uppercaseTitle() {
      // 🛑THIS DOES NOT WORK
      this.slotProps.bookTitle.toUpperCase()
    }
  }
}
</script>
```

Instead, do that on the child prop

```vue
// Book.vue
<template>
  <div class="book">
    <slot name="title"
      :bookTitle="bookTitle"
      :uppercaseBookTitle="uppercaseTitle"
    />
  </div>
</template>
<script>
export default {
  data() {
    return {
      bookTitle: 'Child Providing Data'
    }
  },
  computed: {
    uppercaseTitle() {
      return this.bookTitle.toUpperCase()
    }
  }
}
</script>
```


## Destructuring props

```vue
// Library.vue (after)

// BEFORE
<template>
  <Book>
    <template v-slot:title="slotProps">
      <h1>{{ slotProps.bookTitle }}</h1>
    </template>
  </Book>
</template>

// AFTER
<template>
  <Book>
    <template v-slot:title="{ bookTitle }">
      <h1>{{ bookTitle }}</h1>
    </template>
  </Book>
</template>
```

## Multiple slots
```vue
// multiple slots with incorrect syntax
<template>
  <Book v-slot:title="{ bookTitle }">
    <h1>{{ bookTitle }}</h1>
    <!-- 🛑THIS DOES NOT WORK -->
    <template v-slot:description>
      <p>My Description</p>
    </template>
  </Book>
</template>

//
<template>
  <Book>
    <template v-slot:title="{ bookTitle }">
      <h1>{{ bookTitle }}</h1>
    </template>
    <template v-slot:description>
      <p>My Description</p>
    </template>
  </Book>
</template>

```

## Cleaner V-bind

```vue
// Usual way
<template>
  <img v-bind:src="imageAttrs.source" v-bind:alt="imageAttrs.text" />
</template>

// Slightly better
<img v-bind="{ src: imageAttrs.source, alt: imageAttrs.text }" />

// Next level
<img v-bind="imageAttrs" />

<script>
export default {
  data() {
    return {
      imageAttrs: {
        src: '/vue-mastery-logo.png',
        text: 'Vue Mastery Logo'
      }
    }
  }
}
</script>
```

## V-on but cleaner

```vue
// usual way
<template>
  <img v-on:click="openGallery" v-on:mouseover="showTooltip" />
</template>

// better
<template>
  <img v-on="{ click: openGallery, mouseover: showTooltip }" />
</template>

// way cleaner
<template>
  <img v-on="inputEvents" />
</template>


<script>
export default {
  methods: {
    openGallery() { ... },
    showTooltip() { ... }
  }
}
</script>

```

## Chonky boy
```vue

// BEFORE
<template>
  <main>
    <Component
      v-for="content in apiResponse"
      :key="content.id"
      :is="content.type"
      :article-title="content.title"
      :article-content="content.body"
      :ad-image="content.image"
      :ad-heading="content.heading"
      @click="content.type === 'NewsArticle' ? openArticle : openAd"
      @mouseover="content.type === 'NewsArticle' ? showPreview : trackAdEvent"
    />
  </main>
</template>

// AFTER
<template>
  <main>
    <Component
      v-for="content in apiResponse"
      :key="content.id"
      :is="content.type"
      v-bind="feedItem(content).attrs"
      v-on="feedItem(content).events"
    />
  </main>
</template>


<script>
export default {
  methods: {
    feedItem(item) {
      if (item.type === 'NewsArticle') {
        return {
          attrs: {
            'article-title': item.title,
            'article-content': item.content
          },
          events: {
            click: this.openArticle,
            mouseover: this.showPreview
          }
        }
      } else if (item.type === 'NewsAd') {
        return {
          attrs: {
            'ad-image': item.image,
            'ad-heading': item.heading
          },
          events: {
            click: this.openAd,
            mouseover: this.trackAdEvent
          }
        }
      }
    }
  }
}
</script>
```
