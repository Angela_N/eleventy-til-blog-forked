## Accessibility 
---------------------------

1. Screen reading simulation software recommendations 

-----------
2. Checklist from this post
[] - Clear identifable links&buttons
```
For example, when linking to an about page, ‘click here’ doesn’t give any clue as to where it leads to, but ‘find out more about who we are’ is clear.

If links and buttons are labelled correctly, screen readers can read the label out loud. It means that blind and visually impaired people don’t have to press the link or button without knowing where it will take them.

As well as unlabelled elements, links and buttons that do not have a clear description are also really frustrating. They must have a clear description of where they will lead to when pressed, rather than ‘click here’. Never make your users guess where a link will take them or force them into a trial-and-error situation. This makes for tedious user experience.
```

[] - alt text for images
```
Screen readers read image descriptions out loud. This means that blind and visually impaired people can understand the content of the image in an accessible way. If images do not have alt text, then screen readers will simply say “image” or “graphic” which gives no context or meaning.
```

[] - forms 

https://bighack.org/5-most-annoying-website-features-i-face-as-a-blind-screen-reader-user-accessibility/

-----------
3. Smashing magazine ebook bundle 
Apples for all - coding accessible web applications
[x] - Did you read it? 


Inclusive dseign patterns (ebook)
[x] - Did you read it? 


Practical Approaches For Designing Accessible Websites
[] - Did you read it? 




-----------
4. Form design patterns 
[] - Did you read it? 


## Adding feedback on the title (med)
https://i.imgur.com/7baohNm.png
pg 51 - Document title 
```
This is especially useful when the page reloads after a server
request.
Even though we’re enhancing the user experience by
catching errors on the client with JavaScript, not all errors
can be caught this way. For example, checking that an email
address hasn’t already been taken can only be checked on
the server. And in any case, JavaScript is prone to failure so
we can’t solely rely on its availability.
```

This would be done via PHP during the validation check. So it needs to fail after Vue. 
```
The following JavaScript updates the title:
document.title = "(" + this.errors.length + ")"
+ document.title;

As noted above, this is primarily for screen reader users, but
as is often the case with inclusive design, what helps one set
of users helps everyone else too. This time, the updated title
acts as a notification in the tab
```

## Insure existing errors are removed on form submit a second time. (high)
pg 58 
```
When submitting the form for a second time, we need to
clear the existing errors from view. Otherwise, users may
see duplicate errors.
```

## Crafting a good error message (Low) 
pg 63-66)
Recommend this to Copywriting team 


## Grouping form (med)
PG 85 
```
GROUPING
To group multiple controls, we must wrap them in a
fieldset. The legend describes the group like a label
describes the individual control.

Some screen readers, such as NVDA, will read the legend
out, along with the first individual radio button’s label
when entering the field (in either direction). In this example, “Delivery options, Standard (Free two to three days)” is
announced. In other screen readers, such as Voiceover with
Safari, the legend is announced for every field.

If we omitted the fieldset and legend elements, screen
reader users would only hear “Standard (Free, two to three
days),” which is less clear.

You may be tempted to group all fields this way. For example, the address form from earlier could be wrapped inside a
fieldset with a legend set to “Address.” While this is technically valid, it’s unnecessary and verbose, as the field labels
make sense without a legend. Put another way, users don’t
need to hear “Address: Address Line 1” as it doesn’t add valu
```


## Accessibility alerts when content changes (high)
NOTE: This is in context of a helper text for textarea. Instead, think through how this works when dymanic content modifies the fields for accessibility. 

pg 90 
```
The character countdown
telling users how many
characters they have left.
```

```
The trouble is, this status is only determinable by sighted
users. To give screen reader users a comparable experience
(inclusive design principle 1), we should make sure this information is communicated to them too.
Screen readers will normally only announce content when
it is focused, but live regions announce their content when
it changes. This means we can communicate to screen
reader users without asking them to leave their current
location. In this case, it means users can continue to type
into the textarea.
<div role="status" aria-live="polite">You have 100
characters remaining.</div>
Notes:
• The aria-live="polite" property17 and the status
role18 are equivalent. Both are provided to maximize
compatibility across platforms and screen readers (in
some setups, only one or the other is recognized)
```

```
The equivalent alert and assertive values mean the
current readout of the screen reader will be interrupted
to announce the live region’s new content. In this case,
interrupting the user as they’re typing is aggressive. So
we can keep status and polite values, which means
the contents are announced after the user stops typing
for a moment.
```
More info: 
https://stackoverflow.com/questions/27546070/difference-between-aria-live-assertive-and-aria-live-polite

Things to consider: 
The vue context of the filing speed changes on select button dropdown. This should trigger a aria alert. Make sure this exists. 


## Autofill/autocomplete support (low)
This is more in UX, not accessibility. 
PG 97 
How it should look: https://i.imgur.com/WwxmBM3.png


## Number inputs (high)
pg 101 - 
Thing that i'm thinking about - the credit card info 
```
For example, IE11 and Chrome will ignore non-numeric
input such as a letter or a slash. Some older versions of iOS
will automatically convert “1000” to “1,000.” Safari 6 strips
out leading zeros. Each example seems undesirable, but
none of them stop users from entering true numbers.
Some numbers contain a decimal point such as a price;
other numbers are negative and need a minus sign. Unfortunately, some browsers don’t provide buttons for these
symbols on the keypad. If that wasn’t enough, some desktop
versions of Firefox will round up huge numbers.
```


```
In these cases, it’s safer to use a regular text box to avoid
excluding users unnecessarily. Remember, users are still
able to type numbers this way — it’s just that the buttons
on the keypad are smaller. To further soften the blow, the
numeric keyboard can be triggered for iOS users by using
the pattern attribute.25
<input type="text" pattern="[0-9]*">
```

```
In short, only use a number input if:
• incrementing and decrementing makes sense
• the number doesn’t have a leading zero
• the value doesn’t contain letters, slashes, minus
signs, and decimal points.
• the number isn’t very large
```


## CVC Field (low) 
pg 104 
```
To fix this, we should employ the hint text pattern to tell
users exactly what it is and where to find it. For example:
“This is the last three digits on the back of the card.”
```


## Suggestion - Answers page 
pg - 107 

```
7. Check Your Answers Page
Even though we’ve collected all the information required
to process the order, we should first let users review their
order. As counterintuitive as this may sound, adding an
extra step in the flow actually reduces effort and likely
increases conversion.
```

## Progress bars for screen readers (medium) 
pg 116 

```
Progress Step Text
If you decide to give users an indication of progress, first try
adding the step number inside the heading:
<h1>Payment (Step 3 of 4)</h1
```

```
Visual Progress Bar
If research shows that a more prominent progress bar is
useful, then you can include one — but there are a few
things to consider.
First, keep the text inside the <h1> so screen readers get a
comparable experience (inclusive design principle 1). Second,
you’ll need to hide it from sighted users like this:
<h1>Payment <span class="visually-hidden">Step 3 of 4
</span></h1>
The visually-hidden class contains a special set of properties that hide the element visually, while still ensuring it’s perceivable to screen reader users when the <h1> is announced
```


## Telephone Inputs 


Target specific link
```
[href^="http"]:not([href*="heydonworks.com"]) Translated, the selector reads, “Links with href values that start with http but do not contain the string heydonworks.com .”




Since <main> is designed to contain the salient content of a page, it can make the writing of a print style sheet easier. If your navigation, header, footer and sidebar ( <aside> ) regions are correctly made the siblings of <main> , you can target them quite easily with CSS: @media print { body > *:not(main) { display : none; } }


### Heading semantics 

```
<!-- DONT DO THIS --> 

<h1>How To Mark Up Blog Articles</h1 > <h2>In Seven Simple Steps</h2>

<!-- DO THIS --> 
<h1> How To Mark Up Blog Articles <span >In Seven Easy Steps</span > </h1>
```

### Fieldset to group items

A better way: 
```
<fieldset> 
    <legend>How good is reposado?</legend > 
    <!-- THIS IS A BETTER WAY TO CODE THE EXAMPLE --> 

    <input type="radio" id ="fantastic" name ="reposado" checked >  
    <label for="fantastic" >Fantastic</label > <br > 

    <input type="radio" id ="notBad" name ="reposado" > 
    <label for="notBad" >Not bad</label > <br > 
 
    <input type="radio" id ="meh" name ="reposado" > 
    <label for="meh" >Meh</label > <br > 
</fieldset >
```
`pg 13/97` Designing Accessible Websites 


### F'en div tags for buttons 

> Well, imagine that you had to navigate the web with a keyboard. Suppose that a modal window appeared on the screen, and you had very little context to know what it is and why it's obscuring the content you're trying to browse. Now you're wondering, “How do I interact with this?” or “How do I get rid of it?” because your keyboard's focus hasn't automatically moved to the modal window.

Suppose that a popular gaming website has a full-page modal overlay and has implemented a “close” button with the code below: 

```
<div id="modal_overlay"> <div id="modal_close" onClick="modalClose()" > X </div > 
… 
</div> 
```

This div element has no semantic meaning behind it.

We can circumvent all of these issues simply by writing the correct, semantic markup for a button and by adding an ARIA label for screen readers: 

```
<div id="modal_overlay"> 
<button type="button" class="btn-close" id="modal_close" aria-label="close"> X </button> </div> 
```

By changing the div to a button, we've significantly improved the semantics of our “close” button. We've addressed the common expectation that the button can be tabbed to with a keyboard and appear focused, and we've provided context by adding the ARIA label for screen readers.

*Rocky notes:* 

*There's another book where it explains why.*

*tl;dr: If you recreated `a` or `button`, you'll also have to re-create the dozen of `aria-*` code as well. Much of it comes right out of the box. By doing `<div>` as your button, you throw away all the accessibility code.* 

-----
## Inclusive Design Patterns Book (Rocky is still reading)



-------