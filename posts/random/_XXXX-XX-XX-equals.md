---
title: Not documented
description: This is a post on My Blog about win-win survival strategies.
date: 2018-08-24
tags:
  - second-tag
layout: layouts/post.njk
---

But the === operator does have some nuance to it, a fact many JS developers gloss over, to their detriment. The === operator is designed to lie in two cases of special values: NaN and -0. Consider:
NaN === NaN;            // false
0 === -0;               // true
In the case of NaN, the === operator lies and says that an occurrence of NaN is not equal to another NaN. In the case of -0 (yes, this is a real, distinct value you can use intentionally in your programs!), the === operator lies and says it's equal to the regular 0 value.
Since the lying about such comparisons can be bothersome, it's best to avoid using === for them. For NaN comparisons, use the Number.isNaN(..) utility, which does not lie. For -0 comparison, use the Object.is(..) utility, which also does not lie. Object.is(..) can also be used for non-lying NaN checks, if you prefer. Humorously, you could think of Object.is(..) as the “quadruple-equals” ====, the really-really-strict comparison!
12:14
The story gets even more complicated when we consider comparisons of object values (non-primitives). Consider:
[ 1, 2, 3 ] === [ 1, 2, 3 ];    // false
{ a: 42 } === { a: 42 }         // false
(x => x * 2) === (x => x * 2)   // false
What's going on here?
It may seem reasonable to assume that an equality check considers the nature or contents of the value; after all, 42 === 42 considers the actual 42 value and compares it. But when it comes to objects, a content-aware comparison is generally referred to as “structural equality.”
JS does not define === as structural equality for object values. Instead, === uses identity equality for object values.
12:15
JS, all object values are held by reference (see “Values vs References” in Appendix A), are assigned and passed by reference-copy, and to our current discussion, are compared by reference (identity) equality. Consider:
var x = [ 1, 2, 3 ];
// assignment is by reference-copy, so
// y references the *same* array as x,
// not another copy of it.
var y = x;
y === x;              // true
y === [ 1, 2, 3 ];    // false
x === [ 1, 2, 3 ];    // false
In this snippet, y === x is true because both variables hold a reference to the same initial array. But the === [1,2,3] comparisons both fail because y and x, respectively, are being compared to new different arrays [1,2,3]. The array structure and contents don't matter in this comparison, only the reference identity.
12:16
The == operator performs an equality comparison similarly to how the === performs it. In fact, both operators consider the type of the values being compared. And if the comparison is between the same value type, both == and === do exactly the same thing, no difference whatsoever.
12:19
Just like ==, these operators will perform as if they're “strict” if the types being relationally compared already match, but they'll allow coercion first (generally, to numbers) if the types differ.
Consider:
var arr = [ "1", "10", "100", "1000" ];
for (let i = 0; i < arr.length && arr[i] < 500; i++) {
// will run 3 times
}
The i < arr.length comparison is “safe” from coercion because i and arr.length are always numbers. The arr[i] < 500 invokes coercion, though, because the arr[i] values are all strings. Those comparisons thus become 1 < 500, 10 < 500, 100 < 500, and 1000 < 500. Since that last one is false, the loop stops after its third iteration.
