---
title: TIL using encoding to shorten letters
date: 2021-05-07
published: true
tags: ['system-design']
series: false
canonical_url: false
description: "All about that base64"
layout: layouts/post.njk
---


Base10 - our current numbering system
Base16 - Hexidecimal.
Base64 - usually binaries like image files. Also widely used for email attachments.

While exploring a System Design course, I learned that URL shorteners (like Bitly), they:

1. Hash the URL (e.g., MD5 or SHA256, etc.), so it's uniquely generated.
2. Then encoded for display.

Using base64 encoding, a 6 letters long key would result in 64^6 = ~68.7 billion possible strings.
Using base64 encoding, an 8 letters long key would result in 64^8 = ~281 trillion possible strings.

