---
title: TIL browsers explicity prevent sharing of cached third-party resources
date: 2021-05-15
published: true
tags: ['webdev', 'privacy']
series: false
canonical_url: false
description: "The original idea was that you would load resources from a CDN. Doesn't work anymore"
layout: layouts/post.njk
---

The original idea was that you would load resources from a CDN.

For example:

> If everybody loads the same jQuery script file (cdn.jquery.com/jquery.latest.js – or whatever URL it was back then), then the script file would be cached by the browsers, and other sites requesting the same script would benefit from the speedup of already cached resources. Users would have popular libraries like jQuery already sitting in their browser cache because earlier visited sites requested them.

But because of privacy, this cross-site resource caching can allow those resources to track users across different sites.

tl;dr:
Both Chrome and Safari have cache keys which prevent tracking, but also leads to duplicated resources in the browser cache. It's a small price to pay for privacy.

Also via this post:

> What does this change mean for you? If your sites live on modern hosting that provides a CDN and supports HTTP/2, you can drop the third-parties and should ship all resources yourself. Relying on a third-party provides little value in 2020.

REF:
https://www.stefanjudis.com/notes/say-goodbye-to-resource-caching-across-sites-and-domains/

