---
title: TIL Accessible CSS Accordions
date: 2021-05-23
published: true
tags: ['html', 'css', 'accessibility']
series: false
canonical_url: false
description: "For years, I've been looking to find a CSS accordion that is also accessible."
layout: layouts/post.njk
---

For years, I've been looking to find a CSS accordion that is also accessible.

## The baseline - a sweet CSS Only Accordion

There's a bunch of hard values. It could be refactored to use flexbox. But it's also using the checkbox hide/show hack.

https://www.mraffaele.com/labs/css-only-accordions/

## A Accessible CSS Accordion attempt

There's a few unknowns.

https://codepen.io/AndrewGail/pen/GjwKyb

It's also using the hide/show checkbox hack again.

> you shouldn't be using a checkbox to collapse/expand any element (bad practice). You can instead use a <button> that would trigger/control this functionality and on this button you should further add the proper ARIA values.
Via https://stackoverflow.com/a/54432453/4096078


## What W3 recommends

https://www.w3.org/TR/wai-aria-practices-1.1/examples/disclosure/disclosure-faq.html

They're usind `dl` and `dt` elements, and a button to show & hide.

## The best, cleanest method (so far)
There's a built-in HTML element that does it called [details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details). It was kinda ignored because of IE. But IE is dead so whatever.

Also fun fact:
> Note: The common use of a triangle which rotates or twists around to represent opening or closing the widget is why these are sometimes called "twisties."



details tag is the parent, where the below 2 tags are enclosed.
summary tag is where the headline users see by default(like a question for FAQ's) is held.

https://dev.to/hozefaj/fully-accessible-accordion-with-html-css-no-js-59n9
