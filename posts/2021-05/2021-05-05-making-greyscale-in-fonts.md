---
title: TIL how fonts draw greyscale
date: 2021-05-05
published: true
tags: ['fascinating', 'fonts']
series: false
canonical_url: false
description: "This may be super esoteric knowledge and could have incredibly limited usecases beyond this post."
layout: layouts/post.njk
---

This may be super esoteric knowledge and could have incredibly limited usecases beyond this post.

But TIL sometimes doesn't have actionable steps -- but just fascinating tidbits.

Via https://github.com/mmulet/code-relay/blob/main/markdown/HowIDidIt.md#a-you-dont

## How do you get color?
A: You don't!

All the color is "fake" in that there is nothing telling the renderer to draw a gray pixel, it all relies on undefined behavior and suggestion. Basically we are trying to "trick" the renderer into drawing shades of gray by drawing "pixels" of smaller and smaller sizes:

![](https://github.com/mmulet/code-relay/raw/main/markdown/how/pixels.png)

Pixels To draw a gray pixel we draw our pixels at a size smaller than an actual physical pixel, then the renderer will "average" the total color of the pixel together, so if we make our pixel half-white, half-black we end up with a gray pixel. Take a look at this example:

![](https://github.com/mmulet/code-relay/raw/main/markdown/how/good_colors.png)

The first cloud on the left has a perfect dark gray, while the cloud on the right, failed. It to doesn't work all the time, but when it fails, it looks like scan-lines which gamers (at least, retro-gamers) are used to.


