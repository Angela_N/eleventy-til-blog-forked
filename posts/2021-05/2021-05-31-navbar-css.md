---
title: TIL Sticky navbars with pure CSS
date: 2021-05-31
published: true
tags: ['css']
series: false
canonical_url: false
description: "Google 'Sticky Header', and you'll get a crapton of tutorials. Here's a vanilla CSS way to do it."
layout: layouts/post.njk
---

There's a dozen ways to do everything in web development.

Google "Sticky Header", and you'll get a crapton of tutorials.

Depending on when it was written, you'll get all sorts of things.

From 2010-2015, they'll do it using jquery.

From 2016-2018, it's done with vanilla javascript and measuring document heights.

These days, it can be done using Observer API.

But you don't even have to use javascript.

```css
#navigation {
    position: -webkit-sticky;  // required for Safari
    position: sticky;
    top: 0; // required as well.
}
```

REFERENCE:
https://webdesign.tutsplus.com/tutorials/sticky-positioning-with-nothing-but-css--cms-24042
