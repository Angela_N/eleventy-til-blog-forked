---
title: "TIL how Atari's Pitfall generates over 255 unique rooms"
date: 2021-05-08
published: true
tags: ['gaming']
series: false
canonical_url: false
description: "How it was done was by allowing code to generate the room, rather than mapping the room out and saving the data."
layout: layouts/post.njk
---


Games for the Atari 2600 were quite constrained.

> It was quite an innovation to have multiple rooms, and the fact that Adventure managed to have 30 was revolutionary. But Pitfall!, made by David Crane and released in 1983, had 255, all of which were much more elaborate (graphically speaking) than anything in Adventure. In this article we'll talk about how this was done.

> The console itself had only 128 bytes of RAM. That's 1024 bits. For comparison, this sentence alone takes up more space if encoded in ASCII, let alone the UTF format in which it’s actually encoded*. Suffice it to say there was not much space on the Atari.

How it was done was by allowing code to generate the room, rather than mapping the room out and saving the data.

A room layout was broken into 4 parts:

1) Objects (bits 0, 1, 2)
2) Pit Type (bits 3, 4, 5)
3) Trees (bit 6, 7)
4) Underground wall (bit 7)

It then uses maths to shift the bits around (LFSR), so the rooms are connected when you go back and forth, and yet still feels different.

Absolutely elegant feat!

REF:
https://evoniuk.github.io/posts/pitfall.html

