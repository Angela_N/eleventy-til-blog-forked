---
title: TIL the start attribute in lists
date: 2021-05-21
published: true
tags: ['html']
series: false
canonical_url: false
description: "The start attribute in lists"
layout: layouts/post.njk
---

```html
<ol>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ol>
```

RESULT:
1. Coffee
2. Tea
3. Milk

```html
<ol start="50">
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ol>
```

RESULT:
51. Coffee
52. Tea
53. Milk

VIA:
https://www.w3schools.com/tags/att_ol_start.asp

