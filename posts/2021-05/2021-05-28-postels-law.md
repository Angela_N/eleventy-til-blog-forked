---
title: "TIL Postel's Law (and how it sucks)"
date: 2021-05-28
published: true
tags: ['dev']
series: false
canonical_url: false
description: "Postel's Law... Also known as the Robustness Principle"
layout: layouts/post.njk
---

Postel’s Law is a clever bit of social engineering. Also known as the Robustness Principle, it goes like this:

> Be conservative in what you do, be liberal in what you accept from others.

This is mostly in reference to the early web, as `HTML` should 'just work'. And it does. You can write `HTML` in a lot of really stupid ways. And often times it will show up your screen, no questions asked! That is really cool.

One commentor had a great reason for this:

> That principle applied to critical systems software engineering leads to humans getting killed. E.g. in aerospace the result is airplanes falling out of the sky. Seriously. The Airbus A400M that recently crashed in Spain did so, because somewhere in the installation of the engine control software the control parameter files were rendered unusable. The result was, that the engine control software did fail hard, while this would have been a recoverable error (just have a set of default control parameters hardcoded into the software putting the engines into a fail safe operational regime); instead the engines shut off, because the engine control software failed hard. [comment](https://news.ycombinator.com/item?id=9827439)



## My perspective

I first heard of this being used when building REST APIs. An immediate red-flag popped up. Usually, if I don't understand something, I'll tuck it in my head and research it later. Then I forgot until I heard it recently.

As I get older (and also enjoying how powerful TypeScript is), I see it as a bad idea overall.

Using REST APIs as an example --
If the call was incorrectly made, a error should ALWAYS be given back. Accept nothing but correct calls. I think 99.9% of people will agree with me.

To argue with the 'airplanes falling out of the sky':

First, wtf?

Second, what type of system are you building that is so fragile that a crash KILLS everything?

The main argument is "be liberal in what you accept from others."

Sure. Your API should accept it. And reject it with an error. What you do with that error is your job.

And if you're building airplanes where you're not `try/catch` or defensively coding, and errors take your system down, you don't start yelling that code should just accept anything.


## Lastly

Google

REFERENCES:

http://trevorjim.com/postels-law-is-not-for-you/
