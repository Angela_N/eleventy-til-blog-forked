---
title: TIL roman numerals in list elements
date: 2021-05-01
published: true
tags: ['html']
series: false
canonical_url: false
description: "In documentation, you often find roman numeral listings, and I never bothered to figure out how that was done!"
layout: layouts/post.njk
---

In HTML: I've only used bullets or numbers for list elements.
If I was making custom bullets, I'd do that in CSS.

In documentation, you often find roman numeral listings, and I never bothered to figure out how that was done!

Well, this is it!

`<ol type="i">`


**Sets the numbering type:**
a for lowercase letters
A for uppercase letters
i for lowercase Roman numerals
I for uppercase Roman numerals
1 for numbers (default)

The specified type is used for the entire list unless a different type attribute is used on an enclosed <li> element.
https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol


But after a while:
You're probably better off using css list-style-type since there's a lot more things you can do.
property values --> https://www.w3schools.com/cssref/pr_list-style-type.asp
