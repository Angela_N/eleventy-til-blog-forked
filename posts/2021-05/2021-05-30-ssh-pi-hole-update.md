---
title: TIL how to update pi-hole on Win10
date: 2021-05-30
published: true
tags: ['raspberrypi', 'ssh']
series: false
canonical_url: false
description: "Pi-hole is great. It provides adblocking on a network level."
layout: layouts/post.njk
---

[Pi-Hole](https://pi-hole.net/) is great. It provides adblocking on a network level.

It's open-source, and run by volunteers. It's also updated pretty often with the latest and greatest.

To update -- it's such a simple command.
`pihole -up`

But it didn't really explain anything. I also don't remember if I set up my raspberry pi to allow for SSH, or if it was set up by default. Either way, I highly recommend doing that for ease.


Here's a better set of instructions to update your pi-hole.

1. ssh into your raspberry pi

> How to get your raspberry pi's IP address

> If you're already accessing the pi-hole admin portal by using 192.168.1.xxx/admin, that IP address is what you want to use.

> Another method is to access your modem/router settings, and look for all the networks attached.

> You can also [figure it out from PowerShell](https://stackoverflow.com/a/41797841/4096078), but I'm not smart enough to understand it.


In Powershell:
`PS C:\> ssh pi@192.168.1.xxx`

If you haven't changed your default password, it's `raspberry`.

2. Once you've ssh'ed into your raspberry pi, then run the command.

`pihole -up`




REFERENCES:
https://discourse.pi-hole.net/t/how-do-i-update-pi-hole/249
https://www.raspberrypi.org/documentation/remote-access/ssh/windows10.md


pihole -up


how to connect to your raspberry pi and update pihole
