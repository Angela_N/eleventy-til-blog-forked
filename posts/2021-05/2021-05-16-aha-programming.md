---
title: TIL AHA programming, like WET and DRY
date: 2021-05-16
published: true
tags: ['coding']
series: false
canonical_url: false
description: "AHA ('Avoid Hasty Abstractions')"
layout: layouts/post.njk
---


This is kinda tongue-in-cheek, but still a great article about the philosophy of code.

Via [Kent C Dodd](https://kentcdodds.com/blog/aha-programming)

## DRY ("Don't Repeat Yourself")

Every piece of knowledge must have a single, unambiguous, authoritative representation within a system

> Once, I inherited a codebase that made very heavy use of code duplication and one time I had to fix a bug in eight different places! 😱 Talk about irritating! Abstracting that code into a function that could be called anywhere it was needed would've helped out a LOT.

## WET ("Write Everything Twice.")

You can ask yourself "Haven't I written this before?" two times, but never three.

> In that same codebase I mentioned above, there was some over-abstraction that was even more harmful than duplication. It was AngularJS code and for several AngularJS controllers, the code passed this to a function which would monkey-patch methods and properties onto this in a way enhancing the controller instance with certain abilities. A sort of pseudo-inheritance thing I guess. It was SUPER confusing, hard to follow, and I was terrified to make any changes to that area of the codebase.

> The code was reused in lots more than three places, but the abstraction was bad and I wished that the code had been duplicated instead.

## AHA ("Avoid Hasty Abstractions")

prefer duplication over the wrong abstraction

> If you abstract early though, you'll think the function or component is perfect for your use case and so you just bend the code to fit your new use case. This goes on several times until the abstraction is basically your whole application in if statements and loops 😂😭

> A few years ago, I was hired to review a company's codebase and I used a tool called jsinspect to identify chunks of copy/pasted code to show them opportunities for abstraction. They had a bunch of duplicated code and from my perspective looking in, it was obvious what the abstractions should look like.

