---
title: TIL OS Package Managers
date: 2021-05-22
published: false
tags: ['mac']
series: false
canonical_url: false
description: "OSes also have package managers to make your life easier."
layout: layouts/post.njk
---

Cool kids use command lines to install things. 😎

You know how you can `npm i` and like... things just magically work?
OSes also have package managers to make your life easier.

## Windows

[Chocolatey for Mac](hhttps://chocolatey.org/)

INSTALL:
Follow the instructions, as you need to modify permissions.
https://chocolatey.org/install


GET CHROME & FIREFOX:
`choco install googlechrome`
`choco install firefox`

GET NODE VERSION MANAGER:
`choco install nvm`

## Mac

[Homebrew for Mac](https://brew.sh/)

INSTALL: (As of May 22, 2021. Always check the documentations!)
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

GET CHROME & FIREFOX:
`brew install --cask google-chrome`
`brew install --cask firefox`

GET NODE VERSION MANAGER:
`brew install nvm`


## Linux

Yikes, not touching this with a 10 foot pole.
