---
title: TIL CSS @Supports Rule
date: 2021-05-11
published: true
tags: ['css']
series: false
canonical_url: false
description: "The CSS @supports rule is a great way to check for a new CSS feature."
layout: layouts/post.njk
---

Today I learned about the CSS @Supports Rule

> The CSS @supports rule is a great way to check for a new CSS feature. For example, CSS comparison functions became supported in late 2019 - early 2020 and we don't have to wait for a year to use them.

```css
@supports (width: clamp(20px, 10vw, 50px)) {
  // do something..
}
```

VIA https://ishadeed.com/article/cross-browser-development/
