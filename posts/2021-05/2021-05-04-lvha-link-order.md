---
title: TIL LVHA in link order
date: 2021-05-04
published: true
tags: ['css']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

## The Order for Link Pseudo-Selectors


The popular acronym, LVHA may be of help. LVHA, kinda like LoVe HAte!

Link, Visited, Hover, Active

```
a:link {color: blue;}
a:visited {color: purple;}
a:hover {color: red;}
a:active {color: yellow;}
```



via https://css-tricks.com/snippets/css/link-pseudo-classes-in-order/
