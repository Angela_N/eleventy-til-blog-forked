---
title: TIL that PHP has an abandoned Gender Library
date: 2021-05-06
published: true
tags: ['php', 'lolz']
series: false
canonical_url: false
description: "This extension, in short, tries to guess the gender of first names."
layout: layouts/post.njk
---

This extension, in short, tries to guess the gender of first names. As its introduction says:

> Gender PHP extension is a port of the gender.c program originally written by Joerg Michael. The main purpose is to find out the gender of firstnames. The current database contains >40000 firstnames from 54 countries.

```
so @rphsoftware alerted me that PHP has a "Gender" class which has constants for all 109 genders, which are and I quote:

FEMALE
MOSTLY FEMALE
MALE
MOSTLY MALE
UNISEX
COUPLE
NOT FOUND
ERROR
ANY
BRITAIN
IRELAND
USA
SPAIN
PORTUGAL
ITALY
MALTA
```

It returns a value that looks like this.

```php
const integer IS_FEMALE = 70 ;
const integer IS_MOSTLY_FEMALE = 102 ;
const integer IS_MALE = 77 ;
const integer IS_MOSTLY_MALE = 109 ;
const integer IS_UNISEX_NAME = 63 ;
const integer IS_A_COUPLE = 67 ;
const integer NAME_NOT_FOUND = 32 ;
const integer ERROR_IN_NAME = 69 ;
```

How are those numbers determined?
Math from decades of demographic data?
AI algorithm?

Nope! These are magic numbers randomly made up!

Fortunately, this isn't a PHP Core thing.
This is in PECL, which is the rubygems/npm equivalent for PHP.


REFERENCE:
Foone's hilarious breakdown:
https://threadreaderapp.com/thread/1343285800369147904.html

https://www.sitepoint.com/theres-a-gender-extension-for-php/
