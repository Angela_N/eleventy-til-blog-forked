---
title: TIL When to use Serif vs San-serif
date: 2021-05-02
published: true
tags: ['css', 'fonts']
series: false
canonical_url: false
description: "Serifs are recognized for having small decorative lines added as embellishment. San-Serif is basic."
layout: layouts/post.njk
---

These are one of those "How many times do I have to tell you old man?"

## Serif #
A serif itself is a small line attached to the end of a stroke in a letter or symbol.

The most common serif font is ‘Times Roman'. I'm pretty sure you've seen it around.
Serifs are recognized for having small decorative lines added as embellishment. Aka Fancy Text!


Best Use:
On-screen, they look best on big, bold headlines. In print, they are very readable on print bold copy.

### Sans-serif #
Sans-serif is French for “without serif”. These are fonts without the decorative small line at the end of their letter/symbol strokes.

The most common typeface in this category is Helvetica.

Best Use:
On-screen, these are very readable! In print, best used for headlines and quick bits of texts such as sidebars.

### Monospaced Fod
A monospaced font is also called a fixed-width font. The widths of the characters are fixed!

Best use: In standard body texts, they aren't very good. They are harder to read and take up more horizontal space. In the dev world, they are famously used for code blocks
