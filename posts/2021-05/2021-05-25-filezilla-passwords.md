---
title: TIL how to get passwords from Filezilla
date: 2021-05-25
published: true
tags: ['passwords']
series: false
canonical_url: false
description: "A while back, I needed to get the saved password of a website from FileZilla."
layout: layouts/post.njk
---

A while back, I needed to get the saved password of a website from FileZilla.

This tutorial explains it.
https://www.groovypost.com/howto/retrieve-recover-filezilla-ftp-passwords/

But the XML was giving me a hashed password. That's when I discovered the password was in base64.

So i used this tool to conver it:
https://www.base64decode.org/
