---
title: TIL Optional Chaining
date: 2021-05-12
published: true
tags: ['javascript', 'es2020', 'codepen']
series: false
canonical_url: false
description: "The optional chaining `?.` is to see if the value exists"
layout: layouts/post.njk
---

Very new, very ES2020.

> The optional chaining `?.` is used to check if a value or a property before `?.` is null or undefined .
If it is, it returns `undefined` . Otherwise, it just returns the value.

You can also use optional chaining with object methods too.

```js
const user = {
  name: "Rocky",
  hairColor: "black",
};

console.log(user.age);  // Can create an Error

console.log("user age?", user?.age);   // undefined (no error).

console.log("user hairtype?", user?.hairColor?.hairType);   // undefined (no error).

console.log("user name?", user?.name);   // Rocky

```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="js,result" data-user="rockykev" data-slug-hash="KKWGjNE" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="KKWGjNE">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/KKWGjNE">
  KKWGjNE</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Via
https://javascript.plainenglish.io/4-useful-javascript-es2020-features-that-you-should-know-5d690430cf6e
