---
title: TIL the DeathGenerator
date: 2021-02-27
published: true
tags: ['gaming', 'cool']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

!()[https://i.imgur.com/kVCBoWL.png]

You want to generate some sweet custom death screens?

!()[https://i.imgur.com/OiN2Z6r.png]

(https://deathgenerator.com/)[https://deathgenerator.com/]

Learn more about it here:
https://github.com/foone/SierraDeathGenerator

@foone does a lot of cool stuff.
Check their https://www.patreon.com/foone
