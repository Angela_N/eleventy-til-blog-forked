---
title: TIL Combining JS objects together
date: 2021-02-24
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "tl;dr: use the spread operator"
layout: layouts/post.njk
---

```js
const obj1 = {'a': 1, 'b': 2};
const obj2 = {'c': 3};
const obj3 = {'d': 4};
```

```js
const objCombined = {...obj1, ...obj2, ...obj3};
```

::jazzhands::

VIA:
https://medium.com/developers-arena/some-simple-and-amazing-javascript-tricks-292e1962b1f6
