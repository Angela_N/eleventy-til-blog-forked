---
title: TIL about console.count and console.table
date: 2021-03-28
published: true
tags: ['debug', 'javascript']
series: false
canonical_url: false
description: "Some different ways to show data via your console"
layout: layouts/post.njk
---

We know about our good friend `console.log`.

Meet `console.count` (and the support `console.countReset`)

These two methods are used to set and clear a counter of how many times a particular string gets logged in the console:

![](https://i.imgur.com/yLfhJcN.png)


Also meet `console.table`

This particular method is incredibly useful to describe an object or array content in a human-friendly table:

![](https://i.imgur.com/kYMOslW.png)

-------

Via https://levelup.gitconnected.com/moving-beyond-console-log-8-console-methods-you-should-use-when-debugging-javascript-and-node-25f6ac840ada
