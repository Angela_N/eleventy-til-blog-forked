---
title: TIL
date: 2021-03-21
published: false
tags: ['gamedev', 'hacks']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---



Duke Nukem 3D isn't actually fully 3D. Like DOOM, it's actually a 2D map.

So how did Duke Nukem 3D do this?

> And you know that neat bit near the beginning of the first level, where you fall down an AC shaft and onto the street?
> The build engine can't do that! It's impossible!

<video width="320" height="240" controls>
  <source src="/img/2021-03-21-dukenukem-lv1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

It's actually done by teleporting!

<img src="/img/2021-03-21-dukenukem-map.png" alt="Server Side rendering">

The map above shows a more clearer example.
You start in the SQUARE area. When you jump down...

It teleports you to that lower area!


Via https://threadreaderapp.com/thread/1372766463556083715.html
