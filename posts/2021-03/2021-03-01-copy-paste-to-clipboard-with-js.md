---
title: TIL Copy Text to Clipboard
date: 2021-03-01
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "The user then copies that data, and pastes it somewhere else."
layout: layouts/post.njk
---

I made an app that generates a clean piece of data for the end-user.
The user then copies that data, and pastes it somewhere else.

This trick lets you do that:

```
const copyTextToClipboard = async (text) => {
  await navigator.clipboard.writeText(text)
}
```

Note that this function is asynchronous since the writeText function returns a Promise.

REFERENCE:
via https://javascript.plainenglish.io/15-helpful-javascript-one-liners-946e1d1a1653
