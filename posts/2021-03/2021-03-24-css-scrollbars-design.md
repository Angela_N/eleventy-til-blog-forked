---
title: TIL about adding Styling to scrollbars
date: 2021-03-24
published: true
tags: ['css', 'webdev', 'codepen', 'scrollbar']
series: false
canonical_url: false
description: "You can decorate your scrollbars with CSS"
layout: layouts/post.njk
---

You can design your scrollbars to give them a nice attractive look to it!

Yoinked the CSS-Tricks template and tested.
Direct Link: https://codepen.io/rockykev/pen/yLJLEaq

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="yLJLEaq" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="CSS-Tricks Almanac: Scrollbars">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/yLJLEaq">
  CSS-Tricks Almanac: Scrollbars</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


```css
body::-webkit-scrollbar {
    width: 3em;
    background-color: lightgreen;
}

body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}

body::-webkit-scrollbar-thumb {
  background-color: orange;
  border-radius: 4rem;
}
```

Excellent coverage too on [Can I Use](https://caniuse.com/css-scrollbar)


More info:
https://css-tricks.com/the-current-state-of-styling-scrollbars/
