---
title: TIL the 4 steps in rendering a webpage
date: 2021-03-14
published: true
tags: ['html']
series: false
canonical_url: false
description: "A typical browser renders a document in 4 steps: style, layout, paint, and compositing. "
layout: layouts/post.njk
---

When someone asks - what are the 4 steps of the render phase within a browser?

1. Style
2. Layout
3. Paint
4. Compositing
Via https://medium.com/swlh/optimizing-web-page-render-a64174455a1

STYLE

> At the beginning of the render phase, the DOM tree and CSSOM tree (similar to DOM tree, but built from the process of parsing CSS) are combined to a render tree where CSS stylings are matched to their corresponding DOM nodes (style).

LAYOUT THEN PAINT

> The render tree then computes the geometry of nodes in the tree (layout), and finally paints those elements on the screen (paint).

COMPOSITING

> Under certain circumstances, the visual appearance of the document will be broken into different layers. This is where the compositing is needed to ensure layers are drawn in the correct order.




To learn what's under the hood and do better Performance Rendering:
https://developers.google.com/web/fundamentals/performance/rendering

And some more resources:
https://stackoverflow.com/questions/39210858/what-is-reflow-and-repaint-in-these-steps-styles-layout-paint-composite

