---
title: TIL Switching to SVG for Icon Fonts
date: 2021-03-05
published: true
tags: ['css', 'icons']
series: false
canonical_url: false
description: "Icon fonts on the web were fundamentally flawed from the beginning. With full SVG support, let's use that"
layout: layouts/post.njk
---

> The introduction of the @font-face CSS at-rule allowed web designers to specify custom fonts with which to display text. By 2011, all major browsers supported it. This gave birth to the idea that fonts composed of pictograms, like Wingdings, could be used in place of raster images on the web. Considering that real SVG support across all major browsers didn't become stable until early 2020, icon fonts were the defacto way to add vector-based icons on your web site.

> But icon fonts on the web were fundamentally flawed from the beginning. And now, with full SVG support at our disposal, it's time to put a stop to their use, once and for all.

Overall - I'm still on the fence about this.

I like using fontAwesome's font-face because it's pretty straighforward.
1. Call the font-family
2. Call the glyph content ID
3. DONE

CONS:
You are using hacks like `letter-spacing` and `line-height` to modify sizing.

In one project, I did switch to SVGs, and it was really nice to use a lot of modern CSS designs to make it fit.

But, I'm not ready to switch over just yet.

REF:
https://www.irigoyen.dev/blog/2021/02/17/stop-using-icon-fonts/
