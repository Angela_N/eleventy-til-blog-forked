---
title: TIL Vendoring
date: 2021-03-29
published: false
tags: ['dev']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

Vendoring, in the programming sense, means “copying the source code of another project into your project.” It’s in contrast to the practice of using dependencies, which would be adding another project’s name to your package.json file and having npm or yarn download and link it up for you.

REF:
https://macwright.com/2021/03/11/vendor-by-default.html
