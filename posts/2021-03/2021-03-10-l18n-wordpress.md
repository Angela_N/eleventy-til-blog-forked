---
title: TIL i18n
date: 2021-03-10
published: true
tags: ['webdev', 'wordpress']
series: false
canonical_url: false
description: "Internationalization for WordPress"
layout: layouts/post.njk
---

## What is it?

> Internationalization is the process of developing your plugin so it can easily be translated into other languages. Localization describes the subsequent process of translating an internationalized plugin. Internationalization is often abbreviated as i18n (because there are 18 letters between the i and the n) and localization is abbreviated as l10n (because there are 10 letters between the l and the n.)

https://codex.wordpress.org/I18n_for_WordPress_Developers

> Functions to internationalize your extension. You can use these APIs to get localized strings from locale files packaged with your extension, find out the browser's current language, and find out the value of its Accept-Language header.
Via https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/i18n


## How it looks
This is within WordPress ecosystems:

`__( 'Hello, dear user!', 'my-text-domain' );`

The text domain is a unique identifier.

## Example:
`echo 'We deleted $count spam messages.'`

Would become:

`esc_html_e( "We deleted $count spam messages.", 'my-text-domain' );`


MORE INFO:
https://codex.wordpress.org/I18n_for_WordPress_Developers
