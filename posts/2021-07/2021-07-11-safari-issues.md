---
title: "TIL Safari's one-off problems"
date: 2021-07-11
published: true
tags: ['css', 'mac']
series: false
canonical_url: false
description: "Safari is the new IE when it comes to web development. All the one-offs you will face"
layout: layouts/post.njk
---


Safari is the new IE when it comes to web development.
I've faced most of these issues outlined here.

List via [One-offs and low-expectations with Safari](https://daverupert.com/2021/07/safari-one-offs/)

* appearance: none to fix buttons on iOS
* Keeping up with <meta name="viewport"> rules: minimal-ui? viewport-fit=cover? This keeps changing
* 100vh issues, solveable by and a slew new viewport units
* -webkit-text-size-adjust: 100% and other hacks to prevent input zoom
* iPhone env() notch negotiation
* Monochrome SVG favicons
* Polyfilling for scroll-behavior: smooth
* The -webkit soup required to style any inputs like input type="range"
* Double <meta name="theme-color"> for light/dark mode (Safari 15)

