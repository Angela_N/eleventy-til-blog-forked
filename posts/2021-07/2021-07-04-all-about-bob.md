---
title: TIL Bob who hired chinese developers to fix
date: 2021-07-04
published: true
tags: ['programming']
series: false
canonical_url: false
description: "Bob had hired a consulting firm in China, sent the consultants the work assigned to him, and then got out of the way, collecting paychecks the whole time"
layout: layouts/post.njk
---

About Bob.
Spoilers: He hires chinese developers

> Bob “apparently received excellent performance reviews, even being hailed the best developer in the building: his code was clean, well-written, and submitted in a timely fashion.” He was, somehow, producing great work without actually working. Bob’s employer didn’t seem to notice that he wasn’t doing any work, because from the corporation’s vantage point, he was productive.

> The plan was pretty simple: Bob had hired a consulting firm in China, sent the consultants the work assigned to him, and then got out of the way, collecting paychecks the whole time. The Chinese workers did the rest — including returning the completed code to Bob’s employer’s servers. (Bob could have masked the scam a while longer if he had done the check-in work.)

> Bob had most likely been doing this for a few years, taking about a quarter of his pay and using it to buy the services of lower-cost providers overseas.

Via http://nowiknow.com/what-about-bob/
