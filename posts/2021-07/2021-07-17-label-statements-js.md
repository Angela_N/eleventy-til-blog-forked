---
title: TIL Labels in Javascript
date: 2021-07-17
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "The 'labeled' statement can be used with break or continue statements."
layout: layouts/post.njk
---

The `labeled` statement can be used with break or continue statements. It is prefixing a statement with an identifier which you can refer to.
Via the [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label)


How does it work?

```js
fireThisStatement: {
  console.log("first");
  break fireThisStatement;
  console.log("second");
}

// > first
// -> undefined
```

TYou can use a `label` to identify a loop, and then use the break or continue statements to indicate whether a program should interrupt the loop or continue its execution.

In the example above, we identify a `label` fireThisStatement. After that `console.log('first');` executes and then we interrupt the execution.

Via [WTF Javascript](https://github.com/denysdovhan/wtfjs#readme)
