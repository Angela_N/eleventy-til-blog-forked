---
title: TIL Short circuiting
date: 2021-07-14
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Short circuit conditionals for that sweet one-liner"
layout: layouts/post.njk
---


These are one of the things I keep forgetting.


Short circuit conditionals

```js
// ORIGINAL
if (variableExists) {
    fireFunction();
}

// SHORT CIRCUITING
variableExists && fireFunction();
```


Via [17 Pro JavaScript tricks you didn't know](https://dev.to/rahxuls/17-pro-javascript-tricks-you-didn-t-know-5gog)
