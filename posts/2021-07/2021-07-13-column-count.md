---
title: TIL column-count
date: 2021-07-13
published: true
tags: ['css', 'codepen', 'html', 'webdev']
series: false
canonical_url: false
description: "Using the column-count property you can break an element's content into a specified number of columns."
layout: layouts/post.njk
---



Using the column-count property you can break an element’s content into a specified number of columns.

For instance, if you want to break the content inside a <p> element into 2 columns, you can do it like so.

```css
p {
  column-count: 2;
}
```


<p class="codepen" data-height="300" data-slug-hash="LYyeZbe" data-user="amit_merchant" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/amit_merchant/pen/LYyeZbe">
  The `column-count` property</a> by Amit Merchant (<a href="https://codepen.io/amit_merchant">@amit_merchant</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

It can also handle nestled content.

```css
.nestled-content {
  column-count: 2;
}
```

<p class="codepen" data-height="300" data-slug-hash="bGWaeqP" data-user="amit_merchant" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/amit_merchant/pen/bGWaeqP">
  The `column-count` property - Nested behaviour</a> by Amit Merchant (<a href="https://codepen.io/amit_merchant">@amit_merchant</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

And additional rules.

```css
p {
  column-count: 2;
  column-gap: 50px;
  column-rule: 2px solid purple;
}
```

Via [Break HTML content into newspaper-like columns using pure CSS](https://www.amitmerchant.com/how-to-break-html-content-into-columns-using-pure-css/)
