---
title: "TIL ES2021 - plus(+) operator as an alternative parseInt & parseFloat"
date: 2021-07-09
published: false
tags: ['javascript']
series: false
canonical_url: false
description: "You can use the + operator in front of any numeric string to parse it as a number."
layout: layouts/post.njk
---

You can use the `+` operator in front of any numeric string to parse it as a number.
It will return NaN if the string isn't numeric


```js

// converts string to int
parseInt("20"); // result: 20
+"20" // result: 20.3

// converts string to float
parseFloat("20.3") // result: 20.3
+"20.3"  // result: 20.3
```

Via https://dev.to/krtirtho/javascript-typescript-tips-compilation-2021-35hm
