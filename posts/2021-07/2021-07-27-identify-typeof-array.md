---
title: TIL how to check if the value is an array
date: 2021-07-27
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "If the value is an Array, true is returned; otherwise, false is."
layout: layouts/post.njk
---

Arrays aren't [primatives](https://developer.mozilla.org/en-US/docs/Glossary/Primitive) or [data structures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures).
so `typeof` doesn't work.

```js
const arr = [1, 2, 3];

console.log(typeof arr); // object

console.log(Array.isArray(arr)); // true
```

From the MDN:
```js
// all following calls return true
Array.isArray([]);
Array.isArray([1]);
Array.isArray(new Array());
Array.isArray(new Array('a', 'b', 'c', 'd'));
Array.isArray(new Array(3));
// Little known fact: Array.prototype itself is an array:
Array.isArray(Array.prototype);

// all following calls return false
Array.isArray();
Array.isArray({});
Array.isArray(null);
Array.isArray(undefined);
Array.isArray(17);
Array.isArray('Array');
Array.isArray(true);
Array.isArray(false);
Array.isArray(new Uint8Array(32));
Array.isArray({ __proto__: Array.prototype });
```


Via [17 Pro JavaScript tricks you didn't know](https://dev.to/rahxuls/17-pro-javascript-tricks-you-didn-t-know-5gog)

MDN:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
