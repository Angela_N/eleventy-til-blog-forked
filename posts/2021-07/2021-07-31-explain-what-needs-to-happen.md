---
title: TIL how devs can explain what needs to happen
date: 2021-07-31
published: true
tags: ['influence']
series: false
canonical_url: false
description: "If we’re attempting to convince someone to try something, we should try to empathize with the people we’re presenting to."
layout: layouts/post.njk
---

> If we’re attempting to convince someone to try something, we should try to empathize with the people we’re presenting to. Are they overwhelmed and dealing with lots of complexity? If our proposal helps them remove some of that work, drawing a small box helps emphasize that we’re trying to reduce or eliminate work; a large box might communicate that it‘s a lateral move or actually creating additional work.


One of the things we learn in business is to sell to the audience.

As developers, we often want to explain 'the why' in technical jargon.

And if it isn't hitting a pain point for the audience, they don't care.

For example: if you're trying to get from NY to Philadelphia by bus - what is important to you? The seats, how long it takes to get there, and the dropoff point. But imagine the bus driver trying to explain the specs of the bus engine. Does it really matter to you? (There's always that 1% who say yes. F those people.)

Jason explains it the same with his post.
Via Jason Lengstorf's [Draw the Box Smaller](https://www.jason.af/draw-the-box-smaller)
