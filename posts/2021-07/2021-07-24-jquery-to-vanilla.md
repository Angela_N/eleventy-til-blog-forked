---
title: TIL How to convert a jQuery plugin to Vanilla JS
date: 2021-07-24
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "To use [Lettering.js](http://letteringjs.com/), which is a reallky neat library, you need to bundle jQuery with it. How about not?"
layout: layouts/post.njk
---

It's not that I think jQuery is awful or anything.
It's more that it's outdated (with security risks), the conveniences that jQuery provided have been incorporated into Vanilla JS, and you need to bundle jQuery with your code to make it work.

For example: to use [Lettering.js](http://letteringjs.com/), which is a reallky neat library, you need to bundle jQuery with it.

Projects like this make me happy:
[Converting a jQuery plugin to vanilla JS: Lettering.js](https://gomakethings.com/converting-a-jquery-plugin-to-vanilla-js-lettering.js/)

The original jQuery version:
https://github.com/davatron5000/Lettering.js/blob/master/jquery.lettering.js

The updated Vanilla version (by Chris Ferdinandi):
https://gist.github.com/cferdinandi/68bae3f551d7daff8d442e232c499307


