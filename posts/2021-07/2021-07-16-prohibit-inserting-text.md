---
title: TIL prohibit inserting text
date: 2021-07-16
published: true
tags: ['forms', 'javascript']
series: false
canonical_url: false
description: "You may want to prevent the user from pasting text copied from somewhere in the input fields by calling its method preventDefault()."
layout: layouts/post.njk
---

> You may want to prevent the user from pasting text copied from somewhere in the input fields (think carefully about whether it's worth it). This is very easy to do by tracking the event paste and calling its method `preventDefault()`.

```html
<input type="text"></input>
<script>
  const input = document.querySelector('input');

  input.addEventListener("paste", function(e){
    e.preventDefault()
  })

</script>
```


Via https://dev.to/ra1nbow1/6-useful-frontend-techniques-that-you-may-not-know-about-47hd
