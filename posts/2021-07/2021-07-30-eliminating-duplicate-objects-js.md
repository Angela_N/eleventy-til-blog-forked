---
title: TIL Eliminating duplicate objects
date: 2021-07-30
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Using `Set` datatypes can let you do this with no fuss."
layout: layouts/post.njk
---

Using `Set` datatypes can let you do this with no fuss.

```js
const values = ['jane', 'lars', 'jane'];
const uniqueValues = [...new Set(values)]; // ['jane', 'lars'];
```

But you can't do this with arrays that have objects inside of it.

`Set` would consider each object to be unique.
```js
const members = [
  {
    first: 'Jane',
    last: 'Bond',
    id: '10yejma',
  },
  {
    first: 'Lars',
    last: 'Croft',
    id: '1hhs0k2',
  },
  {
    first: 'Jane',
    last: 'Bond',
    id: '1y15hhu',
  },
];
```

Solutions would be to:

## Approach 1: Building a new Array without duplicates
You can create a helper function that compares a specific key, and add them only if they don't exist.

```js
const uniqueMembers1 = [];

for (const m of members) {
  if (!containsMember(uniqueMembers1, m)) {
    uniqueMembers1.push(m);
  }
}

function containsMember(memberArray, member) {
  return memberArray.find(
    (m) => m.first === member.first && m.last === member.last);
}

```

## Approach #2: using filter


```js
// Only keep members m if they appear for the first time
const uniqueMembers2 = members.filter(
  (m, index, ms) => getIndexOfMember(ms, m) === index);

function getIndexOfMember(memberArray, member) {
  return memberArray.findIndex(
    (m) => m.first === member.first && m.last === member.last);
}

```

## Approach #3: Map from unique keys to members

```js

const uniqueKeyToMember = new Map(
  members.map(m => [m.first+'\t'+m.last, m])); // [key, value]

const uniqueMembers3 = [...uniqueKeyToMember.values()];
```


Via [Eliminating duplicate objects: three approaches](https://2ality.com/2020/07/eliminating-duplicate-objects.html)
