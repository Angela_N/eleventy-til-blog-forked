---
title: TIL how to Make a Square with CSS with aspect-ratio
date: 2021-07-21
published: true
tags: ['css']
series: false
canonical_url: false
description: "The first figure is for the top and bottom dimension, the second is for left and right."
layout: layouts/post.njk
---


> If you want to make a square without having to mess around too much with width and height, you can style your div [or span as the case may be] by setting a background color, the width you need, and then an aspect-ratio with equal figures. The first figure is for the top and bottom dimension, the second is for left and right.

```css
.square {
  background: green;
  width: 25rem;
  aspect-ratio: 1/1;
}
```


Via [CSS Cheat Sheet – 10 Tricks to Improve Your Next Coding Project](https://www.freecodecamp.org/news/10-css-tricks-for-your-next-coding-project/?utm_source=pocket_mylist)
