---
title: TIL Sort by alphabetical order with international safe-guards
date: 2021-07-25
published: true
tags: ['javascript']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

## Sort by alphabetical order
> I worked a lot in international companies and their apps had non-english data. When you do your "awesome" tricks to sort list of this kind of data it looks okay, sometimes because there're just a few strings for that moment. Maybe it looks okay cause you don't know that language's alphabet. Use correct one to be sure that it's sorted by alphabetical order for that language.

> For example. Deutsche alphabet

```js
// Wrong
["a", "z", "ä"].sort((a, b) => a - b);
// ['a', 'z', 'ä']

// Correct
["a", "z", "ä"].sort((a, b) => a.localeCompare(b));
// [ 'a', 'ä', 'z' ]
```

Via https://github.com/gigantz/code-like
