---
title: TIL to refresh your Cypress test
date: 2021-07-08
published: true
tags: ['testing']
series: false
canonical_url: false
description: "A good solution to clearly separate the tests and stop any application callbacks is to visit a 'neutral' blank page."
layout: layouts/post.njk
---


Sometimes, you may have data lingering during a assertion.

Cypress is smart enough to clear it.
Sometimes it doesn't.

That can lead to a flakey test.

> A good solution to clearly separate the tests and stop any application callbacks is to visit a "neutral" blank page. Unfortunately, using the cy.visit('about:blank') would not work.

The trick is this:
```js
afterEach(() => {
  cy.window().then((win) => {
    win.location.href = 'about:blank'
  })
})
```

Via
https://glebbahmutov.com/blog/visit-blank-page-between-tests/
