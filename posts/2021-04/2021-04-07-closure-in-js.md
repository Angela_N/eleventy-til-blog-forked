---
title: TIL Javascript Closures
date: 2021-04-07
published: true
tags: ['javascript', 'advanced']
series: true
canonical_url: false
description: "The answer to var a = add(2)(3); //5"
layout: layouts/post.njk
---

This is based on this question:

Make this syntax possible:
#1 - `var a = add(2, 3); //5`
#2 - `var a = add(2)(3); //5`


#1's Answer:
`var a = add(2, 3); //5`
```js
// CLASSIC JS
function add(x, y) {
  return x + y;
}

// ARROW FUNCTIONS
const add = (x, y) => x + y;
```

#2's Answer:
`var a = add(2)(3); //5`
```js
// CLASSIC JS
var add = function(x) {
    return function(y) {
      return x + y;
    };
}

// ARROW FUNCTIONS
const add = x => y => x + y;
```

So what the heck is happening here?
Using a mental model (and this is is for humans to understand visually what is going on) --

Functions are simply labels. Behind the scenes, the code will replace the label with the body of the function.

Using this again:
`var a = add(2)(3);`

```js
// Taking the params (2) and (3)
// The original function
var add = function() {
    x = 2;
    y = 3;
    return function(y) {
      return x + y;
    };
}
```

```js
// broken down even more
var x = 2;
return function(3) {
      return x + 3;
}
```

This is at a very basic level, and allows you to nestle functions and params.

Closure is the basis of a lot of other tricks for code organization, which I'll share in a few other posts.

SERIES:
* [Closure The Basics](/posts/2021-04/2021-04-08-closure-in-js)
* [Closure Example with class-like methods](/posts/2021-04/2021-04-08-closure-in-js-method)
* [Closure Example with private data](/posts/2021-04/2021-04-09-closure-in-js-private)
* [Closure Example as a backpack](/posts/2021-04/2021-04-10-closure-as-a-backpack)
