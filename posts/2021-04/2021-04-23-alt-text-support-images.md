---
title: TIL Adding Alt text to Supporting Images
date: 2021-04-23
published: true
tags: ['html', 'accessibility']
series: true
canonical_url: false
description: "For images not critical to the content but support it, a alt attribute must be present."
layout: layouts/post.njk
---

Via the HTML Spec docs themselves:

Sometimes, an image is not critical to the content, but is nonetheless neither purely decorative nor entirely redundant with the text. In these cases, the alt attribute must be present, and its value should either be the empty string, or a textual representation of the information that the image conveys. If the image has a caption giving the image's title, then the alt attribute's value must not be empty (as that would be quite confusing for non-visual readers).

## For example:
```html
<p><img src="jamesbond.jpeg" alt="">The James Bond series focuses on a fictional British Secret Service agent created in 1953 by writer Ian Fleming, who featured him in twelve novels and two short-story collections.</p>
```

You can either make it blank, or add that the image is a interpretation of James bond based on Ian Fleming's description.

Via the spec docs:
> If the picture is just a face, there might be no value in describing it. It's of no interest to the reader whether the individual has red hair or blond hair, whether the individual has white skin or black skin, whether the individual has one eye or two eyes.

> However, if the picture is more dynamic, for instance showing the politician as angry, or particularly happy, or devastated, some alternative text would be useful in setting the tone of the article, a tone that might otherwise be missed:

```html
<p><img src="jamesbond.jpeg" alt="James Bond looking cool with his gun">Other important elements which run through most of the films include Bond's cars, his guns, and the gadgets with which he is supplied by Q Branch.</p>

<p><img src="jamesbond.jpeg" alt="James Bond giving seductive eyes to a Bond girl">The films are also noted for Bond's relationships with various women, who are sometimes referred to as "Bond girls".</p>
```

> Whether the individual was "sad" or "ecstatic" makes a difference to how the rest of the paragraph is to be interpreted: is he likely saying that he is resigned to the populace making a bad choice in the upcoming referendum, or is he saying that the election was a mistake but the likely turnout will make it irrelevant? The interpretation varies based on the image.


## Supportive descriptions

IF the image supports the content with visuals, then the alt text should reflect that.

```html
<section class="bio">
 <h1>A Biography of Isaac Asimov</h1>
 <p>Born <b>Isaak Yudovich Ozimov</b> in 1920, Isaac was a prolific author.</p>
 <p><img src="headpics/asimov.jpeg" alt="Isaac Asimov had dark hair, a tall forehead, and wore glasses.
 Later in life, he wore long white sideburns.">
 <p>Asimov was born in Russia, and moved to the US when he was three years old.</p>
</section>
```

NOTE: You don't need to reference in the alt attribute that it's a image or photo.

For example:
If the alternative text was "A photo of Isaac Asimov"
It will be read as "(Image) A photo of Isaac Asimov"

That's not helpful.
It would be better if it was:
"(Image) Isaac Asimov had dark hair, a tall forehead, and wore glasses..."

REFERENCE:
https://html.spec.whatwg.org/#ancillary-images

