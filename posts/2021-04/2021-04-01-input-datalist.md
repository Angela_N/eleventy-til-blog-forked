---
title: TIL Input List with datalist
date: 2021-04-01
published: true
tags: ['html', 'codepen']
series: false
canonical_url: false
description: "datalists and autocomplete features"
layout: layouts/post.njk
---

The <datalist> tag is used to provide an "autocomplete" feature for <input>elements. You will see a drop-down list of pre-defined options as you type.

```html
<label>Selector:
<input list="animals" name="animal" id="animal">
</label>

<datalist id="animals">
  <option value="Cat">
  <option value="Dog">
  <option value="Chicken">
  <option value="Cow">
  <option value="Pig">
</datalist>

```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="gOgGEPP" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Input Selector">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/gOgGEPP">
  Input Selector</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

via
https://javascript.plainenglish.io/11-frontend-tricks-that-most-frontend-developers-dont-know-about-68dc48199ed6
