---
title: TIL Closure as a backpack
date: 2021-04-10
published: false
tags: ['javascript', 'advanced']
series: false
canonical_url: false
description: "The Backpack concept is a mental model to think about how closure saves data behind the scenes."
layout: layouts/post.njk
---

Closure as a backpack.

The Backpack concept is a mental model to think about how closure saves data behind the scenes.

The [example in the StackOverflow](https://stackoverflow.com/a/39045098/4096078) is so freakin good, it's one of the best I ever read.

CHALLENGE:
Incrementing a counter.


SOLUTIONS:

## Attempt 1 - The global variable
You could use a global variable, and a function to increase the counter:

```js
 var counter = 0;

 function updateClickCount() {
     ++counter;
     // Do something with counter
 }
```

Issue: Any script on the page can change the counter, without calling updateClickCount().

## Attempt 2 - Declaring the variable inside the function

```js
 function updateClickCount() {
     var counter = 0;
     ++counter;
     // Do something with counter
 }
```
Issue: Every time updateClickCount() function is called, the counter is reset to 0.

## Attempt 3 - Wrap the function with a inner function
```js

 function countWrapper() {
     var counter = 0;
     function updateClickCount() {
         ++counter;
         // Do something with counter
     }
     updateClickCount();
     return counter;
 }
```
Now, you can call `updateClickCount()` to add the counter!

Issue: How exactly do you call `updateClickCount()` from inside the function? You could try to expose the `updateClickCount()` function [with strategies from here](https://stackoverflow.com/questions/8817872/javascript-call-nested-function).

But then, you're adding a lot more complexity to this.

Let's look at this:
## Attempt 4 - Closure and the backpack
```js
const updateClickCount = (function(){
     let counter = 0;

     return function(){
         ++counter;
         return counter;
     }
 })();

console.log(updateClickCount()); // 1
console.log(updateClickCount()); // 2
console.log(updateClickCount()); // 3
```

First, this is a [IIFE](/post/2021-04-11-immediately-invoked-function-expression-js) statement.

Next, the anonymous function is actually a backpack. It saves the data in it's backpack and hides it away from scope. Nobody can modify counter++ but itself.


REFERENCE:
The amazing Will Sentance covers Closure in details in his Frontend Masters course.
https://frontendmasters.com/courses/javascript-hard-parts-v2/closure-introduction/

https://stackoverflow.com/a/39045098/4096078

SERIES:
* [Closure The Basics](/posts/2021-04/2021-04-08-closure-in-js)
* [Closure Example with class-like methods](/posts/2021-04/2021-04-08-closure-in-js-method)
* [Closure Example with private data](/posts/2021-04/2021-04-09-closure-in-js-private)
* [Closure Example as a backpack](/posts/2021-04/2021-04-10-closure-as-a-backpack)
