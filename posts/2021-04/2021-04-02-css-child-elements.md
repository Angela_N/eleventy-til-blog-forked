---
title: TIL using math to pick CSS child elements
date: 2021-04-02
published: true
tags: ['css']
series: false
canonical_url: false
description: "These are some sweet CSS psuedoclass tricks."
layout: layouts/post.njk
---

These are some sweet CSS psuedoclass tricks.

```css
/* Select All <li> elements but The First Three */
li:nth-child(n+4) {
      color: yellow;
}

/* Select only the first 3 <li> elemets */
li:nth-child(-n+3) {
    color: green;
}

/* Styles are elements that are not a <p> */
.my-class:not(p) {
    display: none;
}
```

REFERENCE:
https://javascript.plainenglish.io/11-frontend-tricks-that-most-frontend-developers-dont-know-about-68dc48199ed6
