---
title: TIL what to do if you're a Win User getting NODE_ENV errors
date: 2021-04-06
published: true
tags: ['windows', 'npm']
series: false
canonical_url: false
description: "Setting NODE_ENV=production before command babel doesn't work on Windows."
layout: layouts/post.njk
---

The life of a windows developer.

`'NODE_ENV' is not recognized as an internal or external command, operable program or batch file.`

If you got this error, it's often because the repo you got has a `package.json` file that contains something like this.

```json
"scripts": {
  "build": "NODE_ENV=production babel src --out-dir dist"
}
```

You can use this global package.

`npm install -g win-node-env`


RESOURCES:
Package: https://www.npmjs.com/package/win-node-env

SO Link: https://stackoverflow.com/a/64608575/4096078
