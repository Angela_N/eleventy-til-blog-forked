---
title: TIL the Chocolatey GUI
date: 2021-04-30
published: true
tags: ['win10']
series: false
canonical_url: false
description: "Installing software via the command line is a way of life. Btw this GUI is really nice too!"
layout: layouts/post.njk
---

Installing software via the command line is a way of life. Installing from a storefront or window... bleh!

For Macs, you have HomeBrew.

For Windows, you have Chocolatey.

Sometimes, you forget what you installed, and wish there was a GUI! AND YES -- This is counter to my opening sentence!

The Official Chocolatey GUI application
[https://github.com/chocolatey/ChocolateyGUI](https://github.com/chocolatey/ChocolateyGUI)


This includes a tab which shows all the currently installed applications in your machine.

To install it, simply do:

```
choco install chocolateygui
```

