---
title: TIL Creating save file versions and a update path
date: 2021-04-15
published: true
tags: ['gamedev']
series: false
canonical_url: false
description: "Check save version. If less than current version, update the save data."
layout: layouts/post.njk
---

QUESTION:
When you add new features in a game, how do you handle file saves?

ANSWER: (via user/jonerandom)[https://www.reddit.com/r/incremental_games/comments/mnbaee/how_do_i_update_everyones_save_file_when_i_add/gtxhinc?utm_source=share&utm_medium=web2x&context=3]
Check save version. If less than current version, update the save data.

Also a good idea to have one function per save version upgrade, and run them in order until the save data is up to date.

For example, if someone loads a save at version 2, and the game is at version 5, run the update function from v2 to v3, then v3 to v4, then v4 to v5.

That way you only ever have to worry about adding changes for the most recent update, and you can assume all previous updates will work because you won’t ever go back and change that code.

This has worked for me in a half a dozen games that have been updated many times over many years.
