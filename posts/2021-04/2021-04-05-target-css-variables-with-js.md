---
title: TIL targetting CSS variables with Javascript
date: 2021-04-05
published: true
tags: ['css', 'javascript', 'codepen', 'webdev']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

You can target CSS variables with JS, to change design elements without hacking the element itself.

This is super cool because now, you can actually focus on targetting CSS variables, rather than each individual JS element, and have it ripple effect to all your other CSS elements.

```css
:root {
  --pagebackground: rebeccapurple;
}

body {
  background: var(--pagebackground);
}
```

```js
let bg = getComputedStyle(document.documentElement).getPropertyValue('--pagebackground');

document.documentElement.style.setProperty('--pagebackground', 'yellow');
```


<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="js,result" data-user="rockykev" data-slug-hash="gOgGyLo" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Targetting CSS Variables with JS">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/gOgGyLo">
  Targetting CSS Variables with JS</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

REFERENCE:
https://christianheilmann.com/2021/02/08/sharing-data-between-css-and-javascript-using-custom-properties/
