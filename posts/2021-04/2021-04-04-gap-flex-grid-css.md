---
title: TIL gap in CSS
date: 2021-04-04
published: true
tags: ['css', 'codepen', 'responsive', 'webdev']
series: false
canonical_url: false
description: "replacing hacky margin-bottom with gap within CSS"
layout: layouts/post.njk
---

Say you want elements to be `1rem` distant apart, except for the last element.

This is the lame-o classic hacky way to do it.
```css
div > *:not(:last-child) {
    margin-bottom: 1rem;
  }
```

With Flex and Grid, you can use `gap`.

```css
div {
  gap: 1rem;
}
```

It's so nice.


<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="GRrMLJv" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="margin-bottom vs gap">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/GRrMLJv">
  margin-bottom vs gap</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

VIA
https://mobile.twitter.com/brad_frost/status/1359953369369088003
