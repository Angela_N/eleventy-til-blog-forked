---
title: TIL about Mounting/Unmounting without software in Windows 10
date: 2021-04-28
published: true
tags: ['win10']
series: false
canonical_url: false
description: "I used to download a virtual CD drive to mount isos. Apparently, it's built into Windows 10!"
layout: layouts/post.njk
---

I used to download a virtual CD drive to mount isos.

Apparently, it's built into Windows 10!

How to mount iso:

```
mount-DiskImage -ImagePath "C:\FILE.ISO"
Dismount-DiskImage -ImagePath "C:\FILE.ISO"
```

Via https://www.windowscentral.com/how-mount-or-unmount-iso-images-windows-10
