---
title: TIL that alternative text (alt text) is a replacement for the image
date: 2021-04-20
published: true
tags: ['accessibility', 'html']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

Via the HTML Spec docs themselves:

It is important to realize that the alternative text is a replacement for the image, not a description of the image.

> In the following example we have a flowchart in image form, with text in the alt attribute rephrasing the flowchart in prose form:

```html
<p>In the common case, the data handled by the tokenization stage
comes from the network, but it can also come from script.</p>
<p><img src="images/parsing-model-overview.svg" alt="The Network
passes data to the Input Stream Preprocessor, which passes it to the
Tokenizer, which passes it to the Tree Construction stage. From there,
data goes to both the DOM and to Script Execution. Script Execution is
linked to the DOM, and, using document.write(), passes data to the
Tokenizer."></p>
```

> Here's another example, showing a good solution and a bad solution to the problem of including an image in a description.

```html

<!-- This is the correct way to do things. -->
<p>
 You are standing in an open field west of a house.
 <img src="house.jpeg" alt="The house is white, with a boarded front door.">
 There is a small mailbox here.
</p>

<!-- This is the wrong way to do things. -->
<p>
 You are standing in an open field west of a house.
 <img src="house.jpeg" alt="A white house, with a boarded front door.">
 There is a small mailbox here.
</p>

REFERENCE:
https://html.spec.whatwg.org/#a-phrase-or-paragraph-with-an-alternative-graphical-representation:-charts,-diagrams,-graphs,-maps,-illustrations
