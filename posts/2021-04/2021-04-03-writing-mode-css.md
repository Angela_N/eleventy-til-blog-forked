---
title: TIL writing-mode
date: 2021-04-03
published: true
tags: ['css', 'accessibility', 'codepen', 'caniuse']
series: false
canonical_url: false
description: "How to flip text so it's sideways, for international reasons."
layout: layouts/post.njk
---

Today I learned about `writing-mode`.

This property specifies the block flow direction, which is the direction in which block-level containers are stacked, and the direction in which inline-level content flows within a block container. Thus, it also determines the ordering of block-level content.

```css
.normal {
  writing-mode: horizonal-tb;
}
.sideway {
  writing-mode: vertical-rl;
}
.sideway-r {
  writing-mode: vertical-lr;
}
```

```css
/* Keyword values */
writing-mode: horizontal-tb;
writing-mode: vertical-rl;
writing-mode: vertical-lr;

/* Global values */
writing-mode: inherit;
writing-mode: initial;
writing-mode: unset;
```

Via the MDN (as of 2021), there's two experimental values:

`sideways-rl`
For ltr scripts, content flows vertically from bottom to top. For rtl scripts, content flows vertically from top to bottom. All the glyphs, even those in vertical scripts, are set sideways toward the right.

`sideways-lr`
For ltr scripts, content flows vertically from top to bottom. For rtl scripts, content flows vertically from bottom to top. All the glyphs, even those in vertical scripts, are set sideways toward the left.

I couldn't get `writing-mode: vertical-lr;` working. Was hoping to create an example.

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="ExZwMmG" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="ExZwMmG">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/ExZwMmG">
  ExZwMmG</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


It has excellent support.
https://caniuse.com/css-writing-mode
