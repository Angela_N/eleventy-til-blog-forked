---
title: TIL Closures in Private Methods
date: 2021-04-09
published: true
tags: ['javascript', 'advanced']
series: true
canonical_url: false
description: "Closure in JS: Private data"
layout: layouts/post.njk
---

In JS, closures are useful in hiding the implementation of functionality while still revealing the interface.

Note - it's using a [IIFE](/post/2021-04-11-immediately-invoked-function-expression-js)

For example, imagine you are writing a class of date utility methods and you want to allow users to look up weekday names by index, but you don't want them to be able to modify the array of names you use under the hood.

```js
const jediCatalog = {

  getJediFromRank: (function() {

    const rankTable = {
      "jedi masters" : ["Yoda", "Obi-Wan Kenobi", "Mace Windu"],
      "jedi knights" : ["Anakin Skywalker", "Luke Skywalker"],
      "padawans" : ["Ahsoka Tano"]
    }

    return function(x) {
      if ((typeof x !== 'string')) {
        throw new Error("invalid type. Must be string");
      }

      if (rankTable.hasOwnProperty(x)) {
        return rankTable[x];
      } else {
        throw new Error("Must be 'jedi masters', 'jedi knights', or 'padawans'")
      }

    };
  }())
};


console.log(jediCatalog.getJediFromRank("jedi masters"));
// result:
// [object Array] (3)
// ["Yoda","Obi-Wan Kenobi","Mace Windu"]
```

The goal of `getJediFromRank` is to get Jedi based on their rank.

Why is `rankTable` inside the anonymous function?
It locks the connection of the data to the output, and scopes that data.

A developer can't just modify the `jediCatalog` object to change the rankTable. They would have to change it inside the anonymous function, which separates it's concern.

REFERENCE:
https://stackoverflow.com/a/2728628/4096078

SERIES:
* [Closure The Basics](/posts/2021-04/2021-04-08-closure-in-js)
* [Closure Example with class-like methods](/posts/2021-04/2021-04-08-closure-in-js-method)
* [Closure Example with private data](/posts/2021-04/2021-04-09-closure-in-js-private)
* [Closure Example as a backpack](/posts/2021-04/2021-04-10-closure-as-a-backpack)
