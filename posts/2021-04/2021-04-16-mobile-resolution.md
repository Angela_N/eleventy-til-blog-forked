---
title: TIL that you should update your mobile Resolution in Devtools
date: 2021-04-16
published: true
tags: ['devtools']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

In Devtools -> Toggle device Toolbar->
    Mobile S 320px -> iPhone 5
    Mobile M 375px -> iPhone 6 (released in 2014)
    Mobile L 425px -> The bigger iPhones

This is kind of the assumption.

Via https://devhints.io/resolutions:

But it's also kinda outdated. We sort of need bigger sizes.

Look at the table below:
---
Most Common Mobile Screen Resolution Size in the United States Of America
414×816 – 17.89%
375×667 – 14.2%
375×812 – 11.97%
360×640 – 8.55%
414×736 – 8.54%
412×846 – 4.7%
360×740 – 4.39%
Via https://www.hobo-web.co.uk/best-screen-size/#chartHeaderTitle
---

If you're targetting certain devices, you'll need a wider range of devtool screen sizes.

Additional Info:
iPhone Model usage (hard to find - and this is just one guy's app data):
Via https://david-smith.org/iosversionstats/
