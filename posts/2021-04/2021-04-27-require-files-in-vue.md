---
title: TIL properly adding files into Vue
date: 2021-04-27
published: true
tags: ['vue']
series: false
canonical_url: false
description: "Adding external files into vue methods"
layout: layouts/post.njk
---

This probably happens to me every other Vue Project. So might as well right about it.

*WRONG: *
```js
var audio = new Audio('./file.mp3');
```

This gets decoded as a string.


*CORRECT: *
```js
var audio = new Audio(require('./file.mp3'));
```

Webpack will ensure the proper path and filename for that resource.

Via https://stackoverflow.com/a/51126831/4096078
