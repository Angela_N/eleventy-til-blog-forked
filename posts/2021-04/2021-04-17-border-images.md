---
title: TIL using images as borders
date: 2021-04-17
published: true
tags: ['css']
series: false
canonical_url: false
description: "Images as a border? Use cases are like pencil borders, or patterns"
layout: layouts/post.njk
---

A client project had me use custom 'pencil-drawing' borders.
That's when I discovered you can use border images to create that effect.


border-image-source
border-image-slice
border-image-width
border-image-outset
border-image-repeat

REFERENCE:
https://developer.mozilla.org/en-US/docs/Web/CSS/border-image
