---
title: TIL Technical Debt isn't a problem, it's a tool
date: 2021-11-18
published: true
tags: ['tech-debt', 'good-dev']
series: false
canonical_url: false
description: "t's a very junior-level thing to assume all technical debt is bad. It's like assuming all debt is bad. Senior devs know how to make trade-offs."
layout: layouts/post.njk
---

> Unlearn what you know about technical debt

> Many people hate technical debt.

> This is nonsense. They don’t know how to take advantage of it.

> When used appropriately, technical debt means working on what truly matters and deferring anything that can wait

It's a very junior-level thing to assume all technical debt is bad. It's like assuming all debt is bad.

Senior devs know how to make trade-offs.

It's okay to acquire technical debt if:

* You need to push something out of the door.
* This project has a limited lifespan. (Maybe the app has a 1-year lifespan)
* You're just not in the right position to solve it now

I think about the business cases.

Look at it from the math perspective:

1) App is completely perfect (no tech debt) that took 6 months to launch.
6 months of development.
6 months of market.

or

2) A app that launches quickly,  but tech debt?
2 months of development.
10 months of market.

(4 months of refactoring.)

You want to know why games launch buggy? Because no matter what you see/hear online, people still pay for buggy launches if at the end, the product is good.

VIA
[Lessons learned from the smartest Software Engineer I’ve met](https://svpino.com/lessons-learned-from-the-smartest-software-engineer-ive-met-35895ac9fe3a)
