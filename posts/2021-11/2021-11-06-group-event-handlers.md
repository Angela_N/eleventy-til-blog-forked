---
title: TIL Grouping Event Handlers
date: 2021-11-06
published: false
tags: ['cleancode', 'event-handlers', 'javascript']
series: false
canonical_url: false
description: "Group your event handlers together for that sweet cleaner, fresher code."
layout: layouts/post.njk
---


Creating a single event handler object!

I'm super guilty of tying the click events like this...

```js

const button1 = document.querySelector('button1');
const button2 = document.querySelector('button2');
const button3 = document.querySelector('button3');

button1.addEventListener('click', event => {
  button.textContent = `Click count: ${event.detail}`;
});

button2.addEventListener('click', event => {
  button.textContent = `Click count: ${event.detail}`;
});

button3.addEventListener('click', event => {
  button.textContent = `Click count: ${event.detail}`;
});

```

A better method would be to group them together.

Something like below:

```js
const button1 = document.querySelector('#button1');
const button2 = document.querySelector('#button2');
const button3 = document.querySelector('#button3');

// Use a single click event listener
document.addEventListener('click', event => {

  // 1 - get the target
  const element = event.target;

  // 2 - business logic based on the target
  if (element.tagName === 'BUTTON') {
      document.querySelector(`#${element.id}Text`).textContent = `${element.id} Click count: ${event.detail}`;
  }

});
```

<p class="codepen" data-height="300" data-slug-hash="ExvJQgX" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/ExvJQgX">
  Untitled</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

That will allow me to keep ALL click events in one easy-to-read function rather than hunt it down.

Right now, the events are checked to see if it's a `button` and automatically assumes what happens based on the `element id`.

A better method would be to use data attributes.

Each handler name corresponds to the value of one of the `data-` attributes.

So something like:
```html
<button data-click="logout">Log Out</button>
```

Now you can loop through `data-click` attributes to really control the flow!

If you want to go wild and crazy, check out this blog post.

NEXT LEVEL:
[Strategies for working with data attributes in vanilla JavaScript](https://gomakethings.com/strategies-for-working-with-data-attributes-in-vanilla-javascript/?utm_source=pocket_mylist)
