---
title: TIL that there was a menu element in prior HTML versions
date: 2021-11-03
published: true
tags: ['history', 'html', 'web-development']
series: false
canonical_url: false
description: "There was a menu element that we replaced with ul and li elements"
layout: layouts/post.njk
---

HTML4, `<menu>` was depreciated. Because `<ul>` and `<li>` could do the same.

HTML5, it was kinda brought back. Only by Firefox. And the other browsers were like, "Why?"

You can still do this. This is still legal. And it will kinda display properly.

But it won't have all the sweet accessibility benefits that `<ul>` and `<li>` has.

```html
<menu>
  <li><a href=...>Some text</a></li>
  <li><button type=button>Some action!</button></li>
  <!-- ... -->
</menu>
```


REF:
[Semantic <menu> context](https://www.scottohara.me//blog/2021/10/21/menu.html)
