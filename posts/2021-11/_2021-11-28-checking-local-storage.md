---
title: TIL Check if local storage is enabled
date: 2021-11-28
published: true
tags: ['local-storage', 'javascript']
series: false
canonical_url: false
description: "You do a quick test to see if you can set/remove an item. If it fails, then LocalStorage is not available."
layout: layouts/post.njk
---

Check if local storage is enabled.

```js
const isLocalStorageEnabled = () => {
  try {
    const key = `__storage__test`;
    window.localStorage.setItem(key, null);
    window.localStorage.removeItem(key);
    return true;
  } catch (e) {
    return false;
  }
};

isLocalStorageEnabled(); // true, if localStorage is accessible
```

REFERENCE:
[21 Useful JavaScript Snippets For Everyday Development](https://medium.com/@danthedev/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
