---
title: TIL all functions create a empty prototype object
date: 2021-11-02
published: true
tags: ['javascript', 'classes', 'advanced']
series: false
canonical_url: false
description: "When you create a function in JavaScript, it automatically creates an empty container called prototype for you to stick your methods into."
layout: layouts/post.njk
---

When you create a function in JavaScript, it automatically creates an empty container called prototype for you to stick your methods into.

![](https://i.imgur.com/hQD4txq.png)


REF:
[Understanding Prototypes in JavaScript](https://medium.com/madhash/understanding-prototypes-in-javascript-e466244da086)
