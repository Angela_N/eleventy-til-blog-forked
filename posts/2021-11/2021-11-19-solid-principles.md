---
title: TIL when to use SOLID in 2021
date: 2021-11-19
published: true
tags: ['programming', 'solid']
series: false
canonical_url: false
description: "SOLID is an acronym for 5 principles. But they were made when dinosaurs roam the earth."
layout: layouts/post.njk
---

What is SOLID?
SOLID is an acronym for 5 principles

S. 👉 Single Responsibility

O. 👉 Open/Close

L. 👉 Liskov Substitution

I. 👉 Interface Segregation

D. 👉 Dependency Inversion

||Use|Avoid|
|--- |--- |--- |
|Single Responsibility|For Scalable and Maintainable Code.|When too much Fragmentation occurs without predictable future changes.|
|Open Close|To prevent old code from breaking due to a new one.|When over-engineering.|
|Liskov Substitution|Parent/Child used interchangeably without breaking.|When substitutions do not make sense. (Bird example)|
|Interface Segregation|For Role-specific interfaces.|When difficult to aggregate  (due to a lot of modules) from segregation.|
|Dependency Inversion|For different low-level implementations.|When different implementations of a low-level module are not needed,  like the String class in most languages are not changed because it's not needed to mostly.|


REFERENCE:

[Solid. Is It Still Useful In 2021?](https://dev.to/huzaifa99/solid-is-it-still-useful-in-2021-5ff6?utm_source=pocket_mylist)
