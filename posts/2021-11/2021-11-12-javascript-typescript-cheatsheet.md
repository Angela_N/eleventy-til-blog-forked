---
title: TIL Javascript Vs Typescript cheatsheet
date: 2021-11-12
published: true
tags: ['javascript', 'typescript', 'cheatsheet']
series: false
canonical_url: false
description: "A easy-to-print sheet pointing out the difference between the two."
layout: layouts/post.njk
---


A easy-to-print sheet pointing out the difference between the two.

![](https://i.imgur.com/442m1CW.png)

VIA:
[The Essential Cheatsheet JavaScript vs. Typescript](https://medium.com/front-end-weekly/the-essential-cheatsheet-javascript-vs-typescript-7a27ef187bdb)
