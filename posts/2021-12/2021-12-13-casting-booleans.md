---
title: TIL Casting values to JS booleans
date: 2021-12-13
published: true
tags: ['javascript', 'booleans']
series: false
canonical_url: false
description: "use the !! to casting values to JS"
layout: layouts/post.njk
---

Sometimes, you get something from a api like:

```js
// json response
{
  type: 'article',
  length: '1200 words',
  publishStatus: 1 // 0 - not published, 1 - published
}
```

For safety, you really want to cast `publishStatus` to a boolean.

You can do that with `!!`.

```js

const isPublished = !!json.publishStatus;

```

How does it work?

The first bang reverse it and casts it to a boolean. So if it was a falsy value, it'll convert it to true.
The second bang reverses it again to it's original status, but keeping the boolean.

falsy value -> true -> false.

