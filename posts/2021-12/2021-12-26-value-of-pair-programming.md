---
title: TIL the Two Types of People and Why Pair Programming works
date: 2021-12-26
published: true
tags: ['frontend-masters', 'quote']
series: false
canonical_url: false
description: "The 'Researcher' & the 'Stack Overflower' - Neither is effective in its own isolation, both are necessary to being an engineer, you've done this in the first year of your careers, both of these."
layout: layouts/post.njk
---

This is from Will Sentance's course JavaScript: The Hard Parts, v2 on Frontend Masters.

The two types of people:

**Person 1 - The Researcher**

The "researcher" hits a block and assuming the code, and then `console.log` & debug the data as you go through, line by line. They spend the following three days reading every last thing about the `new` keyword, and about all aspects of the prototype chain, and try to understand everything.

The problem with this is that they can take hours, days, to solve a single roadblock.

**Person 2 - Stack Overflower**

The "Stack Overflower" grab snippets of code to fix a bug from Stack Overflow. When they plug it in, doesn't work, they try another one. They take these bits and try to stick it  together.

The problem with this is that they aren't really understanding how their code is working. They just knows that it works.

**Why Pair programming is important:**

> Neither is effective in its own isolation, both are necessary to being an engineer, you've done this in the first year of your careers, both of these.


