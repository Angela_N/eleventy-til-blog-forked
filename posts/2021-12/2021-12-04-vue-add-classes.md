---
title: TIL a clean way to add classes based on the page in Vue
date: 2021-12-04
published: true
tags: ['clean-code', 'arrays', 'vue']
series: false
canonical_url: false
description: "If your pages exist in a array, use the includes() to check."
layout: layouts/post.njk
---

In Vue:
## The Vue Version

```html
<!-- The long way -->
<vue-component :class="{
  'classOnlyOnThesePages' :
  $route.name === 'Home' || $route.name === 'Gallery' || $route.name === 'Profile'
}" />


<!-- The short way -->
<vue-component :class="{
  'classOnlyOnThesePages' :  ['Home', 'Gallery', 'Profile'].includes($route.name)
}" />
```

Pretty cool, huh? Super readable!


It pretty much is just this:

```js
const addClassesToThisPage = ( ['Home', 'Gallery', 'Profile'].include($route.name))
```
