---
title: TIL checking falsy values inside an array
date: 2021-12-12
published: true
tags: ['array', 'javascript']
series: false
canonical_url: false
description: "Using the key Boolean to find any true/false statements inside an array"
layout: layouts/post.njk
---


```js
// has falsy values
const theArray = [false, null, 0, 'wizard', undefined, 'barbarian'];

// return true/false if a truthy value exists
const ifAnyIsTrue = theArray.some(Boolean); // true

// return true/false if ALL truthy value exists
const ifAllAreTrue = theArray.every(Boolean); // false

// returns the true values
const giveMeTrueValues = theArray.filter(Boolean);
```
