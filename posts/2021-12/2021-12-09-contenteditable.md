---
title: TIL how to turn anything into a editable text
date: 2021-12-09
published: true
tags: ['html']
series: false
canonical_url: false
description: "So you'd use a 'input' or 'textarea' field to create a space for the end user to add new content. Did you know you can also do with that almost any element?"
layout: layouts/post.njk
---

So you'd use a `input` or `textarea` field to create a space for the end user to add new content.

Did you know you can also do with that almost any element?


```html
<p contenteditable="true" spellcheck="true">
Hey now, you're an allstar!
</p>
```

You can even add a spellcheck feature on it!

<p class="codepen" data-height="300" data-slug-hash="rNGdPEX" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/rNGdPEX">
  Untitled</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

REF:
[mdn - contenteditable](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/contenteditable)
[mdn - spellcheck](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/spellcheck)
