---
title: TIL doing a reverse-search history in bash
date: 2021-12-30
published: true
tags: ['terminal', 'search']
series: false
canonical_url: false
description: "Your bash history is saved with up/down arrow keys. Which means you shouldn't save your passwords on it. But you can use Ctrl+R to open up a search prompt."
layout: layouts/post.njk
---


Your bash history is saved with up/down arrow keys. Which means you shouldn't save your passwords on it.

You can use:
`Ctrl+R`  to do a reverse-search history
(left and right drops you back into your terminal)
