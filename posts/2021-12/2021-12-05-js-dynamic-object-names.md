---
title: TIL You can dynamically name objects
date: 2021-12-05
published: true
tags: ['javascript', 'objects']
series: false
canonical_url: false
description: "Not all Object keys have to be hard-coded strings. To make it dynamic, use [brackets]."
layout: layouts/post.njk
---

You can dynamically name object keys!

Wizards use mana, while barbarians use rage.
You can do something like this!

```js
let magicPoints;

specialPoints = 'mana';

const wizard = {
    health: 20
    spells: 'fireball',
    [specialPoints] : 100

}

specialPoints = 'rage';

const barbarian = {
    health: 100
    skills: 'cleave',
    [specialPoints] : 40

}

```

This is helpful if you were making a 'hero class factory'.

Something like this:

```js

function createHero(class, magicPointName) {...}

```


