---
title: TIL Rule of Three in Refactoring
date: 2021-12-28
published: true
tags: ['programming']
series: false
canonical_url: false
description: "Follow the Rule of Three. It makes it easier to find correct abstractions. Ignore the first. Duplicate the second. Refactor when you see it a third time."
layout: layouts/post.njk
---

The rule was popularized by Martin Fowler in Refactoring[1] and attributed to Don Roberts.

> It states that two instances of similar code do not require refactoring,
> But when similar code is used three times, it should be extracted into a new procedure.

Overall, wait until you see the duplication happening a third time before you introduce your abstraction.

With more occurrences, it will be easier to find the commonalities to extract.

