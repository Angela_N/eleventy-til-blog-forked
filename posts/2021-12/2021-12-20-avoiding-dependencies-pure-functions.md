---
title: TIL Avoiding dependencies in your helper functions
date: 2021-12-20
published: true
tags: ['helpers', 'javascript', 'testing', 'functional-programming']
series: false
canonical_url: false
description: "Impure functions for helpers? What is this... 1999?"
layout: layouts/post.njk
---

This is a impure function.

```js
import VALID_VALUES_LIST from './constants';

function isValueValid( value ) {
    return VALID_VALUES_LIST.includes( value );
}
```

Why? Because it NEEDS `VALID_VALUES_LIST` to work. It depends on it.

One of the super powers of functional programming is pure functions.

```js

function isValueValid( value, validValuesArray = [] ) {
    return validValuesArray.includes( value );
}
```

This is a pure function. You can pass any array in the second param.

Now in your testing library, all of these will work with zero dependencies.

```js
expect( isValueValid( 'hulk', [ 'batman', 'superman' ] ) ).toBe( false );

expect( isValueValid( 'hulk', null ) ).toBe( false );

expect( isValueValid( 'hulk', [] ) ).toBe( false );

expect( isValueValid( 'hulk', [ 'iron man', 'hulk' ] ) ).toBe( true );
```

VIA
[WordPress Block Testing](https://developer.wordpress.org/block-editor/contributors/code/testing-overview/#dependency-injection)
