---
title: TIL a neat way to clone and object and override the items
date: 2021-12-07
published: true
tags: ['javascript', 'objects']
series: false
canonical_url: false
description: "Spread the object into a new object, then override the element later."
layout: layouts/post.njk
---

```js

const wizard = {
    health: 20
    spells: 'fireball',
    armor: 'robes'
}

const betterWizard = {
    ...wizard,
    spells: 'better fireball'
}

console.log(betterWizard);
// { health: 20, spells: 'better fireball' }
```
