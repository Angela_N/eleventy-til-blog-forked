---
title: TIL the Download attribute in links
date: 2021-12-10
published: true
tags: ['html']
series: false
canonical_url: false
description: ' href="link/to/your/file" download="filename.pdf" to make your links auto-download'
layout: layouts/post.njk
---


`<a href="link/to/your/file" download="filename.pdf">Download</a>`

Note: This only works on the same origin requests, not cross origin.

`mysite.com/filename.pdf` works
`google.com/random-internet/filename.pdf` will not work.

REF:
[mdn - attr download](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#attr-download)
[Stack Overflow](https://stackoverflow.com/a/42266268/4096078)
