---
title: TIL that it's normal to feel embarrassed by your code
date: 2021-12-03
published: true
tags: ['quotes', 'podcast', 'code']
series: false
canonical_url: false
description: "If you wait till you’re not embarrassed about your code to show your code, you’ve waited too long."
layout: layouts/post.njk
---

I love this quote from [Amal Hussein](https://twitter.com/nomadtechie):

> if you wait till you’re not embarrassed about your code to show your code, you’ve waited too long.

> Yeah, if you wait till you’re not embarrassed, then you’ve missed so many opportunities for early feedback, that could have maybe even made it better. Because that’s the beautiful thing about feedback - you always are in a better place because of it. The collaborative experience, even if it just shifts you by one degree, you are one degree better than you were without this feedback.

Via [JS Party Podcast episode #201: The inside story on React’s all new docs](https://changelog.com/jsparty/201)
