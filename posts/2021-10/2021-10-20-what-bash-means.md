---
title: TIL what bash means
date: 2021-10-20
published: true
tags: ['bash', 'terminal', 'definition']
series: false
canonical_url: false
description: "Bash means Bourne-again shell. sh was proprietary and not open source, and Bash was created in 1989 to create a free alternative for the GNU project and the Free Software Foundation. Since projects had to pay to use the Bourne shell, Bash became very popular."
layout: layouts/post.njk
---


All shells originate from the Bourne Shell, called `sh`. "Bourne" because its creator was Steve Bourne.

Bash means Bourne-again shell. `sh` was proprietary and not open source, and Bash was created in 1989 to create a free alternative for the GNU project and the Free Software Foundation. Since projects had to pay to use the `Bourne shell`, Bash became very popular.

Via
[The Linux Command Handbook](https://www.freecodecamp.org/news/the-linux-commands-handbook/?utm_source=pocket_mylist)
