---
title: TIL Check if a given element is in focus
date: 2021-10-24
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "The key ingredient is document.activeElement!"
layout: layouts/post.njk
---


The key ingredient is `activeElement`

```javascript
const elementIsInFocus = el => (el === document.activeElement);

elementIsInFocus(anyElement)// returns true if in focus, false if not in focus
```

Via
[21 Useful JS snippets](https://javascript.plainenglish.io/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
