---
title: TIL hoisting based on function declarations & function expressions
date: 2021-10-10
published: true
tags: ['javascript', 'advanced']
series: false
canonical_url: false
description: "Function declarations get hoisted on load. So you can use it anywhere. Function expressions do not. Neither are better than another"
layout: layouts/post.njk
---


## Function Declaration

```javascript
function add (num1, num2) {
	return num1 + num2;
}
```

tl;dr: You 'declare' it with function.


## Function Expression
```javascript

let add = function (num1, num2) {
	return num1 + num2;
};
```

tl;dr: It's expressed as a variable.




## Hoisting
When a JavaScript file (or HTML document with JavaScript in it) is loaded, function declarations are hoisted to the top of the code by the browser before any code is executed.

### Function Declarations

```javascript
/**
 * This works!
 */
function add(num1, num2) {
	return num1 + num2;
}
add(3, 3); // returns 7

/**
 * This does, too!
 */
subtract(7, 4); // returns 3
function subtract(num1, num2) {
	return num1 - num2;
}
```

### Function Expressions

```
/**
 * This works!
 */
let add = function(num1, num2) {
	return num1 + num2;
};
add(3, 3); // returns 7


/**
 * This does not =(
 */
substract(7, 4); // returns Uncaught TypeError: subtract is not a function
let subtract = function (num1, num2) {
	return num1 - num2;
};
```

REFERENCE:
https://gomakethings.com/function-expressions-vs.-function-declarations-revisisted/
