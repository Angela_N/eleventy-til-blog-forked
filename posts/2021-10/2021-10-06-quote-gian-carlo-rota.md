---
title: TIL being a good teacher
date: 2021-10-06
published: true
tags: ['quotes']
series: false
canonical_url: false
description: "A good teacher does not teach facts, he or she teaches enthusiasm, open-mindedness and values."
layout: layouts/post.njk
---

Mathematician and philosopher Gian-Carlo Rota on teaching:
"A good teacher does not teach facts, he or she teaches enthusiasm, open-mindedness and values."
