---
title: TIL Class Versus Function Prototypes
date: 2021-10-19
published: true
tags: ['javascript', 'classes', 'prototype']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


This is the same:

```js

// Class Version
class Person {
  constructor (name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  incrementAge() {
    this.age += 1;
  }
}

// function version
function Person(name, age, gender) {
  this.name = name;
  this.age = age;
  this.gender = gender;
}

Person.prototype.incrementAge = function() {
  return this.age =+ 1;
}

```

REFERENCE:
[Must know JavaScript for react developers](https://medium.com/@SilentHackz/must-know-javascript-for-react-developers-6b98ee053585)
