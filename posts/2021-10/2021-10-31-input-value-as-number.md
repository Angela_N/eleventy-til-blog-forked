---
title: TIL Value as Number
date: 2021-10-31
published: true
tags: ['mdn', 'inputs', 'forms']
series: false
canonical_url: false
description: "Returns the value of the element, interpreted as one of the following, in order: A time value, A number, NaN if conversion is impossible."
layout: layouts/post.njk
---

If you want your input value casted as a number, use `valueAsNumber`

```html
<script>
  const checkMyValueType = (event) => {
    console.log(typeof event.target.value) // string
    console.log(typeof event.target.valueAsNumber) // number
  }
</script>

<input type="number" onkeyup="checkMyValueType(event)" />
```

> double: Returns the value of the element, interpreted as one of the following, in order:
> A time value
> A number
> NaN if conversion is impossible


REFERENCE:
[HTMLInputElement - MDN](https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement)


