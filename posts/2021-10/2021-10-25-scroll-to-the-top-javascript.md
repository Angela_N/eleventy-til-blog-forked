---
title: TIL Scroll to the top of the page
date: 2021-10-25
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "A neat trick to create one of those 'To the top' buttons!"
layout: layouts/post.njk
---

A neat trick to create one of those 'To the top' buttons!

```javascript
const scrollToTopOfDocument = () => {
  const c = document.documentElement.scrollTop || document.body.scrollTop;
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    window.scrollTo(0, c - c / 8);
  }
};

scrollToTopOfDocument();
```


Via
[21 Useful JS snippets](https://javascript.plainenglish.io/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
