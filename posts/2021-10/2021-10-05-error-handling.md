---
title: TIL not ignoring errors
date: 2021-10-05
published: true
tags: ['errors']
series: false
canonical_url: false
description: "Doing nothing with a caught error doesn't give you the ability to ever fix or react to said error."
layout: layouts/post.njk
---

I'm guilty of this.

## Don't ignore caught errors

Doing nothing with a caught error doesn't give you the ability to ever fix or react to said error. Logging the error to the console (console.log) isn't much better as often times it can get lost in a sea of things printed to the console. If you wrap any bit of code in a try/catch it means you think an error may occur there and therefore you should have a plan, or create a code path, for when it occurs.

Bad:
```
try {
  functionThatMightThrow();
} catch (error) {
  console.log(error);
}
```

Good:

```
try {
  functionThatMightThrow();
} catch (error) {
  // One option (more noisy than console.log):
  console.error(error);
  // Another option:
  notifyUserOfError(error);
  // Another option:
  reportErrorToService(error);
  // OR do all three!
}
```

Via [JS Clean Code](https://github.com/ryanmcdermott/clean-code-javascript#dont-ignore-caught-errors)
