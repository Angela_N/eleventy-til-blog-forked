---
title: TIL Netflix' Paved Road
date: 2021-10-13
published: true
tags: ['products', 'netflix', 'projectmanagement']
series: false
canonical_url: false
description: "The Paved Road is a concept in Netflix that allows their teams to use a product or tool that is sort of default for the rest of the company. The product/tool is produced internally and managed internally."
layout: layouts/post.njk
---

The Paved Road is a concept in Netflix that allows their teams to use a product or tool that is sort of default for the rest of the company. The product/tool is produced internally and managed internally.

The benefit of the Paved Road approach at Netflix:

> One especially attractive part of a Paved Road approach for security teams with a large portfolio is that it helps turn lots of questions into a boolean proposition: Instead of “Tell me how your app does this important security thing?”, it’s just “Are you using this paved road product that handles that?”.

Individual Teams may choose not to use the paved road.

via [this](https://netflixtechblog.com/the-show-must-go-on-securing-netflix-studios-at-scale-19b801c86479)


[Conference talk](https://www.oreilly.com/library/view/oscon-2017/9781491976227/video306724.html)
[Slideshare](https://www.slideshare.net/diannemarsh/the-paved-road-at-netflix)
[How We Build Code at Netflix](https://netflixtechblog.com/how-we-build-code-at-netflix-c5d9bd727f15)
