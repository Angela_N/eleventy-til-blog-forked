---
title: TIL Optimizing Closures and backpacks
date: 2019-08-02
published: false
tags: ["js"]
series: false
canonical_url: false
description: "Optimizing functions with Closure"
layout: layouts/post.njk
---

Let's say you created a function called multiply by the power of.

```js

function multiply(y) {
  let x = Math.pow(10,10);
  return x * y;
}

multiply(25); //250,000,000,000
```

Some details:

1. Big numbers require more memory.
2. Javascript functions are created on the stack, then removed.
3. If you called `multiply()` ten times, it creates it 10 times, and allocates the memory for it.

That's a LOT of memory.

Closure:

```js
function multiply() {

  let x = Math.pow(10,10);

  return function(y) {
    return x * y;
  }

}

let multiplyClosureVersion = multiply();

multiplyClosureVersion(25);  //250,000,000,000

```

The difference here is that the stack will not create multiple versions of `  let x = Math.pow(10,10);`. It'll be stored in memory, as it's not recreated with every call.


LINK: Use Closures for Memory Optimizations in Javascript (a case study)
https://dev.to/ahmedgmurtaza/use-closures-for-memory-optimizations-in-javascript-a-case-study-43h9
