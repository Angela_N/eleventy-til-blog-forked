---
title: TIL Closure
date: 2019-08-01
published: true
tags: ["js"]
series: false
canonical_url: false
description: "A closure is a combination of a function bundled together with references to it's surrounding state.  In other words, it has a backpack."
layout: layouts/post.njk
---

A closure is a combination of a function bundled together with references to it's surrounding state.  In other words, it has a backpack.

```js

function adder(a) {
  return function(b) {
    return a + b;
  }
}

let add5 = adder(5);

console.log(add5(5)); // 10

```
What's happening behind the scenes?

`add5` is taking the value of of the function + param.

So `add5` becomes this:
```js

function add5(b) {
  var a = 5;
  return a + b;
}

```

`a` will always equal 5.


LINK: Use Closures for Memory Optimizations in Javascript (a case study)
https://dev.to/ahmedgmurtaza/use-closures-for-memory-optimizations-in-javascript-a-case-study-43h9
