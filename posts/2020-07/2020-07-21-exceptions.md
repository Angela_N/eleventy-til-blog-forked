---
title: TIL Exceptions to the === rule
date: 2020-07-21
published: true
tags: ['javascript', 'wat']
series: true
canonical_url: false
description: "Exceptions to === rule"
layout: layouts/post.njk
---
Exceptions to === rule

```
NaN === NaN is false
(although they are the same value.)

-0 === 0 and 0 === -0 are true
(although they are different values.)


```
let width = 0 / 0; // NaN
let height = width * 2; // NaN because of width

// when compared normally, it's false.
// but when compared with Object.is, it's true.
console.log(width === height); // false
console.log(Object.is(width, height)); // true

```

**Instead, check it with these:**
Number.isNaN(size)
Object.is(size, NaN)
size !== size

Humorously, you could think of Object.is(..) as the “quadruple-equals” ====, the really-really-strict comparison!

[why does -0 exist](https://softwareengineering.stackexchange.com/questions/280648/why-is-negative-zero-important/280708#280708)
