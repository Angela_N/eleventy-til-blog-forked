---
title: TIL Prettier-ignore
date: 2020-07-06
published: true
tags: ['linting']
series: false
canonical_url: false
description: "Ah stop it!"
layout: layouts/post.njk
---

Sometimes, prettier's auto-formatting will reformat something and make it look worse.

Example - This one line of code.
`this.tiles = this.map.addTilesetImage('background', 'background', 32, 32, 1,2 );`

Prettier will turn it into this:
```
this.tiles = this.map.addTilesetImage(
    'background',
    'background',
    32,
    32,
    1,
    2
);
```

To stop that, use the JS comment // prettier-ignore to exclude the next snippet

```
// prettier-ignore
this.tiles = this.map.addTilesetImage('background', 'background', 32, 32, 1,2 );
```

Of course, now I have this issue.
```
// prettier-ignore
this.tiles = this.map.addTilesetImage('background', 'background', 32, 32, 1,2 );

// prettier-ignore
this.backgroundLayer = this.map.createStaticLayer( 'background', this.tiles, 0, 0);

// prettier-ignore
this.blockedLayer = this.map.createStaticLayer( 'blocked', this.tiles, 0, 0);
```
A crappy workaround is to put it in a `{}` (per this [comment](https://github.com/prettier/prettier/issues/5287#issuecomment-545357716)). The next best thing is implementing it yourself, as the Prettier team could use help.

NOTE: There's also HTML, CSS, JSX, etc versions as well.

[Documentation](https://prettier.io/docs/en/ignore.html)
