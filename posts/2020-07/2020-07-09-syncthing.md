---
title: TIL about syncthing
date: 2020-07-09
published: true
tags: ['devops', 'windows', 'linux', 'mac']
series: false
canonical_url: false
description: "Sync all the things!"
layout: layouts/post.njk
---

> Syncthing is a continuous file synchronization program. It synchronizes files between two or more computers in real time, safely protected from prying eyes. Your data is your data alone and you deserve to choose where it is stored, whether it is shared with some third party, and how it's transmitted over the internet.

tl;dr: Think of it like Dropbox, but the server is your own local computer. And it keeps all your files together.

Usecases:
* You're coding on a Windows, need to debug on a Mac, and bring your files to a Linux computer.
* You want all of your dev files to stay together. (And ignore certain patterns like node_modules)

It's Private (Local storage, Encrypted, authenticated), and Open (Open protocol, Open Source, Open Development).

[Syncthing](https://syncthing.net/)
