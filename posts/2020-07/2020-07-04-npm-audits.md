---
title: TIL NPM Audit Fix
date: 2020-07-04
published: true
tags: ['git', 'npm', 'basics']
series: false
canonical_url: false
description: "Update your security packs"
layout: layouts/post.njk
---

You `npm install` and NPM warns you that there are 100 vulnerabilities. (npm -v 6 or higher).

If you're worried about potential damage, create a new branch.

`git checkout -b fix/vulnerabilities`

Then  `npm audit fix` - This will fix all vulnerabilities that wont cause any funky side effects. It will target versions of packages that <g-link to="/content/posts/2020-06-14-semver.md">semver</g-link> that uses minor/patch versions to update.

It's not 100% fullproof (which is why you branched it), and you'll still have a lot of vulnerabilities afterwards.

Next, use `npm audit` to get a list of other vulnerabilities. Scroll to the top, and you'll see NPM recommend a action like `npm install <package@version> to fix 7 vulnerabilities`. These are often major updates that can cause side effects. Do that one at a time, and test your code.

If there is no update - it's your decision to leave it alone or find a new package/solution to your problem.
