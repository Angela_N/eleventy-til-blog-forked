---
title: TIL WordPress direct page ID
date: 2020-07-05
published: true
tags: ['wordpress']
series: false
canonical_url: false
description: "WordPress Haxxs!"
layout: layouts/post.njk
---

Once in a while, I'd find a conditional like `is_page('42')`.

WordPress stores all content as IDs, from comments, to posts & pages.

42 is difficult to track down.
To access that page in WordPress (if it IS a page/post), do the below.

```
http://wordpress.loc/?page_id=16
```

[reference](https://wordpress.stackexchange.com/questions/264482/access-post-from-post-id-in-url)
