---
title: TIL Finding Header information on any website
date: 2020-07-17
published: false
tags: ['webdev']
series: false
canonical_url: false
description: "Don't save API keys in your header"
layout: layouts/post.njk
---

!(find-header)[2020-07-17-header-opt.gif]

You can find header data on any website by:

1. Visiting Devtools > Network
2. Refresh the page to load all files
3. Find the Type->Document (which is probably the webpage itself)
4. Visit the Headers tab

You can find all sorts of helpful info, like what type of server it is. Or if they pass secret API keys.

[Why you should never put secrets in URL query params](https://www.fullcontact.com/blog/2016/04/29/never-put-secrets-urls-query-parameters/)
