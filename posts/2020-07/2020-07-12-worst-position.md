---
title: TIL about the worst job position ever
date: 2020-07-12
published: true
tags: ['webdev', 'humor']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

I found this on Reddit, and I wish I could remember where. I shared the link to my friends because of how wild it was.

Fortunately, they did revise it.

I'm not using it to mock them.
This is more of an exercise of how many red flags can appear in a application.

[The application](https://web.archive.org/web/20200612162539/https://universalwebdesign.co.uk/job/senior-web-developer/)

Some red flags:
* Base Salary: £ 30,000, or $37,000 USD ([average according to this site is £ 45,000](itjobswatch.co.uk/jobs/uk/web%20developer.do) )
* Confident to call people you do not know
* Willing to work extra hours if needed
* Expectations to work hard and to work fast
* To smash targets and exceed expectations as an individual and team
* General Office Tasks include Answering the telephone and Transferring calls (!!!)
* General Client Related Tasks include Calling new customers, Calling existing customers, Retaining clients long term, Attending client meetings
* Work-Related tasks include building new websites, maintaining websites, creating email accounts, SETTING UP PCS, creating plugins, creating themes, creating mobile apps, managing mobile apps, Google Ads work, Tracking conversions with Google Ads, A/B testing, support sales team, keep spreadsheets up to date, track your time using a diary
* Set repeat tasks to avoid ever forgetting what needs doing
* Expert in WordPress, Magento, Drupal, Concrete5, Laravel, Joomla
* Expert in HTML, CSS, PHP, Javascript, MySQL, PHPMyAdmin, JQuery
* Being able to speak multiple languages (Preferred)
* A minimum of 10 years of web development experience
* A minimum of 10 years of experience using the latest technologies
* And just to repeat it back: Base Salary: £ 30,000, or $37,000 USD
