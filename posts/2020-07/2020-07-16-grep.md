---
title: TIL what grep means
date: 2020-07-16
published: true
tags: ['linux']
series: false
canonical_url: false
description: "What does grep mean"
layout: layouts/post.njk
---

When you are searching for a specific thing in linux, you may use the

`grep`

command.

> This means "search globally for matches to the regular expression re, and print lines where they are found". There are various command line switches available when using grep that modify the default behavior.

Examples:

**To search all files in the current directory for the string "car".**

`grep car *`

**To search all files in the current directory and all its subdirectories (recursive) for the string "car".**

`grep -r car .`

(The character "." is a relative path from the current directory to itself.)

This Show all mozilla related processes running.

`ps aux | grep mozilla`

Find all files containing the word bleh.

`grep bleh -R /*`

```

[reference](https://linux.fandom.com/wiki/Grep)
