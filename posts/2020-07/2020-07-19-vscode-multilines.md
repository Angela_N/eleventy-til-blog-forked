---
title: TIL VSCode Multi-lines
date: 2020-07-19
published: true
tags: ['vscode']
series: false
canonical_url: false
description: "Have multiple cursors"
layout: layouts/post.njk
---

![](/src/img/2020-07-19-multilines-opt.gif)


Use-cases:
* You have to declare a lot of variables at once, and want them on their own separate line.
* You have to fix a lot of issues in the same exact spot, but different lines.

Bonus tip: You can use ALT+LEFT-CLICK to create multiple cursors in different places too.
