---
title: TIL Single Responsibility Principle
date: 2020-07-26
published: true
tags: ['webdev']
series: false
canonical_url: false
description: "Single Responsibility Principle"
layout: layouts/post.njk
---
> What is the responsibility of your class/component/microservice?
> If your answer includes the word “and”, you’re most likely breaking the single responsibility principle. Then it’s better to take a step back and rethink your current approach. There is most likely a better way to implement it.

A quote from the blogpost: [Single Responsibility Principle](https://stackify.com/solid-design-principles/).
