---
title: TIL the hotkey for collapsing all codeblocks within VSCode
date: 2020-07-25
published: true
tags: ['vscode']
series: false
canonical_url: false
description: "Have multiple cursors"
layout: layouts/post.njk
---

```
Ctrl + k + [0, 1, 2, 3...] for depth

```

For a list of all shortcuts, check out: `ctrl + k + s`

[Collapse all codeblocks](https://stackoverflow.com/questions/42660670/collapse-all-methods-in-visual-studio-code?rq=1)
