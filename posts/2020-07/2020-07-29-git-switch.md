---
title: TIL git switch
date: 2020-07-29
published: true
tags: ['git']
series: false
canonical_url: false
description: "No more checkout. Only git switch."
layout: layouts/post.njk
---
These two commands replaces two common uses of git checkout.

`git switch` to switch to a new branch after creating it if necessary

```
git switch my-branch              # same as git checkout my-branch
git switch -c my-branch           # same as git checkout -b my-branch
git switch -c my-branch HEAD~3    # branch off HEAD~3
git switch --detach HEAD~3        # checkout commit without branching

```

`git restore` to restore changes from a given commit.

```
git restore --source HEAD~3 main.c  # --worktree is implied

rm -f hello.c
git restore hello.c

git restore '*.c'

git restore --staged hello.c    # same as using git reset

git restore --source=HEAD --staged --worktree hello.c   # same as git checkout
```
[Reasoning](https://stackoverflow.com/questions/57265785/whats-the-difference-between-git-switch-and-git-checkout-branch)
[reference](https://www.infoq.com/news/2019/08/git-2-23-switch-restore/)
