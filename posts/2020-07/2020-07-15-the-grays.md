---
title: TIL Why gray is darker than darkgray
date: 2020-07-15
published: true
tags: ['webdev']
series: false
canonical_url: false
description: "Colors you so crazy"
layout: layouts/post.njk
---

Lighter Gray:
`color: darkgray;`

Darker Gray:
`color: gray`

!(grays)[2020-07-15-gray.png]

Gray       '#808080'  (128,128,128)
DarkGray   '#A9A9A9'  (169,169,169)  //equal to X11 DarkGray
Silver     '#C0C0C0'  (192,192,192)  //close to X11 Gray (190,190,190)
LightGray  '#D3D3D3'  (211,211,211)  //equal to X11 LightGray


Why?

> These colours don't actually originate in HTML` - they date still further back than that to the X Window System that originated on UNIX systems. The HTML specification defines sixteen named colours that map onto the basic sixteen colours present in the EGA palette, but the earliest browsers such as Mosaic also supported any of the other X11 named colours, based on their colour representation as defined on X. Unfortunately, some of the original sixteen named colours have different representations to the X11 equivalents, for example Green is represented in X11 in this colour, whereas in HTML it's represented in this colour. The unfortunate result is that Gray is defined as #808080 in HTML, but DarkGray is represented as #A9A9A9, meaning that they're the wrong way around. Since WPF allows the same named colours as HTML for compatibility, the result is that the same idiosyncrasies carry forward. (You can find more information on the full set of X11 colour names and their representations in Wikipedia).


(reference)[https://stackoverflow.com/questions/3811973/why-is-darkgray-lighter-than-gray]
