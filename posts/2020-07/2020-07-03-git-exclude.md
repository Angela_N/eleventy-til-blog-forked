---
title: TIL Ignoring files in a repo without .gitignore
date: 2020-07-03
published: true
tags: ['git']
series: false
canonical_url: false
description: ".gitignore"
layout: layouts/post.njk
---

USECASE:
You're working on a project that's uploaded to bitbucket/github.

You have a folder called 'repo-notes-for-myself' but you don't want to upload it to `main`.

You can either add the folder `repo-notes-for-myself` to `.gitignore`. But then that ignore file will be added to the repo.

What if you add it to a localized ignore file that isn't uploaded?
`.git > info > exclude` is a text file that you can modify to exclude specific files.

```
# git ls-files --others --exclude-from=.git/info/exclude
# Lines that start with '#' are comments.
# For a project mostly in C, the following would be a good set of
# exclude patterns (uncomment them if you want to use them):
# *.[oa]
# *~
```

[github help](https://help.github.com/en/github/using-git/ignoring-files#excluding-local-files-without-creating-a-gitignore-file)
