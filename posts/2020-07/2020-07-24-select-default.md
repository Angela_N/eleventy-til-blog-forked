---
title: TIL the default is the first item
date: 2020-07-24
published: true
tags: ['webdev', 'html']
series: false
canonical_url: false
description: "Select all the html"
layout: layouts/post.njk
---
```
<select>
  <option> I'm default if there's no selected </option>
  <option selected> But I'm default because there IS a selected</option>
  <option> I'm a third option and I suck at parties</option>
</select>
```
