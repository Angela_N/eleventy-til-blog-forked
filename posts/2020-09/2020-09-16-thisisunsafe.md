---
title: TIL thisisunsafe
date: 2020-09-16
published: true
tags: ["devops"]
series: false
canonical_url: false
description: "If you are not a developer and you stumbled on this tip... you should understand that this is a BAD IDEA."
layout: layouts/post.njk
---

!()[https://res.cloudinary.com/practicaldev/image/fetch/s--zTMMH7Ay--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/m0a8ey0teb7i9psaot0k.jpeg]

If you are not a developer and you stumbled on this tip... you should understand that this is a BAD IDEA.

If you get that error:
> You type "thisisunsafe" while on the page, and it redirects you to the site.

This is helpful if you're deploying a local site, and you have SSH issues.



REFERENCE:
https://dev.to/brettimus/this-is-unsafe-and-a-bad-idea-5ej4
