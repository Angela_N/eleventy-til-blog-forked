---
title: TIL VScode bracket formatting for PHP
date: 2020-09-10
published: true
tags: ["php", "vscode"]
series: false
canonical_url: false
description: "Intelephense -> Format: Braces: k&r "
layout: layouts/post.njk
---


DEFAULT:
```php
function test()
{

}
```

FOR A MORE REFINED BEING:
```php
function test() {

}
```

HOW-TO:
![](https://i.stack.imgur.com/07utT.png)


REF:
https://stackoverflow.com/a/66115402/4096078
