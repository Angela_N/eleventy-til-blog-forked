---
title: TIL how to get WordPress functions working in VSCode
date: 2020-09-09
published: true
tags: ["vscode", "php"]
series: false
canonical_url: false
description: "Right out of the box, PHP Intelephense Extension for VSCode will give errors for WordPress code. How to fix."
layout: layouts/post.njk
---

WordPress has a bunch of built-in functions/hooks to help with development.

But right out of the box, with the [PHP Intelephense Extension for VSCode](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client), it'll still give errors for WordPress code.

For WordPress Devs using VSCode,
You have to manually add wordpress as a `stub`.

REF:
https://stackoverflow.com/questions/59890854/vs-code-highlighted-all-my-wordpress-function-name
