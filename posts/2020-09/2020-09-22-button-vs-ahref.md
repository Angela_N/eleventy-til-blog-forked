---
title: TIL When to use a button versus an "a href" link
date: 2020-09-22
published: true
tags: ["html", "accessibility"]
series: false
canonical_url: false
description: "When to use one or the other"
layout: layouts/post.njk
---

The best route to go in:

If it takes you to another route/page/website, or a part of a website, use a `href`.

If it's a function, maybe it sends data (a form submission) or fires an action (toggles dark mode/light mode, search box, hide/show content), a `button` is ideal.
## The debate:

> A link is focusable and can be triggered by the enter key. A link will redirect you to a new page or a section within the same page. In VoiceOver's rotator, it will also be collected within the "Links" window.

> A button is focusable, too, and can be triggered by the space key. A button will trigger an action like opening or closing something. Or sending a form. In most cases JavaScript is used to do this. In VoiceOver's rotator, it will be collected within the "Form controls" window. That alone says something.

[via a11ly-101.com](https://a11y-101.com/design/button-vs-link)

## Don't use divs

> Elements like <div> and <span> are for layout only. They’re semantically meaningless, they don’t have keyboard or touch support in any browser, and they don’t communicate anything to the accessibility API. For example, never use a div or span for a button when you could use a semantically meaningful button element.

[via web-accessibility.carnegiemuseums.org](http://web-accessibility.carnegiemuseums.org/foundations/semantic/#:~:text=Elements%20like%20and%20%3C,a%20semantically%20meaningful%20button%20element.)
