---
title: TIL the difference between strong and i
date: 2020-09-21
published: true
tags: ["html", "css", "accessibility"]
series: false
canonical_url: false
description: "When to use strong vs i"
layout: layouts/post.njk
---

## strong vs i.
The `<b>` and `<strong>` tags by default make text bold.
The `<i>` and `<em>` tags by default make text italicized.

> As far as accessibility goes, while a particular screen reading software may or may not pronounce it differently, using `<strong>` or `<em>` at least opens the possibility, whereas a screen reader will never pronounce text within `<b>` or `<i>` differently.

REF:
Via https://thisthat.dev/b-i-vs-strong-em/
