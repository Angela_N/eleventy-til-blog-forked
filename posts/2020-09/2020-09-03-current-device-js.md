---
title: TIL the JS Library Current Device
date: 2020-09-03
published: true
tags: ["js"]
series: false
canonical_url: false
description: "Current Device JS Library lets you target specific devices"
layout: layouts/post.njk
---

I worked on a project where android phones and iOS Tablets were giving me WILDLY different results. [WebKit Safari bug](/2020-05-25-webkit-100vh);


This libary does the following:

1. It injects the OS, device type and position into the html element.

`<html class="ios tablet landscape">`

So you can do hacky things like:

```css
.ios body {
  background-color: red;
}
```

2. You also can fire specific JS code based on that as well.

```js

body.addEventListener("click", () => {

  if (device.landscape()) {
    console.log("I'm in landscape!")
  }

});

```

You really shouldn't be targetting this level of specificity.
But then again, WebKit (Safari iOS) is a pain in the ass.

**Reference**
https://matthewhudson.github.io/current-device/
