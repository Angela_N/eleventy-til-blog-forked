---
title: TIL about how to quickly add a WP Theme customizer Option
date: 2020-09-11
published: true
tags: ["wordpress", "php"]
series: false
canonical_url: false
description: "You can do a lot of theme customization that auto-loads the page with every setting change. It's a really nice feature."
layout: layouts/post.njk
---

In WordPress, when you go to

Appearances > Customize:

You can do a lot of theme customization that auto-loads the page with every setting change.
It's a really nice feature.

As a developer, maybe you want to include a custom setting in the WordPress Theme Customizer, using the WordPress Theme Customizer API.

AN EXAMPLE:
```php
/**
 * Typically in your functions.php
 * Create Logo Setting and Upload Control.
 */
function tracy_new_customizer_settings($wp_customize) {
    // add a setting for the site logo
    $wp_customize->add_setting('add_theme_logo');
    // Add a control to upload the logo
    $wp_customize->add_control(new WP_Customize_Image_Control(
        $wp_customize,
        'add_theme_logo',
        array(
            'label' => 'Add Banner Logo (1200px)',
            'section' => 'title_tagline',
            'settings' => 'add_theme_logo',
        )
    ));
}

add_action('customize_register', 'tracy_new_customizer_settings');
```

```php
/**
 * Typically in your page-template.php
 * Call your new theme option element.
 */

<?php if (get_theme_mod('add_theme_logo')): ?>

    <img src="<?php echo get_theme_mod('add_theme_logo'); ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">

<?php endif; ?>

```

REF:
https://codex.wordpress.org/Theme_Customization_API
