---
title: TIL of how to generate ASCII folder structures
date: 2020-09-08
published: true
tags: ["devtools"]
series: false
canonical_url: false
description: "It's common to explain or discuss a file system structure "
layout: layouts/post.njk
---

It takes input like this:

```
my-project
  src
    index.html
    my-project.scss
  build
    index.html
    my-project.css
```

... and transforms it into an ASCII tree diagram like this:

```
.
└── my-project/
    ├── src/
    │   ├── index.html
    │   └── my-project.scss
    └── build/
        ├── index.html
        └── my-project.css
```

REF:
https://github.com/nfriend/tree-online
