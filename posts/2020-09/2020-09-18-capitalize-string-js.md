---
title: TIL the Capitalize String shortcut
date: 2020-09-18
published: true
tags: ["js"]
series: false
canonical_url: false
description: "JavaScript doesn’t have a function that allows you to capitalize a string"
layout: layouts/post.njk
---

I use this a bunch. But i also always forget it.

> Unlike other popular programming languages like Python, C#, and PHP JavaScript doesn’t have a function that allows you to capitalize a string. However, it’s quite a basic function that gets used a lot. You can put in a word or a complete sentence to this function, as long as it’s a string.

```
const capitalize = (str) => str.charAt(0).toUpperCase() + str.slice(1);
```

REF:
https://javascript.plainenglish.io/15-helpful-javascript-one-liners-946e1d1a1653
