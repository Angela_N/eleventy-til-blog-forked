---
title: TIL Categorizing code
date: 2020-09-27
published: true
tags: ["codetheory", "dev"]
series: false
canonical_url: false
description: "I find myself wanting to refactor everything that doesn't move right. I like this post because it doesn't treat all code blocks as equal."
layout: layouts/post.njk
---

I find myself wanting to refactor everything that doesn't move right.

I like this post because it doesn't treat all code blocks as equal.



(Yoinked from: https://css-tricks.com/the-three-types-of-code/)

When looking at a new code base:

Thinking about these different categories has helped me figure out what to refactor and prioritize, and what to leave alone for now.
Those categories: Boring Code, Salt Mine Code, and Radioactive Code (Renewable Code).

## Boring Code:
Boring code is when it makes perfect sense when you read it. There’s no need to refactor it, and it performs its function in a way that doesn’t make you want to throw yourself into a river. Boring code is good code. It doesn’t do a kick-flip and it’s not trying to impress you. You can use it without having to write even more code or engineer hacks on top of it. Boring code does exactly what it says on the tin and never causes any surprises.

## Salt Mine Code
It’s this type of code that we can mostly forget about. It’s the type of code that is dangerous if opened up and tampered with, but for now, everything is okay.

## Radioactive Code (aka Renewable code)

> Radioactive code is the real problem at the heart of every engineering team. It’s the let’s-not-go-to-work-today sort of code. It’s the stuff that is not only bad but is actively poisoning our codebase and making everything worse over time.

NOTE: I don't really agree with this description. In the comments, I actually like Lucas' response:

> I plead for
Boring code
Radioactive code
Renewable code


