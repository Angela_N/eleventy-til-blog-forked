---
title: TIL Bootstrap 5 removing jQuery will reduce about 83kb of loading time
date: 2020-09-25
published: true
tags: ["frameworks", "css"]
series: false
canonical_url: false
description: "Bootstrap 5-alpha1 has been officially launched dropping jQuery as a dependency and also removing browser support for Internet Explorer 10 and 11."
layout: layouts/post.njk
---

Switching from Bootstrap 4 to Bootstrap 5:

> On 16th June of 2020, Bootstrap 5-alpha1 has been officially launched dropping jQuery as a dependency and also removing browser support for Internet Explorer 10 and 11. This came as a big surprise to the developers, especially the decision to no longer support Internet Explorer browsers.

> Removing jQuery as a dependency comes as a welcoming update for two main reasons:

> You will save up to 82.54 KB for the loading time
Many of the features of jQuery can now be done natively with Vanilla Javascript
You can read a tutorial on Bootstrap 5 and how to use it without jQuery.

REFERENCE:
https://themesberg.com/blog/tutorial/bootstrap-5-tutorial
https://themesberg.com/knowledge-center/bootstrap/what-is-bootstrap
