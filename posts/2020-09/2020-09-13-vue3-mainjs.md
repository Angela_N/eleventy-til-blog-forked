---
title: TIL chaining Vue 3 Main.js
date: 2020-09-13
published: true
tags: ["vue", "js"]
series: false
canonical_url: false
description: "Vue's Main.js file looks slightly different. Here is how to modify it."
layout: layouts/post.njk
---

Pretty newbie unknown.

When you scaffold a Vue 3 project, your main js file looks like this.

Within `main.js`:

```js
import { createApp } from "vue";
import App from "./App.vue";

createApp(App).mount("#app");
```

But let's say you wanted to get axios or whatever plugin included.
[Axios-vue](https://www.npmjs.com/package/vue-axios) wants:

```js
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = Vue.createApp(...)
app.use(VueAxios, axios)
```

It's essentially chaining commands.
So to make it follow that Vue 3 pattern that was established:

```js
import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

createApp(App).use(VueAxios, axios).mount("#app");
```

Via:
https://stackoverflow.com/a/64273694/4096078
