---
title: TIL adding WP_HOME & WP_SITEURL
date: 2020-09-15
published: true
tags: ["wordpress"]
series: false
canonical_url: false
description: "You can add WP_HOME and WP_SITEURL to wp-config.php to override your WP database"
layout: layouts/post.njk
---

You can add WP_HOME and WP_SITEURL to wp-config.php to override your WP database.

```
define( 'WP_HOME', "http://mystagingsite.local");
define( 'WP_SITEURL', "http://mystagingsite.local");
```

REFERENCE:
https://kinsta.com/knowledgebase/wordpress-change-url/#change-wordpress-url-in-wpconfigphp-file

