---
title: TIL Adding a repo as a dependency in package.js
date: 2020-06-17
published: true
tags: ['javascript', 'npm']
series: false
canonical_url: false
description: "If they aren't published as a NPM, you can still do this"
layout: layouts/post.njk
---

npm install --save username/repo#branch-name-or-commit-or-tag

The above installs a node package from github into your node_modules folder and automatically adds the dependency to your package.json! It’s marginally safer to install at a specific commit hash or git tag instead of a branch name. This way your dependency is tied to a specific commit, instead of whatever the latest commit is on a branch.

[reference](https://medium.com/@jonchurch/use-github-branch-as-dependency-in-package-json-5eb609c81f1a#:~:text=To%20npm%20install%20a%20public,repo%23branch%2Dname%20format.&text=Run%20npm%20install%20and%20npm,into%20your%20%2Fnode_modules%2F%20folder.).
