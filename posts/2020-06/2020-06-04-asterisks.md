---
title: TIL How to Un-asterisk a password
date: 2020-06-04
published: true
tags: ['webdev']
series: false
canonical_url: false
description: "Password asterisks!"
layout: layouts/post.njk
---

Sometimes, you want the password to something that is in asterisks.

For example: I needed to grab the api password of a site in production to use in local. The password is saved on the server, but in asterisks. This is how you un-mask it.

!(animated gif)[how-to-asterisks.gif]

Note: This is a old password used for a service at my old job. I no longer use that site.

