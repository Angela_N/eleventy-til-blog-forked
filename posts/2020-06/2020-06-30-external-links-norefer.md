---
title: TIL External links should not be "=_target"
date: 2020-06-30
published: true
tags: ['webdev']
series: false
canonical_url: false
description: target=_blank bad for external links
layout: layouts/post.njk
---

> When you link to a page on another site using the target="_blank" attribute, you can expose your site to performance and security issues:

> * The other page may run on the same process as your page. If the other page is running a lot of JavaScript, your page's performance may suffer.
> * The other page can access your window object with the window.opener property. This may allow the other page to redirect your page to a malicious URL.

Adding `rel="noopener"` or `rel="noreferrer"` to your `target="_blank"` links avoids these issues.


[web.dev](https://web.dev/external-anchors-use-rel-noopener/)
