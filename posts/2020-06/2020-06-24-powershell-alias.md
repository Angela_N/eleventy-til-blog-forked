---
title: TIL Aliasing commands in Powershell
date: 2020-06-24
published: true
tags: ['terminal', 'win10', 'timesaver']
series: false
canonical_url: false
description: "Save time and brain power"
layout: layouts/post.njk
---

If you happen to use a lot of the same commands in Windows PowerShell, look into creating alias.


```
//In a Win10 Powershell
notepad $profile
```

That will open up notepad file. That file is what Powershell will look at before loading.
In that file, paste this in:
```
function goSomewhereThenOpenGoogleThenDeleteSomething {
    cd C:\Users\
    Start-Process -FilePath "http://www.google.com"
    rm fileName.txt
}

Set-Alias google goSomewhereThenOpenGoogleThenDeleteSomething
```

Now you can type the word "google" into Windows PowerShell, it will:

1. change directory
2. open a browser page that points to google.com
3. remove a fileName.txt from that user folder.

I use docker/devilbox config. This is my shortcut:

```
function startDocker {
    cd E:\devilbox
    docker-compose up -d bind httpd mysql php
    Start-Sleep -Second 30
    Start-Process -FilePath "http://localhost/"
}

Set-Alias start-docker startDocker
```
That will change into a directory, turn on my docker, wait 30 seconds (which is roughly how long it takes), and then load a browser page with my docker/devilbox configuration.

Via [StackOverflow](https://stackoverflow.com/questions/24914589/how-to-create-permanent-powershell-aliases)

I had to modify security settings too. https://stackoverflow.com/a/50309277/4096078

NOTES:
1. Be sure to restart powershell so it loads up the file again.
2. I used a wait method because i'm still learning PowerShell commands. A better option would be to use a 'when the previous command is finished, then excute the next one'.

