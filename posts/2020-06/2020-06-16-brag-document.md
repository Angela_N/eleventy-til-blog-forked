---
title: TIL Brag Documents
date: 2020-06-16
published: true
tags: ['workplace']
series: false
canonical_url: false
description: "Record your small wins"
layout: layouts/post.njk
---

You don't remember everything you did, or the growth you've made.

**Benefits:**

* Negotiating a raise or getting a promotion will become easier. In fact, come performance review time, even your boss will thank you for it.
* Explain the big picture.
* Identifying what you enjoy doing. Your wins are likely a representation of tasks you enjoyed. And you should be very proactive about focusing on those tasks going forward.

**What it can look like:**

* Results - Contributed to a project? What happened?
* Collaboration & Mentorship - Did you help a coworker with how a API works? Anticipate a nasty bug and proactively reached out about it?
* Design & Documentation - Did you write out a how-to or added something to the entire company's body of knowledge? Improved the code quality?
* What you learned - Any new frameworks or little feature that surprised you?
* Outside of Work - blog posts? Talks? Open source work?

Over time - these will add up.

[Julia Evan's Blog](https://jvns.ca/blog/brag-documents/#template)
