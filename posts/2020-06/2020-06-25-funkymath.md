---
title: TIL JS's funky decimal math
date: 2020-06-25
published: true
tags: ['javascript', 'gotchas']
series: false
canonical_url: false
description: "JS you so crazy"
layout: layouts/post.njk
---

Here is a snippet that demonstrates it:
console.log(0.1 + 0.2 === 0.3); // false
console.log(0.1 + 0.2 === 0.30000000000000004); // true

const money = 600.90;
const winePrice = 200.30;
const total = winePrice * 3;

// Outputs: false
console.log(money >= total);

// Outputs: 600.9000000000001
console.log(total);

>Because integers can be represented perfectly accurately, just shifting our perspective to treat cents as the basic unit of currency means that we can often avoid the problems of comparisons in floating point arithmetic.

[reference](http://adripofjavascript.com/blog/drips/avoiding-problems-with-decimal-math-in-javascript.html)
