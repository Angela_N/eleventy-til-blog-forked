---
title: TIL The Boyscouts Rule
date: 2020-06-15
published: true
tags: ['basics']
series: false
canonical_url: false
description: "Always leave the code you're editing a little better than you found it. - Robert C Martin (Clean Code)"
layout: layouts/post.njk
---

Always leave the code you're editing a little better than you found it. - Robert C Martin (Clean Code)

Your goal isn't to add more smelly code.

As you add new features/solve bugs - try to find opportunities to clean up too and remove the rot. As more developers (who follow this method) continue to come in and squash bugs, we make the entire project cleaner, bit by bit.

[medium blog](https://medium.com/@biratkirat/step-8-the-boy-scout-rule-robert-c-martin-uncle-bob-9ac839778385)
