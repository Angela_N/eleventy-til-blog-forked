---
title: TIL Static Site Generators
date: 2020-06-20
published: true
tags: ['javascript', 'webdev']
series: false
canonical_url: false
description: "Easily make websites"
layout: layouts/post.njk
---

These let you make sites quickly.

| Service | install command | Benefits |
| --- | --- | --- |
| [Gridsome](https://gridsome.org/docs/) | `npm install --g @gridsome/cli` then `gridsome create <project-name>`|  Static site Vue + GraphQL  |
| [Nuxtjs](https://nuxtjs.org/guide/installation) | `npx create-nuxt-app <project-name>` | Full-stack Vue |
| [VuePress](https://vuepress.vuejs.org/) | `npm install -g vuepress` then `vuepress dev` | Static Site Vue / Mostly for documentation |
| [Gatsby](https://www.gatsbyjs.org/docs/quick-start/) | `npm install -g gatsby-cli` then `gatsby new <project-name>` | Static site React + GraphQL |
| [Nextjs](https://nextjs.org/docs/getting-started) | `npx create-next-app <project-name>` | Full-stack React |
| [Sapper](https://sapper.svelte.dev/) | `npx degit "sveltejs/sapper-template#webpack" <project-name` | Site with Svelte |


<!-- | [Vue Create](https://cli.vuejs.org/guide/creating-a-project.html) | `npm install -g @vue/cli` then `vue create <project-name>` | Default Vue | -->
<!-- | [Preact](https://preactjs.com/guide/v10/getting-started) | `npm install -g preact-cli` then `preact create default <project-name` | Lightweight React | -->
<!-- | [Create-React-App](https://reactjs.org/docs/create-a-new-react-app.html) | `npx create-react-app <project-name>` | Default React | -->

[Popularity check here](https://2019.stateofjs.com/back-end/)
