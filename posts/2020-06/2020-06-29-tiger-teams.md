---
title: TIL of the Tiger Team Approach
date: 2020-06-29
published: true
tags: ['processes', 'workplace']
series: false
canonical_url: false
description: "JS you so crazy"
layout: layouts/post.njk
---

Tiger teams are formed to help solve a critical issue after the most likely solutions have been attempted. Tiger teams usually focus on important, high-profile, high-impact, mission-critical projects. (In other words, you wouldn’t form a tiger team to tackle your regular day-to-day projects—think of them more like an elite force.)

They typically have a short lifespan of a few months/years until the problem is solved.

**The tiger team approach**

1. Observe and document symptoms and their impact.
2. Identify possible causes.
3. Develop tests to validate those causes.
4. Decide which tests to perform based on organizational priorities.
5. Conduct testing until root cause is confirmed.
6. Outline possible solutions.
7. Agree on a solution and implement it.
8. Document results.

[Reference](https://www.lucidchart.com/blog/what-is-a-tiger-team#:~:text=Tiger%20teams%20are%20formed%20to,impact%2C%20mission%2Dcritical%20projects.)
