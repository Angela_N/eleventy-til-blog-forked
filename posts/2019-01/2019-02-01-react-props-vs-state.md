---
title: TIL React - SetState
date: 2019-02-01
published: false
tags: ['react']
series: false
canonical_url: false
description: "Fetch this data"
layout: layouts/post.njk
---

`props` get passed to the component (similar to function parameters).
`state` is managed within the component (similar to variables declared within a function).

[more info](https://github.com/uberVU/react-guide/blob/master/props-vs-state.md)

Calls to `setState` are asynchronous - don’t rely on `this.state` to reflect the new value immediately after calling `setState`. Pass an updater function instead of an object if you need to compute values based on the current state (see below for details).

```

//won't work
incrementCount() {
  this.setState({count: this.state.count + 1});
}

//will work
incrementCount() {
  this.setState((state) => {
    // Important: read `state` instead of `this.state` when updating.
    return {count: state.count + 1}
  });
}

handleSomething() {
  // Let's say `this.state.count` starts at 0.
  this.incrementCount();
  this.incrementCount();
  this.incrementCount();
}

```
