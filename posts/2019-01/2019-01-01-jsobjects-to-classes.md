---
title: TIL JS Objects to Classes
date: 2019-01-01
published: false
tags: ['javascript', 'basics']
series: false
canonical_url: false
description: "It's all sugar baby"
layout: layouts/post.njk
---

Turn this into from Object constructor to class constructor
remember it's just synatic sugar


## Before
```
function gameCharacter (name, xPos, health) {
    this.name = name;
    this.xPos = xPos;
    this.health = health;
    this.move = function(x) {
        this.xPos += x;
    };
    this.class = "Wizard";
}

var gc1 = new gameCharacter("Nimish", 0, 100);
```

## After
It's the same.
```
class gameCharacter {
    constructor(name, xPos, health) {
        this.name = name;
        this.xPos = xPos;
        this.health = health;
        this.class = "Wizard";
    }

    move(x) {
        this.xPos += x;
    };
}

var gc1 = new gameCharacter('Nimish', 0, 100);
```

What about prototypes?
With JS Classes, it's called Extending & inheritance.
Extends is just like prototyping. Again, syntax sugar.

```
class humanCharacter extends gameCharacter {
    constructor(name, xPos, health) {
        super(name, xPos, health);
        this.classification = "Human";
    }
}
```
