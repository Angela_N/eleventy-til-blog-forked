---
title: TIL how to make mobile devices vibrate
date: 2021-08-06
published: true
tags: ['javascript', 'mobile']
series: false
canonical_url: false
description: "The Navigator.vibrate() method pulses the vibration hardware on the device, if such hardware exists."
layout: layouts/post.njk
---

This is using the Vibration API.

The Navigator.vibrate() method pulses the vibration hardware on the device, if such hardware exists. If the device doesn't support vibration, this method has no effect. If a vibration pattern is already in progress when this method is called, the previous pattern is halted and the new one begins instead.

```js
navigator.vibrate(200); // vibrate for 200ms
navigator.vibrate([100,30,100,30,100,30,200,30,200,30,200,30,100,30,100,30,100]); // Vibrate 'SOS' in Morse.
```

It currently doesn't work on a few desktop browser types. But it's great on mobile.
https://caniuse.com/vibration


Via:
https://dev.to/ra1nbow1/6-useful-frontend-techniques-that-you-may-not-know-about-47hd?utm_source=pocket_mylist
