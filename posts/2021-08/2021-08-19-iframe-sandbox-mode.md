---
title: TIL iframe sandbox attribute
date: 2021-08-19
published: false
tags: ['html', 'javascript']
series: false
canonical_url: false
description: "The iframe HTML element represents a nested browsing context, embedding another HTML page into the current one. There's also a sandbox mode."
layout: layouts/post.njk
---

The `<iframe>` HTML element represents a nested browsing context, embedding another HTML page into the current one.

It can be abused.

`<iframe sandbox>` allows you to lock it down. If some form tried to submit something in there: nope, won’t work. What if it tries to trigger a download? Nope. Ask for device access? No way. It can’t even load any JavaScript at all.

What you can do is unlock it by doing:

`<iframe sandbox="allow-downloads">`

There's actually a huge list of restrictions you can unlock, if you want fine control over the iframe in the [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#attr-sandbox)

Learned in:
[Choice Words about the Upcoming Deprecation of JavaScript Dialogs](https://css-tricks.com/choice-words-about-the-upcoming-deprecation-of-javascript-dialogs/)
