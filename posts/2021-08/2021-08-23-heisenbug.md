---
title: TIL the Heisenbug
date: 2021-08-23
published: true
tags: ['dev']
series: false
canonical_url: false
description: "In computer programming, heisenbug is a classification of an unusual software bug that disappears or alters its behavior when an attempt to isolate it is made."
layout: layouts/post.njk
---


Today I learned about the heisenbug.

> In computer programming, heisenbug is a classification of an unusual software bug that disappears or alters its behavior when an attempt to isolate it is made. Due to the unpredictable nature of a heisenbug, when trying to recreate the bug or using a debugger, the error may change or even vanish on a retry.
[Definition](https://www.webopedia.com/definitions/heisenbug/)

There's even a conference on it!
https://heisenbug-moscow.ru/en/


What is an example?

> when I worked at Microsoft, we saw this problem all the time, bugs that existed in live code disappeared in debug mode. The most common culprit is that somebody forgot to initialize a variable. When you run in the debugger it first zeros out all of your memory, thus a defacto init. But when you run live, you get whatever random value was already in that memory location. If your program then assumes that the var has already been initialized to zero, you are in trouble and with a very hard to find bug. I can't even count how many times this happened. It's one of the reasons that I came to the conclusion that C is a menace to the software industry.
  -- codeslinger  www.compsalot.com

Via https://wiki.c2.com/?HeisenBug
