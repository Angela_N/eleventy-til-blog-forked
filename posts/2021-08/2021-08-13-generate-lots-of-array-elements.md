---
title: TIL a easy way to generate a lot of fake numbers
date: 2021-08-13
published: true
tags: ['javascript', 'arrays']
series: false
canonical_url: false
description: "You need lots of array elements. You got it."
layout: layouts/post.njk
---

You need lots of array elements. You got it.

```js
Array.from({ length: 1000 }, Math.random);
// [ 0.6163093133259432, 0.8877401276499153, 0.4094354756035987, ...] - 1000 items
```


[Via 11 JavaScript Tips and Tricks to Code Like A Superhero (Vol.2)](https://dev.to/orkhanjafarovr/11-javascript-tips-and-tricks-to-code-like-a-superhero-vol-2-mp6)
