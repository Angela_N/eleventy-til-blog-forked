---
title: TIL about some key elements of NestJS
date: 2021-08-29
published: true
tags: ['javascript', 'node', 'framework']
series: false
canonical_url: false
description: "Controllers, Providers, and Modules OH MY!"
layout: layouts/post.njk
---

Today I learned about some key elements of NestJS.

https://docs.nestjs.com/providers


**Controllers** keep your code organized. Controllers are responsible for handling incoming requests and returning responses to the client.

So Controllers handle HTTP requests, while **Providers** contains shared logic throughout the entire application and can be injected as a dependency. Examples: You can use Providers to implement a guard to handle role-based user authentication or turn it into a filter to pipe data that validates and transform values in a controller.

**Modules** allows code to be organized into smaller components where it can be lazy loaded to run faster in serverless environments.

[NestJS Documentation](https://docs.nestjs.com/)
