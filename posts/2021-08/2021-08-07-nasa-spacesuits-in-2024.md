---
title: TIL why NASA will not be able to have usable spacesuits
date: 2021-08-07
published: true
tags: ['history']
series: false
canonical_url: false
description: "The answers were mostly because the space suits of the past were made in a specific way that we can't replicate without building it from scratch again."
layout: layouts/post.njk
---

The question was:

"How is it not possible for NASA to have usable spacesuits by 2024?"

The answers were mostly because the space suits of the past were:

1. Made in a specific style/factory
2. Could only be used for a few minutes
3. Weren't maintained to today's standards

> To emphasize your first point, it's the same reason why the US Army keeps a tank factory operating in Ohio, despite having a surplus of tanks and parts. They don't wanna be in a situation where they need more tanks but have to start from scratch: sourcing supplies, building/repairing machinery, training employees, logistics of shipping the assembled tanks, and so on.

Very fascinating stuff, especially as we start to forget how computers work, and then it becomes magic!


Via [AdmiralAkbar1 on Reddit](https://www.reddit.com/r/explainlikeimfive/comments/p1us2i/eli5_how_is_it_not_possible_for_nasa_to_have/h8fxlx3)
