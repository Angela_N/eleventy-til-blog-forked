---
title: TIL why emojis in JS counts as a length of 2
date: 2021-08-11
published: false
tags: ['javascript', 'fonts']
series: false
canonical_url: false
description: "Characters are written in UTF-16. Emojis are bigger, and require two characters."
layout: layouts/post.njk
---

You can put emojis in code.

Take a look at this:

```js
const smile = '😀';

smile.length; // => 2
```

Why is it a `length` of 2?

It’s because JavaScript considers strings as a sequence of code units, rather than a sequence of visible characters

The [Javascript Specs](https://tc39.es/ecma262/#sec-ecmascript-language-types-string-type):
> The String type is the set of all ordered sequences of zero or more 16-bit unsigned integer values (“elements”). The String type is generally used to represent textual data in a running ECMAScript program, in which case each element in the String is treated as a UTF-16 code unit value.

Look at that word `code unit`.
That means every symbol/character is a code unit from  `0x0000` until `0xFFFF`.

Another key word is UTF-16s. We're talk about that later.

```js
const letter = '\u0048';

letter === 'H' // => true
```

The grinning face character '😀', which would have the code unit of `0x1F600` (the number `0x1F600` is bigger than `0xFFFF` thus doesn’t fit into 16 bits), is encoded with a sequence of 2 code units `0xD83D0xDE00`.

Via
https://dmitripavlutin.com/what-is-string-in-javascript/
