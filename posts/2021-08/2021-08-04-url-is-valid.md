---
title: TIL check if the URL is valid
date: 2021-08-04
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Using the Web API URL to check if a link is valid or not."
layout: layouts/post.njk
---


Brilliant! This uses the [web APIs URL() - MDN](https://developer.mozilla.org/en-US/docs/Web/API/URL/URL).



```js
const isValidURL = (url) => {
  try {
    new URL(url);
    return true;
  } catch (error) {
    return false;
  }
};

isValidURL("https://dev.to");
// true

isValidURL("https//invalidto");
// false

new URL('/en-US/docs');                        // Raises a TypeError exception as '/en-US/docs' is not a valid URL
new URL('http://www.example.com', );           // => 'http://www.example.com/'
```

[https://caniuse.com/url](https://caniuse.com/url)


Via 11 JavaScript Tips and Tricks to Code Like A Superhero (Vol.2)](https://dev.to/orkhanjafarovr/11-javascript-tips-and-tricks-to-code-like-a-superhero-vol-2-mp6)
