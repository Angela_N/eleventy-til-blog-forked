---
title: TIL that you can chain if/else using ternary conditionals
date: 2021-08-24
published: false
tags: ['javascript']
series: false
canonical_url: false
description: "Chaining Ternary conditionals are really neat"
layout: layouts/post.njk
---



Today I learned that the ternary operator is right-associative, which means it can be "chained" in the following way, similar to an if … else if … else if … else chain.

```js
function example(…) {
    return condition1 ? value1
         : condition2 ? value2
         : condition3 ? value3
         : value4;
}
```

// Equivalent to:

```js
function example(…) {
    if (condition1) { return value1; }
    else if (condition2) { return value2; }
    else if (condition3) { return value3; }
    else { return value4; }
}
```

REFERENCE:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator#conditional_chains
