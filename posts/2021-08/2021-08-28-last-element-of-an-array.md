---
title: TIL getting the last element of an array
date: 2021-08-28
published: true
tags: ['arrays', 'javascript']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

The slice method is very useful in JavaScript when you need to get the last elements of the array. Just pass the negative number as an argument and it will slice the array from the last index.

The Slice method does not mutate your array.

```js
let array = [1, 3, 4, 5, 6, 9, 10, 12]
console.log(array.slice(-1)) // [12]
console.log(array.slice(-2)) // [10, 12]
console.log(array.slice(-4)) // [6, 9 ,10, 12]
```

Via #2 -
[17 Clever JavaScript Tricks That Every Developer Should Use](https://levelup.gitconnected.com/17-clever-javascript-tricks-that-every-developer-should-use-e7f299e49896)
