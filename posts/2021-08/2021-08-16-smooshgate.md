---
title: TIL SmooshGate
date: 2021-08-16
published: true
tags: ['javascript', 'history']
series: false
canonical_url: false
description: "It was a a proposal for a JavaScript language feature called `Array.prototype.flatten`."
layout: layouts/post.njk
---

I remember watching this unfold, and then forgetting about it until someone brought it up.

It's a fantastic story about how the goal of new Javascript features must never break the internet. It's the [number one design principle](https://www.w3.org/TR/html-design-principles/#support-existing-content) for HTML/CSS/JS and other web standards that are used on the internet!

It's why TC39 Committee has five stages before approving a new feature into Javascript. And some features can take YEARS before it goes through all 5 stages!

**What is Smooshgate?**

It was a proposal for a JavaScript language feature called `Array.prototype.flatten`.

When Firefox Nightly implemented it, it broke some websites that was using the widespread MooTools Library. (Although MooTools is not commonly used for new websites in 2018, it used to be very popular and is still present on many production websites.)

MooTools defines their own non-standard version of `Array.prototype.flatten`.

So the proposal author jokingly suggested renaming `flatten` to `smoosh` to avoid the compatibility issue.

That lead to a life on it's own, as people from both sides came out.
One side argued: Why should the internet be beholden to one library's decision nearly a decade ago?
Another side argued: Why not match the specs so it works just like MooTool's formatting?
And finally, the side (that ended up making the decision) was: Don't break the internet.

During the May 2018 TC39 meeting, #SmooshGate was officially resolved by renaming `flatten` to `flat`.

```
// Flatten one level:
const array = [1, [2, [3]]];
array.flat();
// → [1, 2, [3]]
```
Via:
https://developers.google.com/web/updates/2018/03/smooshgate
and
https://medium.com/@jacobdfriedmann/smooshgate-the-ongoing-struggle-between-progress-and-stability-in-javascript-2a971c1162dd
