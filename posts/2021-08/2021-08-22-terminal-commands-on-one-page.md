---
title: TIL a one-page guide to a bunch of terminal commands I use frequently
date: 2021-08-22
published: false
tags: ['terminal']
series: false
canonical_url: false
description: "Developers work in the command line frequently."
layout: layouts/post.njk
---

Developers work in the command line frequently. Sometimes you just have to `ssh` directly into a server and tinker around.

It's always been a pain to google for these commands, since there's so many different sites that has the answer, but it's also filled with distracting links/pop-ups, it has a 5-paragraph 'reason' or it's just plain ugly.

This repo is great since it's just a nice one-pager:
[You don't need a GUI - terminal commands](https://github.com/you-dont-need/You-Dont-Need-GUI#quick-links)


**Some Examples**

Duplicate a file:
```
$ cp readme.txt readme.bak.txt
```

Merge Directories:
```
$ rsync -a /images/ /images2/
```

Zip a directory:
```
$ zip -r archive_name.zip folder_to_compress
```

Show a calendar:
```
$ cal
$ cal 11 2018
```
