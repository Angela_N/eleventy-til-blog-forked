---
title: "TIL HTML's native hidden attribute"
date: 2021-08-14
published: true
tags: ['html', 'css']
series: false
canonical_url: false
description: "To hide a DOM element: there's a native HTML attribute called `hidden`."
layout: layouts/post.njk
---

To hide a DOM element: there's a native HTML attribute called `hidden`.

The effect is similar to adding a style `display: none;`.

The hidden attribute must not be used to hide content just from one presentation. If something is marked hidden, it is hidden from all presentations, including, for instance, screen readers.

```html

<p hidden>This content is not relevant to this page right now, so should not be seen. Nothing to see here. Nada.</p>

```

Heads up:
>  Changing the value of the CSS display property on an element with the hidden attribute overrides the behavior. For instance, elements styled display: flex will be displayed despite the hidden attribute's presence.

Via the MDN:
https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/hidden
