---
title: TIL there's actually a official guide for how HTML is implemented
date: 2021-08-20
published: true
tags: ['html']
series: false
canonical_url: false
description: "Enjoy arguing about HTML standards and realize the MDN isn't cutting it? Well I have a solution."
layout: layouts/post.njk
---

Enjoy arguing about HTML standards and realize the MDN isn't cutting it? Well I have a solution.

[HTML Specifications - Living Standard](https://html.spec.whatwg.org/)
and the [github](https://github.com/whatwg/html)

This is as bare-bones as you get to the original source.
