---
title: TIL an Email Address Regular Expression that 99.99% works
date: 2021-08-27
published: true
tags: ['regex', 'email', 'validation']
series: false
canonical_url: false
description: "This is probably the closest we can get to the most perfect regex for validating emails."
layout: layouts/post.njk
---


This is probably the closest we can get to the most perfect regex for validating emails.

As it says, it's a Email Address Regular Expression that 99.99% works.

Via [the Email Regex site](http://emailregex.com/)

```
(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])
```

There's also versions for different languages so you can copy/paste.

Via the [StackOverflow post (where I found the answer)](https://stackoverflow.com/a/201378/4096078):

> It's also important to understand that validating it per the RFC tells you absolutely nothing about whether that address actually exists at the supplied domain, or whether the person entering the address is its true owner. People sign others up to mailing lists this way all the time. Fixing that requires a fancier kind of validation that involves sending that address a message that includes a confirmation token meant to be entered on the same web page as was the address.
