---
title: TIL ES modules and dynamic imports
date: 2021-08-26
published: true
tags: ['javascript', 'es-modules']
series: false
canonical_url: false
description: "Did you know we can also dynamically load modules based on conditionals? The import(path) returns a promise, it fits great with the async/await syntax."
layout: layouts/post.njk
---

ES Modules allow us to separate JS code into separate files, aka 'modules'.

We can now do this on the browser. (as long as you identify the script tag as a module type)

```js
import { concat } from './concatModule.js';

concat('a', 'b'); // => 'ab'
```

Did you know we can also dynamically load modules based on conditionals?
The `import(path)` returns a promise, it fits great with the async/await syntax.
```js
// loading it normally
const module1 = await import('./myModule.js');


// loading it as a promise
async function execBigModule(condition) {
  if (condition) {
    const { funcA } = await import('./bigModuleA.js');
    funcA();
  } else {
    const { funcB } = await import('./bigModuleB.js');
    funcB();
  }
}


loadMyModule();
```


via [How to Dynamically Import ECMAScript Modules](https://dmitripavlutin.com/ecmascript-modules-dynamic-import/)
