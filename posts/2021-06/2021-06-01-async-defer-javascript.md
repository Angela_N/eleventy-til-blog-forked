---
title: TIL javascript async vs defer
date: 2021-06-01
published: true
tags: ['javascript', 'performance']
series: false
canonical_url: false
description: "The order of operations in Async and Defer"
layout: layouts/post.njk
---

The basic tl;dr: (All the sweet ascii graphics were taken from the [original source](https://thisthat.dev/script-async-vs-script-defer/))

## Default:
`<script src="/path/to/script.js"></script>`

Steps:
1. Parse the document.
2. Pause document parser
3. Download Script
4. Execute Script
5. Continue parsing

Results:
* The page will remain a white screen until JS is finished.
* The page is not interactable either during that time.
* If the JS is broken, the page can break too. Content can be blocked.

Visual:
```
┌───────────────────────┬───────────────────────────────────────┬───────────────────┐
/ Parse the document    / Pause parsing                         / Resume parsing    /
└───────────────────────┼───────────────────┬───────────────────┴───────────────────┘
                        / Download script   /
                        └───────────────────┼───────────────────┐
                                            / Execute script    /
                                            └───────────────────┘
```

## Async:

`<script src="/path/to/script.js" async></script>`

Steps:
1. Parse the script.
2. Download Script during parsing.
3. Pause Parsing and Execute Script.
4. Continue parsing

Results:
* The page will be interactable. HTML is still available.


```
┌───────────────────────────────────────────┬───────────────────┬───────────────────┐
/ Parse the document                        / Pause parsing     / Resume parsing    /
└───────────────────────┬───────────────────┼───────────────────┴───────────────────┘
                        / Download script   /
                        └───────────────────┼───────────────────┐
                                            / Execute script    /
                                            └───────────────────┘
```


## Defer
`<script src="/path/to/script.js" defer></script>`

Steps:
1. During parsing, download the script.
2. Execute the script after the page is ready.

Result:
* Site is interactable, even without JS.
* Content can pop in. If it's below the fold, it won't visually be noticed.

Visual:
```
┌───────────────────────────────────────────────────────────┐
/ Parse the document                                        /
└───────────────────────┬─────────────────┬─────────────────┘
                        / Download script /
                        └─────────────────┘                 ┌───────────────────┐
                                                            / Execute script    /
                                                            └───────────────────┘
```
