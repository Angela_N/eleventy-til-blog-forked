---
title: TIL the -s in npm install means silent install
date: 2021-06-24
published: false
tags: ['npm']
series: false
canonical_url: false
description: "In other words, including -s (or --silent) in your npm install command means that it will have no output (only a newline)"
layout: layouts/post.njk
---


`npm install <package> -s`

What does the `-s` mean?

It's short for `--silent`.

`-s` appears to silence the output of the command (at least in npm v7.0.15).

In other words, including -s (or --silent) in your npm install command means that it will have no output (only a newline)

Via
https://stackoverflow.com/questions/54471399/what-npm-install-s-mean#:~:text=npm%20%2DS%20with,your%20dependencies%20in%20your%20package.
