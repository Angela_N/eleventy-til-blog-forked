---
title: TIL the definition of various testing types - part 1
date: 2021-06-02
published: true
tags: ['testing']
series: false
canonical_url: false
description: "This is probably a part 1 of many, since there's so many different test types out there."
layout: layouts/post.njk
---

This is probably a part 1 of many, since there's so many different test types out there. Since I always forget, it becomes a constant TIL reminder.

## Smoke Test:

**What is it**
Also called sanity test, or build acceptance test.
It's usually a quick test to see if your code didn't break anything.

Via [Microsoft docs](https://docs.microsoft.com/en-us/previous-versions/ms182613(v=vs.80)?redirectedfrom=MSDN):
> The term smoke testing originated in the hardware industry. The term derived from this practice: After a piece of hardware or a hardware component was changed or repaired, the equipment was simply powered up. If there was no smoke, the component passed the test.

**Example**
Using a e-commerce example -- a quick smoke test would be if you added a feature, does it break checkout?

So a tester can simply run a quick order to see if there's any conflicts.
If it breaks, then something else down the line may have happened.

**How to actually do it**
Depends!

The basic level is using a pipeline or automation building tool. If it pops out an error, then something's wrong.


## End-to-end test:

**What is it**

Testing the application or software from beginning to end. Hence, end-to-end.

(via [Tutorialspoint](https://www.tutorialspoint.com/software_testing_dictionary/end_to_end_testing.htm))
The purpose of performing end-to-end testing is to identify system dependencies and to ensure that the data integrity is maintained between various system components and systems.

The entire application is tested for critical functionalities such as communicating with the other systems, interfaces, database, network, and other applications.

**Example**
Using a e-commerce example -- a end-to-end test would be to simulate the customer adding a item into their shopping cart, and running it all the way to the checkout complete stage.

Depending on your needs, you can even earlier starts and later end points. For example, the testing can start where the user clicks on the ad, lands on the store page, logs in. And it ends with the user receiving the receipt via email.


**How to do it**

Manually! Usually a user does it all.
For web development, you can automate it using [Selenium](https://www.selenium.dev/), or [Cypress](https://www.cypress.io/)

## UI test:

**What is it**

User interface or UI testing, also known as GUI testing, is where you test the visual elements of a application to ensure they work and was what the stakeholder wanted, as well as it meets specific needs (like accessibility, or conversion)

It can involve testing all visual indicators and graphical icons, including menus, radio buttons, text boxes, checkboxes, toolbars, colors, fonts, and more.

([yoinked from perfecto.io](https://www.perfecto.io/blog/ui-testing-comprehensive-guide))

**Example**

Using a e-commerce example -- The Story is asking for a way to switch between t-shirt colors.

One proposal was to use a selector that simply changes the color of the shirt's graphic with CSS.

After setting up that behavior, through UI Testing, someone may notice that the CSS colors are actually inaccurate to how the shirt actually looks, and suggests the radio button switches t-shirt graphics itself. So Red shirt is a photo of a red shirt.

**How to do it**

Typically manual. A human goes through it all.



REFERENCES:
https://en.wikipedia.org/wiki/Smoke_testing_(software)
https://www.browserstack.com/guide/end-to-end-testing
https://www.perfecto.io/blog/ui-testing-comprehensive-guide
