---
title: TIL Stateful and stateless
date: 2021-06-07
published: true
tags: ['math', 'loops', 'javascript']
series: false
canonical_url: false
description: "An expression in a programming language can be “stateful” or “stateless”."
layout: layouts/post.njk
---


Programming languages have the notion of “state”. This is a snapshot of a program’s current environment: all the variables that have been declared, functions created and what is currently being executed.

An expression in a programming language can be “stateful” or “stateless”. A stateful expression is one that changes a program’s current environment. A very simple example in Javascript would be incrementing a variable by 1:


```js
var number = 1;
var increment = function() {
    return number += 1;
};
increment();
```

The function `increment()` modifies the variable `number` directly.
That is a hidden intent, and should be avoided.


This stateless expression, on the other hand, is one that does not modify a program’s environment:

```js
var number = 1;
var increment = function(n) {
    return n + 1;
};
increment(number);
```

The function `increment()` has no values inside of it. It takes a parameter. It's 'pure'.

Via: https://stephen-young.me.uk/2013/01/20/functional-programming-with-javascript.html
