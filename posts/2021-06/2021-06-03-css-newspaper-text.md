---
title: TIL column gap for newspaper look
date: 2021-06-03
published: true
tags: ['css', 'codepen', 'responsive', 'webdev', 'mdn']
series: false
canonical_url: false
description: "Use `column-count` and `column-gap` to give it that sweet newspaper look."
layout: layouts/post.njk
---

Use `column-count` and `column-gap` to give it that sweet newspaper look.

You can also use flex/grid if you want to control the elements inside.

```css
/* Where the magic happens */
.column-gap-single {
  column-count: 4;
  column-gap: 10px;
}

.column-gap-flex {
    display: flex;
    column-gap: 10px;
}

.column-gap-flex > p {
  flex-basis: 0;
  flex-grow: 1;
}

.column-gap-grid {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    column-gap: 50px;

}

```

REFERENCE:
https://developer.mozilla.org/en-US/docs/Web/CSS/column-gap
https://developer.mozilla.org/en-US/docs/Web/CSS/column-count


<p class="codepen" data-height="300" data-default-tab="css,result" data-slug-hash="NWjKEMm" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/NWjKEMm">
  Using column-gap with long text</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
