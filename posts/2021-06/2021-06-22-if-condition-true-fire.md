---
title: TIL Console Log trick if a condition is true
date: 2021-06-22
published: false
tags: ['javascript']
series: false
canonical_url: false
description: "condition && console.log('it's true')"
layout: layouts/post.njk
---


```js
if (condition) {
   console.log("it's true!");
}
```

can be shortcut to
```js
condition && console.log("it's true!");
```

via
https://blog.logrocket.com/refactoring-cascading-conditionals-favor-readability/
