---
title: TIL How CSS get added into the DOM
date: 2021-06-13
published: true
tags: ['css', 'html']
series: false
canonical_url: false
description: "Render Tree is CSSOM + DOM Tree with all the correct stylings"
layout: layouts/post.njk
---

When a application encounters a CSS file, it'll:

1. Wait for the file to be downloaded
2. load the CSS file
3. create a **CSS Object Model. (CSSOM)**
4. Combine the CSSOM with the DOM tree.

At the end, the DOM tree becomes the "Render Tree". (Render Tree is CSSOM + DOM Tree with all the correct stylings)

That step 1 where it waits for the file to be downloaded is render-blocking.

SOURCE: "Simple CSS Hack to Reduce Page Load Time"
https://javascript.plainenglish.io/simple-css-hack-to-reduce-page-load-time-366f7aaaa3be
