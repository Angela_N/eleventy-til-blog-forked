---
title: TIL Deallocating memory in Javascript
date: 2021-06-26
published: false
tags: ['javascript']
series: false
canonical_url: false
description: "Garbage collection is done automatically in JS. But how does it work?"
layout: layouts/post.njk
---

Garbage collection in a nutshell:
> Low-level languages require the developer to manually determine at which point in the program the allocated memory is no longer needed and to release it.

> Some high-level languages, such as JavaScript, utilize a form of automatic memory management known as garbage collection (GC). The purpose of a garbage collector is to monitor memory allocation and determine when a block of allocated memory is no longer needed and reclaim it.
Via:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management

How do you force garbage collection?

> An object is said to be "garbage", or collectible if there are zero references pointing to it.

In other words, set the variable to `null`.

REFERENCE:
https://stackoverflow.com/a/17478670/4096078

