---
title: TIL minus minus plus
date: 2021-06-25
published: false
tags: ['math', 'lolz']
series: false
canonical_url: false
description: "Well technically true, don't do this. Ever."
layout: layouts/post.njk
---

Well technically true, don't do this. Ever.

![](https://i.redd.it/5h2dqrfck0671.jpg)

```
i++;

i = i + 1;

i -= -1;
```

via
https://i.redd.it/5h2dqrfck0671.jpg
