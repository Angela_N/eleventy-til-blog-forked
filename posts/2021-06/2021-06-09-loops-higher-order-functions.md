---
title: TIL Higher Order functions to reduce loops
date: 2021-06-09
published: true
tags: ['math', 'loops', 'javascript']
series: false
canonical_url: false
description: "Higher Order Functions like Sum, Map, Filter, Reduce"
layout: layouts/post.njk
---

## Sum

Usecase: You wanted to add a array of numbers together.

```js

const countOf6 = [1,2,3];

// Using a loop: Add all the elements together
const sumLoop = arr => {
  let total = 0;

  for (let val of countOf6) {
      total += val;
  }

  return total;
}

// Higher order Function: Add all the elements together

const sum = xs => xs.reduce((a, b) => a + b, 0)

console.log( sumLoop(countOf6) ); // => 6
console.log( sum(countOf6) ); // => 6

```



## Map

You want to get all the names in the array that contains objects.

```js

const users = [
  { name: "Bob", age: 44 },
  { name: "Linda", age: 44 },
  { name: "Tina", age: 13 },
  { name: "Gene", age: 11 },
  { name: "Louise", age: 9 },
  { name: "Teddy", age: 40 }
];

let getName = (user) => user.name;

// Using a Loop: Combining all the names together
loopNames = [];

for (let i = 0; i < users.length; i++) {
  const name = getName(users[i]);

  loopNames.push(name);
}

// Using a higher order function: Combining all the names together
mapNames = users.map(getName);

console.log(loopNames); // ["Bob","Linda","Tina","Gene","Louise","Teddy"]
console.log(mapNames); // ["Bob","Linda","Tina","Gene","Louise","Teddy"]

```

## Filter

Use case: You want to find a person that has a specific criteria. For example, you want all the `T` names.

```js
const users = [
  { name: "Bob", age: 44 },
  { name: "Linda", age: 44 },
  { name: "Tina", age: 13 },
  { name: "Gene", age: 11 },
  { name: "Louise", age: 9 },
  { name: "Teddy", age: 40 }
];

const startsWithT = (string) => string.toLowerCase().startsWith('t');

// Using a Loop: Finding all the names that start with T

let loopNamesStartingWithT = [];

for (let i = 0; i < users.length; i++) {
  if (startsWithT(users[i].name)) {
    loopNamesStartingWithT.push(users[i]);
  }
}

// Higher order Function: Finding all the names that start with T
let namesStartingWithT = users.filter((user) => startsWithT(user.name));


console.log(loopNamesStartingWithT); // Tina and Teddy
console.log(namesStartingWithT);// Tina and Teddy

```

## Reduce

You want to turn this array into a single value, based on a element key.

For example: You want to know what are the combined ages of the entire Bob's Burgers character list.

```js
const users = [
  { name: "Bob", age: 44 },
  { name: "Linda", age: 44 },
  { name: "Tina", age: 13 },
  { name: "Gene", age: 11 },
  { name: "Louise", age: 9 },
  { name: "Teddy", age: 40 }
];

// Using a loop: Add up all the ages
var total = 0;

for (let i = 0; i < users.length; i++) {
  total += users[i].age;
}

// Higher order Function: Add up all the ages
totalAge = users.reduce((total, user) => user.age + total, 0);

console.log({total}); // 161
console.log({totalAge}); // 161
```


REFERENCES:
https://github.com/you-dont-need/You-Dont-Need-Loops#higher-order-functions
https://www.freecodecamp.org/news/a-quick-intro-to-higher-order-functions-in-javascript-1a014f89c6b/
