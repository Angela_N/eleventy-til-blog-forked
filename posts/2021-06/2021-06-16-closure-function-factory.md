---
title: TIL making Function Factories
date: 2021-06-16
published: true
tags: ['javascript', 'closure']
series: false
canonical_url: false
description: "Functions making functions"
layout: layouts/post.njk
---

This is code from the MDN.
```js
function makeAdder(x) {
  return function(y) {
    return x + y;
  };
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);

console.log(add5(2));  // 7
console.log(add10(2)); // 12
```

The function `makeAdder()` is a function factory.

It creates functions that can add a specific value to their argument.

`add5` becomes `function add5(x = 5, y) {}`
`add10` becomes `function add10(x = 10, y) {}`


Via:
[MDN Web Docs: Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
