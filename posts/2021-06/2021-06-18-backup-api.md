---
title: TIL Putting a redundant API key as a backup
date: 2021-06-18
published: false
tags: ['api']
series: false
canonical_url: false
description: "Have a backup API key in your system. If you get a rate limit error, you can switch over to it. "
layout: layouts/post.njk
---

Have a backup API key in your system. If you get a rate limit error, you can switch over to it.

Great if you're using a free project and need a quick workaround.
But just remember not to abuse resources.

> The fatal bug that caused me to take down my bot was that I hit an hourly API rate limit. I took the obvious approach of keeping redundancy in my system: Keep an alternative API key, and once the primary one runs out, switch over to the alternative, and then switch back at the turn of the hour.

Via [How to Recover from Deployment Hell – What I Learned After My Discord Bot Crashed on a 1,000+ User Server](https://www.freecodecamp.org/news/recovering-from-deployment-hell-what-i-learned-from-deploying-my-discord-bot-to-a-1000-user-server/)
