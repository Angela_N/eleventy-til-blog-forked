---
title: TIL of Amazon's hiring process
date: 2021-06-19
published: true
tags: ['job']
series: false
canonical_url: false
description: "The rounds within a Amazon Interview"
layout: layouts/post.njk
---

There's 5 rounds:
* Screening round
* Two technical rounds (might be three)
* A hiring manager round
* Bar raiser round

## The Screening Round
> basic Frontend Tech was discussed including Html, Css, Javascript, some logical questions. There was no coding-test/assignment round.

## Technical Rounds
> First technical [round] was mostly focused on FE Technologies as in Html, CSS, Javascript, DOM Manipulation etc. All the rounds were Library/Framework agnostic. I was not asked to code in React/Angular specifically.

> Second Technical Round was more focused on Data Structures and Algorithms — the topics on focus were Arrays, Strings, Stacks, Linked List, Trees and Maps. There were questions related to core javascript concepts as well. Few were output based.

## Hiring Manager Round
>  behavioural and to check team culture fit. The 14 Amazon Leadership Principles are very important for this round.

## Bar Raiser Round
> It can be technical, behavioural or a combination of both. For me it majorly revolved around Amazon’s 14 LPs. I was asked questions related to my prior experience and projects. Finally this round ended by a DSA question of Trees. This round is to check that the candidate passes a certain bar set by the engineers and managers and also whether they have the potential of raising the quality bar of team as a whole.

Via
https://medium.com/@vinayakvvg/frontend-engineer-interview-experience-amazon-bdf8b3d66d40
