---
title: "TIL Halo 2's smart autoload"
date: 2021-06-29
published: false
tags: ['gamedev']
series: false
canonical_url: false
description: "Autosaves were special."
layout: layouts/post.njk
---

In Halo 2 and onward, after being stuck in an unwinnable autosave situation for a few deaths, the game will punt you back to an earlier autosave. If you find yourself in a situation that is possibly winnable but incredibly difficult, then multiple suicides to trigger this might be a good idea.

