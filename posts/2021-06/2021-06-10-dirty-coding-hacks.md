---
title: TIL Some fun dirty coding tricks in video games (2009 edition)
date: 2021-06-10
published: true
tags: ["hacks", "gaming"]
series: false
canonical_url: false
description: "This classic gamasutra article from 2009 about some of the hacks developers did to get their game out of the door."
layout: layouts/post.njk
---

This classic gamasutra article from 2009 about some of the hacks developers did to get their game out of the door.

## Turning the camera

> the PS2 version in particular had a late, extremely hard-to-track-down issue: A long soak test of the player character standing still in the first level of the game would cause it to periodically crash. Unfortunately, the crash was limited to disc-only retail builds that included no debugging information. With fears of a Sony technical requirement check (TRC) failure looming we worked hard to find a solution.

> Because the crash was intermittent, the lead engineer on the title and I would manually burn a bunch of discs and start them up on an array of PS2s. (Keep in mind this was an independent developer, with no IT team or fancy disc burning machines.)

> Between stretches of passing the time over the weekend in the office with World of Warcraft while waiting for a crash, someone noticed that the game didn't crash if you rotated the camera 90 degrees to the right... As we ran out of time, we created the final PS2 disc with a rotated camera at level start -- the other two platforms were already on track -- and submitted everything. The game passed all the platform holders' TRC tests and was shipped to market on time.

## File conflicts

> In the afternoon, we make the last build with the last few game-balancing tweaks, and do one last playthrough session when disaster strikes: The game crashes hard!

> It turns out that one of the innocent tweaks the designers had checked in that afternoon made it so a text file had the exact same filename and data CRC as another resource file, even though they were completely different!

> Then, as quickly as despair swept over us, we realized how we could fix this on time for the gold candidate release. We opened up the text file responsible for the conflict, added a space at the end, and saved it.

## If it breaks, don't do it

`IF (Damp will fall through a hole()) THEN Don't do it`


```
damp_old = damp_loc;

move_damp();

if (NoCollision()) {
  damp_loc = damp_old;
}
```

> In one swoop a thousand bugs were fixed. Now instead of falling off the level, Damp would just shudder a bit when he walked over the holes. We found what was hurting us, and we stopped doing it. The publisher laid off their "jump around" tester, and the game shipped.

## The angry face of a the team lead

> It's hard to convince a team of 100 people that the programmers can't simply "fix" the performance of the engine, and that some of the ways people had gotten used to working to needed to be changed.

> The solution took maybe an hour. A fellow programmer took four pictures of my face -- one really happy, one normal, one a bit angry, and one where I am pulling my hair out. I put this image in the corner of the screen, and it was linked to the frame rate. If the game ran at over 30fps, I was really happy, if it ran below 20, I was angry.

> After this change, the whole FPS issue transformed from, "Ah, the programmers will fix it." to, "Hmm, if I put this model in, Nick is going to be angry! I'd better optimize this a little first." People could instantly see if a change they made had an impact on the frame rate, and we ended up shipping the game at 30fps.

## Hiding data

> We cut megabyte after megabyte, and after a few days of frantic activity, we reached a point where we felt there was nothing else we could do. Unless we cut some major content, there was no way we could free up any more memory. Exhausted, we evaluated our current memory usage. We were still 1.5 MB over the memory limit!

> At this point one of the most experienced programmers in the team, one who had survived many years of development in the "good old days,"... he brought up a source file and pointed to this line:

`static char buffer[1024*1024*2];`

> "See this?" he said. And then deleted it with a single keystroke. Done!

>  He knew from experience that it was always impossible to cut content down to memory budgets, and that many projects had come close to failing because of it. So now, as a regular practice, he always put aside a nice block of memory to free up when it's really needed.

Fun bit to that story above -- it was also done in the original Famicon Super Mario Bros! I couldn't find the story, but one of the developers hid some placeholder data for that exact reason. When they deleted it, Miyamoto got excited that he could add more bricks!

REFERENCE:
https://www.gamasutra.com/view/feature/132500/dirty_coding_tricks.php?page=1
