---
title: TIL JS Object destructuring
date: 2020-05-31
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Object Destructuring"
layout: layouts/post.njk
---

**The basic syntax of object destructuring**
```
const animation = {
  provider: 'Netflix',
  title: 'Avatar: The Last Airbender'
}

const { provider } = animation; // => 'Netflix'
````

**Add a object directly into an array**
```
const animation = {
  provider: 'Netflix',
  title: 'Avatar: The Last Airbender',
  mainCharacter: 'Aang'
}

const { provider, mainCharacter } = animation;

provider; // => 'Netflix'
mainCharacter; // => 'Aang'
```


**Add a object directly into an array**
This one is pretty neat. It's a shortcut.
```
{name: fruits[fruits.length]} = {name: 'cherry'}

//fruits is now ['banana', 'apple', 'kumquat', 'cherry']
```

[ref](https://dmitripavlutin.com/javascript-object-destructuring/)
[ref - destructure into array](https://til.hashrocket.com/posts/mtawv6khka-destructure-into-an-existing-array)
