---
title: TIL Web Speech API
date: 2020-05-27
published: true
tags: ['webdev', 'codepen', 'caniuse', 'mdn']
series: false
canonical_url: false
description: "Have multiple cursors"
layout: layouts/post.njk
---

NOTE:
This uses [https://caniuse.com/#feat=speech-synthesis](https://caniuse.com/#feat=speech-synthesis)
and
[https://caniuse.com/#feat=speech-recognition](https://caniuse.com/#feat=speech-recognition).

Speech Recognition API is not implemented in most browsers, only Chrome.

<p class="codepen" data-height="277" data-theme-id="dark" data-default-tab="result" data-user="rockykev" data-slug-hash="rNeBBKq" style="height: 277px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Web Speech API test">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/rNeBBKq">
  Web Speech API test</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

[reference](https://www.voorhoede.nl/en/blog/exploring-the-web-speech-api/#putting-everything-together)
