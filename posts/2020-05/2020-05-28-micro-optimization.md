---
title: TIL Micro-optimization
date: 2020-05-28
published: true
tags: ['cleancode', 'optimization', 'quotes']
series: false
canonical_url: false
description: "Readability"
layout: layouts/post.njk
---

I saw this speed test of these Dom selectors.
[getElementById vs querySelector vs getElementsByClassName vs querySelectorAll](https://jsben.ch/8tEs3)

This was my score.
![micro-optimization screenshot](\img\2020-05-28-micro-optimization.png)

I got really excited to start switching my style.

But then I remembered this quote from Ruben Bridgewater, a member of the Node TSC, whose goal is to push Node forward in every way possible.

```
The optimizations mentioned in the post are micro-optimizations that reduce the readability of the code and thus the code requires more time to read and to understand which leads to less time to optimize hot code paths. The performance is also just a snapshot of the current version and the very same code might behave very different in the next version due to new engine optimizations.
```

So remember - focus on readability, not micro-optimization.

[Read his whole comment for even more context] (https://medium.com/@BridgeAR/this-whole-blog-post-is-sadly-very-bad-advice-when-it-comes-to-real-application-performance-7608c05bb93f)
