---
title: TIL that I had to ask interview questions
date: 2020-11-25
published: true
tags: [""]
series: false
canonical_url: false
description: "Interview questions I should ask"
---

## GENERAL:

### 1. What are these acryonyms?

HTML
DOM
CSS
JS
JSON
XML
AJAX
CMS
CRON
CRUD
FTP
LAMP
REST
SDK
SQL
PHP

**GOAL OF QUESTION**
To gauge how they respond to certain technologies.
If they don't know what it is, press them a bit if they ever heard of it in context.

### 2. What is the difference between functional programming and OOP?

**GOAL**
For our purpose, it really doesn't matter that they know the exact details. To be part of modern web development means that you understand the two major design paradigns. To know nothing about it is a red flag.

### 3. Tell me what the LAMP stack looks like?

**GOAL**
The goal is to identify their familiarity with full-stack development. It's important to understand that there's a lot of layers involved with coding.

### 4. What are ssh keys?

**GOAL**
This checks to see their experience with terminal commands, and git.

### 5. What is the difference between backend, frontend, and full-stack?

**GOAL**
A basic check to see their understanding of the separation of skillsets.

### 6. What are APIS?

**GOAL**
Sort of a trick question - to guage their experience level.

### 7. What are some git actions you can take?

**GOAL**
This checks their understanding of git and familiarity, as well as their experience working with code bases.

### 8. What is regex?

**GOAL**
Checking for familiarity of the language(?) and it's use-case, and why it's important.

### 9. What is SSL Certificate?

**GOAL**
Basic Web dev! The goal is that they know what this is and why it exsits.

### 10. What is NPM/Yarn?

**GOAL**
Basic Web dev! The goal is that they know what this is and what it is used for.

### 11. What is the difference between server-side rendering and client-side rendering?

**GOAL**
Basic Web dev! The goal is that they know what this is and what it is used for.

### 12. When you're looking for a way to solve a particular problem, what are some places you end up online?

**GOAL**
This is to check their tech experience in research. Red flags are ones where they don't really have any research material.

### 13. What is a SQL Injection?

**GOAL**
Basic Web dev! The goal is to see if they have an eye for security.

---

## HTML:

### 1. What is the basic structure of a webpage - and what goes into it?

**GOAL**
Check their comfort level in writing HTML.

### 2. What are some elements of a HTML form?

**GOAL**
Check out their understanding of a form element within HTML.

### 3. What are good usecases for a link, vs a generic button, vs a submit button?

**GOAL**
Check their experience in basic markup, and also seeing if their experience in writing JS functions

### 4. What is the DOM?

**GOAL**
Basic web development!

### 5. What is reflow?

**GOAL**
Advanced web development.

### 6. What is repaint?

**GOAL**
Advanced web development.

### 7. What are data attributes in HTML?

**GOAL**
Advanced web development!

### 8. What are some primary usecases for avid, webp, webm, svg, gif, jpg, and png?

**GOAL**
Check on their familiarity with image types.

### 9. Within HTML - when we say inline Javascript, or inline CSS... what does inline mean?

**GOAL**
Check to see if they are familiar with the different methods of inserting Javascript into a webpage.

### 10. What's the difference between a div, span, section, article, and body?

**GOAL**
Check their familiarity with HTML semantics.

---

## CSS

### 1. What is bootstrap?

**GOAL**
Their familiarity with using CSS frameworks.

### 1. What are psuedo-elements?

**GOAL**
Basic web development.

### 1. What is flexbox?

**GOAL**
Basic web development - and focused on mobile friendly design.

### 1. What is CSS Grid?

**GOAL**
Modern web development - and experienced with mobile friendly design.

### 1. What is the difference between IDs and classes?

**GOAL**
Basic web development

### 1. What is CSS specificity rules?

**GOAL**
Advaned Web development. Familiarity with CSS specificity is a sign that they are familiar with nesting and properly grouping css.

### 1. What is the difference between positioning elements: absolute, relative, fixed, static

**GOAL**
Basic web development and familiarity of organizing web content.

### 1. How would you check for mobile responsiveness?

**GOAL**
Familiarity with devtools or using online tools.
Red flag if they say they load the site on their phone.

### 1. What is a CSS variable?

**GOAL**
Advanced web development. Familiarity with DRY, even in CSS.

### 1. What is the difference between padding and margin?

**GOAL**
If they are familiar with element spacing/size.

### 1. What are some benefits of SASS?

**GOAL**
Familiarity with modern tools to make writing CSS easier/efficient.

### 1. What is a mobile-first design?

**GOAL**
Advanced web development skillset.

---

## PHP AND WORDPRESS TOGETHER

### 1. What is PHP? What is the purpose of it?

### 2. In the chat, show me a "Hello World" with PHP?

### 3. What is the current version of PHP?

### 4. I need you to create a function that takes a variable, and then prints it out the result 20 times. Can you write that for me?

### 5. What current version of WordPress?

### 6. Have you ever built anything in WordPress?

### 7. What does the WordPress architecture look like?

### 8. How does WordPress generate a web page?

### 9. What are posts/pages/themes and plugins in WordPress?

---

## BASIC JAVASCRIPT

### 1. How is the relationship between Java and Javascript?

### 2. How does Javascript work in web development?

### 3. What are var, let, and const?

### 4. What are the types?

### 5. What's the difference between an object and a array?

### 6. Right now, in the app... I want to turn a link to fire off a function. But the link will repaint the page. How do I stop it?

### 7. I want to save data that a user inputs. How would I do that?

### 8. What is ES5, ES6, modern Javascript?

### 9. The page load is slow - and the assumption is that it's a Javascript thing. What is your process for identifying what JS files?

---

## HIGHER LEVEL JAVASCRIPT

### 1. What is hoisting?

### 2. What is event bubbling?

### 3. What is `this`?

-

### 4. What is babel and why should we use it? Why shouldn't we use it?

### 5. What are some Web Browser APIs that look like Javascript functions?

### 6. What is a template literal and what are the benefits to using it instead of the alternatives?

### 7. What is a callback?

### 8. Give me an example of a promise and relationship to that... callback hell?

### 9. What is the differenec between Declaritive and Imperative programming?

### 10. What is the event loop?

### SENIOR LEVEL JAVASCRIPT (aka things I barely know!)

### 1. Explain Iterators and Generators?

### 2. How does a factory function work?

### 3. The different between Javascript classes and other languages classes?

### 3. Explain Currying.

### 4. What are some use cases for the Symbol type?

### 5. Explain Monads and good use cases.

### 6. How does the V8 engine parse javascript?

### 7. Service Workers.

### 9. Why do they say that in Javascript, everything is an object - and why is it wrong?

---

## CODING, STYLE AND WORK:

### 1. What are some good use-cases to refactor? What are some bad use-cases for refactoring?

### 2. You're exploring a new project/task that you have never worked on before. What is your process from receiving it to finally completing it? (Look for the keywords 'ask for help')

### 3. Tell me about a project management tool that you've used?

### 4. When was a time you messed up badly?

### 5. What is your experience working with a designer? What are some things you would ask/negotiate on?

### 6. What is the goal of a PR - for both the story writer and the people reviewing it?

### 7. What technical aspect do you just not understand (right now) that you wish you did?

### 8. What is your experience working remotely?

### 9. Tell me about a time you helped another dev asyncronously? Via email, or slack, or over the phone.

### 10. What are the developers on the internet that really influence your code & style?

### 11. Who is a developer that you know in real life that you really admire? When was the last time you told them that?

### 12. What is the boyscout rule in coding?

### 13. What is the most exciting thing you learned in the past 3 months?

---
