---
title: TIL Graphic Cards and Browsers
date: 2020-11-13
published: true
tags: ["javascript"]
series: false
canonical_url: false
description: "Using Hardware acceleration for better browser performance: Improve custom cursor performance by using “translate3d"
---

Anything that requires a redraw of the layout layer takes time.
Your graphic card/internal graphics could already do the work for you. But you need to use the hardware acceleration api.

```
// Slow :(
customCursor.style.top = "100px";
customCursor.style.left = "50px";

// Better! :D
customCursor.style.transform = `translate3d(${xPosition}, ${yPosition}, 0)`;
```

First, updating a top/right/bottom/left property of a DOM element will trigger a redraw of the layout layer. Avoiding this would be great for performance!

Secondly, using “translate3d” instead of the regular “translate” will force the animation into hardware acceleration. This will speed up performance and will make the animation/transition a lot smoother.

via [https://www.thatsanegg.com/blog/6-essential-javascript-tips-for-the-developer-of-2020/?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_220](https://www.thatsanegg.com/blog/6-essential-javascript-tips-for-the-developer-of-2020/?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_220)
