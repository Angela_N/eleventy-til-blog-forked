---
title: TIL Default Variables
date: 2020-11-14
published: true
tags: ["javascript"]
series: false
canonical_url: false
description: "Default Variables in JS"
---

You can set defaults by using `||`.

```
doSomethingVeryCool = (coolParameter) => {
  const coolThing = coolParameter || "This is not so cool"
  console.log(coolThing);
}

doSomethingVeryCool("This is super cool")
// Result: "This is super cool"
// It's using the argument

doSomethingVeryCool()
// Result: "This is not so cool"
// No argument provided, so it's outputting the default

```

// TODO: Relink back to it
There's also a method to provide defaults to parameters too.

[Set a variable default using “||”
](https://medium.com/@biratkirat/97-journey-every-programmer-should-accomplish-a0c53dbbfd47)
