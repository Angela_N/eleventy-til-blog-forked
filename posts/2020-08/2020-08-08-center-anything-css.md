---
title: TIL how to center anything in CSS
date: 2020-08-08
published: true
tags: ['css']
series: false
canonical_url: false
description: "How to center anything - a decision tree."
---

How to center anything - a decision tree.

[https://css-tricks.com/centering-css-complete-guide/](https://css-tricks.com/centering-css-complete-guide/)


* Horizontally

  * Is it inline or inline-* elements (like text or links)?
  * Is it a block level element?
  * Is there more than one block level element?

* Vertically
Vertical centering is a bit trickier in CSS.

* Is it inline or inline-* elements (like text or links)?
* Is it a block-level element?


* Both Horizontally and Vertically

  * You can combine the techniques above in any fashion to get perfectly centered elements. But I find this generally falls into three camps:
  * Is the element of fixed width and height?
  * Is the element of unknown width and height?
  * Can you use flexbox?
  * Can you use grid?
