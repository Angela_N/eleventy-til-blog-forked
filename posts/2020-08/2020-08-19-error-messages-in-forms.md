---
title: TIL Putting error messages above the field
date: 2020-08-19
published: true
tags: ['accessibility', 'forms']
series: false
canonical_url: false
description: "Putting error messages above the field"
---


> We need to put the relevant error message just above the field. This saves users scrolling up and down the page to check the error message, and keeps them moving down the form. If the message was placed below the field, we’d increase the chance of it being obscured by the browser autocomplete panel or by the onscreen keyboard.

![](img/2020-08-19-firefox.png)

![](img/2020-08-19-mobile-chrome.png)

Examples are from:
https://adrianroselli.com/2017/01/avoid-messages-under-fields.html

BONUS:
> To give both sighted and non-sighted users an equivalent experience, we can use the well-supported aria-invalid attribute. When the user focuses the input, it will now announce “Invalid” (or similar) in screen readers. [https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-invalid_attribute](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-invalid_attribute)

`<input aria-invalid="false">`



pg 56 - From [Form Design Patterns](https://www.smashingmagazine.com/2018/10/form-design-patterns-release/)
