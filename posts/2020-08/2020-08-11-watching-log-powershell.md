---
title: TIL Watching Log Files
date: 2020-08-11
published: true
tags: ['powershell']
series: false
canonical_url: false
description: "Watching log files is useful for debugging in Powershell"
---

Watching log files is useful for debugging.

For most terminals, it's:
`tail -f /log/php/log.error`

But Powershell, it's:
`Get-Content -Path "C:\php\log.error" -Wait`

via [https://stackoverflow.com/questions/4426442/unix-tail-equivalent-command-in-windows-powershell](https://stackoverflow.com/questions/4426442/unix-tail-equivalent-command-in-windows-powershell)
