---
title: TIL about flattening a JS array
date: 2020-08-15
published: true
tags: ['javascript', 'array']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

> There's a nice method for Array called Array.flat, as an argument it needs depth you need to flat (default: 1). But what if you don't know the depth, you need to flatten it all. We just put Infinity as the argument. Also there's a nice flatMap method.


```
const arrays = [[10], 50, [100, [2000, 3000, [40000]]]];
arrays.flat(Infinity);
// [ 10, 50, 100, 2000, 3000, 40000 ]
```

via https://dev.to/gigantz/9-javascript-tips-tricks-to-code-like-a-wizard-559i
