---
title: TIL Disabling the submit buttons is bad
date: 2020-08-20
published: true
tags: ['accessibility', 'forms']
series: false
canonical_url: false
description: "Disabling the submit buttons is bad"
---

> Some forms are designed to disable the submit button until all the fields become valid. There are several problems with this. First, users are left wondering what’s actually wrong with their entries. Second, disabled buttons are not focusable, which makes it hard for the button to be discovered by blind users navigating using the Tab key. Third, disabled buttons are hard to read as they are grayed out.

> As we’re providing users with clear feedback, when the user expects it, there’s no good reason to take control away from the user by disabling the button anyway.


Pg 63  - From [Form Design Patterns](https://www.smashingmagazine.com/2018/10/form-design-patterns-release/)

