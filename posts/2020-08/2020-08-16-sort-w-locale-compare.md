---
title: TIL localCompare
date: 2020-08-16
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Sort with localComapre"
---

Via [https://dev.to/gigantz/9-javascript-tips-tricks-to-code-like-a-wizard-559i](https://dev.to/gigantz/9-javascript-tips-tricks-to-code-like-a-wizard-559i?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_221):
> I worked a lot in international companies and their apps had non-english data. When you do your "awesome" tricks to sort list of this kind of data it looks okay, sometimes because there're just a few strings for that moment. Maybe it looks okay cause you don't know that language's alphabet.
Use correct one to be sure that it's sorted by alphabetical order for that language.

> For example. Deutsche alphabet

```
// Wrong
["a", "z", "ä"].sort((a, b) => a - b);
// ['a', 'z', 'ä']

// Correct
["a", "z", "ä"].sort((a, b) => a.localeCompare(b));
// [ 'a', 'ä', 'z' ]
```


```
let items = ['réservé', 'Premier', 'Cliché', 'communiqué', 'café', 'Adieu'];
items.sort( (a, b) => a.localeCompare(b, 'fr', {ignorePunctuation: true})); 
// ['Adieu', 'café', 'Cliché', 'communiqué', 'Premier', 'réservé']
```

How it works: 

* Negative when the referenceStr occurs before compareString
* Positive when the referenceStr occurs after compareString
* Returns 0 if they are equivalent

Note: not -1, 0, 1! Different browsers implement it differently. 


More Resources: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare
