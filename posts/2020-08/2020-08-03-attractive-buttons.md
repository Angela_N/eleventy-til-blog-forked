---
title: TIL Of these attractive buttons
date: 2020-08-03
published: true
tags: ['webdev', 'embed', 'css', 'codepen']
series: false
canonical_url: false
description: "Attractive"
layout: layouts/post.njk
---

So nice!

<p class="codepen" data-height="432" data-theme-id="dark" data-default-tab="result" data-user="toaster99" data-slug-hash="BpgzQR" style="height: 432px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="FontAwesome Icon Transition Hover Animation: Pure CSS">
  <span>See the Pen <a href="https://codepen.io/toaster99/pen/BpgzQR">
  FontAwesome Icon Transition Hover Animation: Pure CSS</a> by Alexander White (<a href="https://codepen.io/toaster99">@toaster99</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

Via [codepen](https://codepen.io/toaster99/pen/BpgzQR)


Another batch of really nice buttons.
Via [dev.to/duomly](
https://dev.to/duomly/8-amazing-html-button-hover-effects-that-will-make-your-website-memorable-386e)
The neon hover is so freakin cool.

<style>
#neon-btn {
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 100vh;
  background: #031628;
}

.btn {
  border: 1px solid;
  background-color: transparent;
  text-transform: uppercase;
  font-size: 14px;
  padding: 10px 20px;
  font-weight: light;
}

.one {
  color: #4cc9f0;
}

.two {
  color: #f038ff;
}

.three {
  color: #b9e769;
}

.btn:hover {
  color: white;
  border: 0;
}

.one:hover {
  background-color: #4cc9f0;
  -webkit-box-shadow: 10px 10px 99px 6px rgba(76,201,240,1);
  -moz-box-shadow: 10px 10px 99px 6px rgba(76,201,240,1);
  box-shadow: 10px 10px 99px 6px rgba(76,201,240,1);
}

.two:hover {
  background-color: #f038ff;
  -webkit-box-shadow: 10px 10px 99px 6px rgba(240, 56, 255, 1);
  -moz-box-shadow: 10px 10px 99px 6px rgba(240, 56, 255, 1);
  box-shadow: 10px 10px 99px 6px rgba(240, 56, 255, 1);
}

.three:hover {
  background-color: #b9e769;
  -webkit-box-shadow: 10px 10px 99px 6px rgba(185, 231, 105, 1);
  -moz-box-shadow: 10px 10px 99px 6px rgba(185, 231, 105, 1);
  box-shadow: 10px 10px 99px 6px rgba(185, 231, 105, 1);
}
</style>

<div id="neon-btn">
  <button class="btn one">Hover me</button>
  <button  class="btn two">Hover me</button>
  <button  class="btn three">Hover me</button>
</div>
