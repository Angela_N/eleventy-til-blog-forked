---
title: TIL how to use CSS to count things
date: 2020-08-30
published: true
tags: ["html", "css", "fe-masters-css-in-depth", 'codepen']
series: false
canonical_url: false
description: "Using CSS Counter, counter-reset, and counter-increment."
---

Using CSS Counter, counter-reset, and counter-increment.

```html
<h1>Testing the Counter</h1>
<ul>
  <li>ITEM 1</li>
  <li class="invalid">ITEM 2 - is invalid</li>
  <li>ITEM 3</li>
    <li class="invalid">ITEM 4 - is invalid</li>
    <li class="invalid">ITEM 5 - is invalid</li>
    <li class="invalid">ITEM 6 - is invalid</li>
</ul>
<p>DETAILS: </p>

<style>
body {
  counter-reset: invalidCount;
}

.invalid {
  background-color: pink;
  counter-increment: invalidCount;
}

p:after {
content: "You have " counter(invalidCount) " invalid entries";
}
</style>
```

EMBED:

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="wvzpbma" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="wvzpbma">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/wvzpbma">
  wvzpbma</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
