---
title: TIL making data prettier
date: 2020-08-05
published: true
tags: ['ui']
series: false
canonical_url: false
description: "Your UI doesn't need to map one-to-one with your data's fields and values."
---


> Don't be afraid to "think outside the database" — your UI doesn't need to map one-to-one with your data's fields and values.

Here are a few ideas you can use to present "field: value" data in a more interesting way:
https://pbs.twimg.com/media/DdZ_wQuX4AA0vrF?format=png&name=orig

<image width="100%" href="/img/2020-08-05-presenting-data.png">

Via refactoringui.com
