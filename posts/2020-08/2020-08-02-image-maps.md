---
title: TIL image maps
date: 2020-08-02
published: true
tags: ['html']
series: false
canonical_url: false
description: "Find the image"
layout: layouts/post.njk
---

<style>

figure.clickable {
    /* display: initial; */
    position: relative;
	width: 100%;
padding-bottom: 49%;
	vertical-align: middle;
	margin: 0;
	overflow: hidden;
}
figure.clickable svg {
	display: inline-block;
	position: absolute;
	top: 0; left: 0;
}
</style>

This is a image map.
<img src="/img/2020-08-02-forest-bg3.jpg" alt="Forest" usemap="#forest">

<map name="forest">
    <area shape="rect" coords="670,387,703,461,698,514,670,538,637,515,631,454,650,395" alt="Sun" id="Sun" onclick="imageMap(this.id)">
</map>

<script>
function imageMap(content) {
    console.log('You selected:', content);
    alert("You have found the person");
}
</script>

The Code
```
<img src="img/2020-08-02-forest-bg3.jpg" alt="Forest" usemap="#forest">

<map name="forest">
    <area shape="rect" coords="670,387,703,461,698,514,670,538,637,515,631,454,650,395" alt="Sun" id="Sun" onclick="imageMap(this.id)">
</map>

function imageMap(content) {
    console.log('You selected:', content);
    alert("You have found the person");
}
```

## Responsive version
This doesn't scale. Go ahead and try it. Instead, turn it into a SVG.


  <figure class="clickable">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 1200 808">
        <image width="100%" xlink:href="/img/2020-08-02-forest-bg3.jpg">
        </image>
        <rect x="535" y="300" fill="#fff" opacity="0.25" width="120" height="200"
            onclick="imageMap(this.id)" />
    </svg>
  </figure>

```
  <figure class="clickable">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 1200 808">
        <image width="100%" xlink:href="assets/images/forest-bg.jpg">
        </image>
        <rect x="535" y="300" fill="#fff" opacity="0.25" width="120" height="200"
            onclick="imageMap(this.id)" />
    </svg>
  </figure>
```

References:
[Image map generator](https://www.image-map.net/)
[howto](http://thenewcode.com/760/Create-A-Responsive-Imagemap-With-SVG)
