---
title: TIL reverting back to master after a wild rabbit hole
date: 2020-08-25
published: true
tags: ['git']
series: false
canonical_url: false
description: "Sometimes, you go off in the deep end with your repo and you want everything back to how it was on master."
---

Sometimes, you go off in the deep end with your repo and you want everything back to how it was on master.
Here's how:

1. git pull

2. git fetch --all

3. git reset --hard origin/master

4. git clean -d -x -f

This removes all the unstaged files.
(warning: using the -x flag will also cause Git to delete ignored files -- right back to remote master)



