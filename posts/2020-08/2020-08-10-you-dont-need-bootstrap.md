---
title: TIL You don't need bootstrap
date: 2020-08-10
published: true
tags: ['css']
series: false
canonical_url: false
description: "Bootstrap is big. Maybe you don't need it"
---
> Bootstrap v4 minified is 160 KB of CSS + 60 KB of JS + 71 KB of jQuery + 21 KB of Popper.js for a total of 312 KB.

For reference:
> Materialize is 142 KB of CSS + 181 KB of JS = 323 KB.
> Foundation is 145 KB of CSS + 503 KB of JS + 272 KB of jQuery = 916 KB.

That's huge.

If you just need a few basics --
Below are a bunch of recipes of Bootstrap-styled css.

Example: need box sizing?
```
html {
  box-sizing: border-box;
}
*,
*:before,
*:after {
  box-sizing: inherit;
}
```

Want the bootstrap styled cards?
```
.card {
  /* This flex basis and max-width of 18rem is arbitrary. */
  flex: 1 0 18rem;
  max-width: 18rem;
  border: 1px solid #ddd;
  border-radius: 0.25rem;
  /* This margin spacing is also arbitrary. */
  margin: 1rem 1rem 0 0;
}
.card-content {
  padding: 1rem;
}
.card-media {
  width: 100%;
}
.card-title {
  font-size: 1.125rem;
  font-weight: 500;
  line-height: 1.15;
}
.card-link {
  font-weight: 500;
  font-size: 1rem;
  padding: 0.125rem 0;
  margin: 0 1rem 0 0;
  letter-spacing: 0.025rem;
  display: inline-block;
}
.card-link:hover,
.card-link:focus {
  text-decoration: none;
  /* Darker blue */
  color: #125ece;
}
.card-action {
  margin: 0.25rem 1rem 0 0;
}
```

Via [https://github.com/davidhartsough/you-dont-need-bootstrap](https://github.com/davidhartsough/you-dont-need-bootstrap)
