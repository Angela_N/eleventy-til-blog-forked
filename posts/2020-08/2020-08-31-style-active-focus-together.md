---
title: TIL about styling active/focus together
date: 2020-08-31
published: true
tags: ["html", "css", "fe-masters-css-in-depth"]
series: false
canonical_url: false
description: "Style active and focus at the same time. Some people get to your link or selection by keyboard."
---

In the course --
Style `:active` and `:focus` at the same time. Some people get to your link or selection by keyboard.

I'm not exactly sure of the benefits, and couldn't find real rules of thumb for that.

But it does seem like a good idea.

I then went into a rabbit hole of research about the topics:

:active Adds a style to an element that is activated
:focus Adds a style to an element that has keyboard input focus

References:

- https://stackoverflow.com/questions/1677990/what-is-the-difference-between-focus-and-active

- https://alligator.io/css/hover-active-pseudo-classes/
