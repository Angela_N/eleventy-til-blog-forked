---
title: TIL Cloud9 and pulling down a private repo
date: 2020-08-13
published: true
tags: ['aws', 'ide']
series: false
canonical_url: false
description: "Using a online IDE Cloud9"
---

I'm currently coding on a chromebook, using AWS Cloud9.

Chromebooks are criminally underpowered and even with sideloading linux, doesn't have the processing power to do any real coding.

AWS Cloud9 provides a cloud-based IDE so you can code directly in the browser. (Like I am doing right now.)

Let's get started:
1. Log into your AWS Account and [follow these instructions to setting up Cloud9](https://docs.aws.amazon.com/cloud9/latest/user-guide/create-environment-main.html)
2. To pull your private repo, you need to get the SSH key of your cloud9 instance.
3. In the Cloud9 terminal --> ` ssh-keygen -t rsa`
4. Stick with defaults.
5. When it's finished, it'll tell you where it was saved.


For example:
```
Your identification has been saved in /home/ec2-user/.ssh/id_rsa.
Your public key has been saved in /home/ec2-user/.ssh/id_rsa.pub.
```

6. Visit that directory (for me it was `/home/ec2-user/.ssh`)
7. Type `cat id_rsa.pub` to get the public key.
8. Visit your github or [bitbucket profile](https://bitbucket.org/account/settings/ssh-keys/) and add it to the keys.

Via https://medium.com/sonabstudios/setting-up-github-on-aws-cloud9-with-ssh-2545c4f989ea

