---
title: TIL using underscores to separate numbers
date: 2020-08-22
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "using underscores to separate numbers"
---


```
let budget = 1000000000;

// It's hard for the human eye to detect what is the actual order in here.

// But, by using the Numerical Separators, it can be rewritten in the following way:

let budget = 1_000_000_000;
```

Via [https://dev.to/shubhambattoo/numeric-separators-p4b](https://dev.to/shubhambattoo/numeric-separators-p4b)

Check the caniuse:
[https://caniuse.com/mdn-javascript_grammar_numeric_separators](https://caniuse.com/mdn-javascript_grammar_numeric_separators)
