---
title: TIL Microcopy
date: 2020-08-18
published: true
tags: ['accessibility']
series: false
canonical_url: false
description: "Password vs Choose Password"
---

I never really think about these things.

`Password` vs `Choose Password`

> MICROCOPY
> The label is set to “Choose password” rather than “Password.” The latter is somewhat confusing and could prompt the user to type a password they already possess, which could be a security issue. More subtly, it might suggest the user is already registered, causing users with cognitive impairments to think they are logging in instead. Where “Password” is ambiguous, “Choose password” provides clarity.


pg 45 - From [Form Design Patterns](https://www.smashingmagazine.com/2018/10/form-design-patterns-release/)
