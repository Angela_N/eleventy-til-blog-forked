---
title: TIL Wordpress guesses URLs
date: 2020-08-24
published: true
tags: ['wordpress']
series: false
canonical_url: false
description: "If you write the URL of a page in a WordPress site incorrectly, WordPress will try to guess what page you were trying to access and “fix” your request so that you get the proper page and not a 404 error."
---

> The other day I was working on one of my projects and I encountered an unexpected behavior: if you write the URL of a page in a WordPress site incorrectly, WordPress will try to guess what page you were trying to access and “fix” your request so that you get the proper page and not a 404 error.

WordPress tries to guess the URL if you typed it in wrong.

> Is it good or bad that WordPress tries to fix a URL that does not exist instead of returning a 404? According to [bug #16557](https://core.trac.wordpress.org/ticket/16557) reported 9 years ago, there should be a filter to deactivate this behavior if the user wishes so. But although we have a patch for it, for some reason it never made it into core so… what if you wanted to deactivate this behavior?

To solve it - follow the link:
[https://neliosoftware.com/blog/devtips-how-to-stop-wordpress-from-guessing-urls/](https://neliosoftware.com/blog/devtips-how-to-stop-wordpress-from-guessing-urls/)

