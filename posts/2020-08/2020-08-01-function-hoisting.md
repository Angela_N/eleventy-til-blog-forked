---
title: TIL function hoisting in JS
date: 2020-08-01
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Hoisting is cray"
layout: layouts/post.njk
---

We know hoisting creates unintended side effect within JS.

It's why we switch from `var` to `let` & `const`.
There's also side efects with functions.

Via [Type of NaN - A quiz system to test your JS](https://quiz.typeofnan.dev/hoisting-race-to-the-top/)

Consider the following declarations and expression. What gets logged?

## Example 1:
```
immaBeOnTop();

var immaBeOnTop;

function immaBeOnTop() {
  console.log('first');
}

immaBeOnTop = function() {
  console.log('second');
};
```

**result:**
```
first
```

1. The `var` is hoisted.
2. The `function immaBEOnTop()` is hoisted.
3. Then we start going down the list of commands.
4. We then run the `immaBeOnTop();`

## Example 2:
```
spitNumbers();

function spitNumbers() {
  console.log("first");
}

var spitNumbers = function() {
  console.log("second");
};

function spitNumbers() {
  console.log("third");
}

spitNumbers();
```

**result:**
```
third
second
```

1. the `first` gets hoisted.
2. The `third` gets hoisted, and overrides.
3. `spitNumbers();` is called, and returns `third`.
4. We then move down to the next function, `var spitNumbers = function()`. That overrides the result.
5. `spitNumbers();` is called again, and returns `second`.

