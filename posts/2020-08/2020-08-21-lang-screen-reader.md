---
title: TIL the html lang affects the screen reader voice
date: 2020-08-21
published: true
tags: ['html', 'accessibility']
series: false
canonical_url: false
description: "<html> element's lang attribute tells it which language it is written in. "
---

> `<html>` element's lang attribute tells it which language it is written in.

```
<html lang="en"> <!-- language set to English -->
```

> That is, if `<html lang="en">` is present but the text is actually in French, you'd hear a voice profile called Jack doing a bad impression of French, rather than a French profile called Jaques using authentic French pronunciation.  It is possible to switch languages within a page using the lang attribute on child elements within the `<body>`.

> For instance, I may want to quote some French within an English language page:

```
<blockquote lang="fr" > <p >Ceci n'est pas une pipe</p > <p >— <cite >René Magritte</cite > </p > </blockquote >
```

From [Form Design Patterns](https://www.smashingmagazine.com/2018/10/form-design-patterns-release/)

