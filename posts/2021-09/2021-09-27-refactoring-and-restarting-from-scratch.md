---
title: TIL about massive re-writes vs write-in-place
date: 2021-09-27
published: true
tags: ['lessonlearned', 'devstories']
series: false
canonical_url: false
description: "There's two schools of thinking when it comes to rebuilding a project."
layout: layouts/post.njk
---

There's two schools of thinking when it comes to rebuilding a project.

THINKING 1:
> Netscape 6.0 is finally going into its first public beta. There never was a version 5.0. The last major release, version 4.0, was released almost three years ago. Three years is an awfully long time in the Internet world. During this time, Netscape sat by, helplessly, as their market share plummeted.

> Well, yes. They did. They did it by making the single worst strategic mistake that any software company can make:

> They decided to rewrite the code from scratch.

[Via Mitch Joel - 2000](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/)

This school of thinking, make minor refactors and slow changes. Big revamps are bad.

THINKING 2:

> At the beginning of this massive rewrite, it was my job as lead developer on the project to determine what all needed to be done. We were given an order saying "take this 18-year-old project and rewrite it using modern technology and methodology." True to that order, I immediately dove into the existing codebase and attempted to determine what all it comprised of. There were sections for a library of documents, sections for an employee lookup, sections for links to other tools in our ecosystem. I found all the sections, wrote up an estimate, got it approved by our business unit, and my team was off to the races.

This school of thinking, isolate everything and rebuild from scratch.

[Via Matthew Jones - 2017](https://exceptionnotfound.net/technical-debt-and-the-rewrite-in-place/)
## Thinking 2 coming around to thinking 1

> But I missed one. I missed a section, and the panicked decisions that resulted from that miss nearly killed the project.

> The section that I missed was not very visible. You could liken it to a land mine: concealed and explosive. It was only accessible to a few people, a few very important people, a few people who were responsible for millions of dollars in revenue to our company. You had to have very specific permissions to even see this part of the tool. I didn't, but I also didn't look in the code for this section. I have no explanation as to why I found all the other sections we needed and not this one; I just missed it.

And here's where it gets interesting:

> I directed my team to do a rewrite-in-place.

> "What's that?" you ask. A rewrite-in-place is a well-known term I just made up that says we should copy the code from the old application to the rewritten one, fix it as little as possible to work in the new architecture, and then call it good enough.


## Maybe Mitch was right

A article from 2000, and then the same issue happening in 2017.

