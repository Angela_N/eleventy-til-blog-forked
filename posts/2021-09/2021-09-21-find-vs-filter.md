---
title: TIL find() vs filter()
date: 2021-09-21
published: true
tags: ['javascript', 'arrays']
series: false
canonical_url: false
description: "`find()` method return the first value which match from the collection. `filter()` method returns the match value from the array."
layout: layouts/post.njk
---

`find()` method return the first value which match from the collection. `find()` method search the array from the start if the desired value is matched then `find()` method return that value and terminate and rest of the array is not process.

`filter()` method returns the match value from the array. it search whole array from start to end and returns all the value which is matched.


<p class="codepen" data-height="300" data-slug-hash="vYZMPgr" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/vYZMPgr">
  find() vs filter()</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


REFERENCE:
`filter()` method returns the match value from the array.
