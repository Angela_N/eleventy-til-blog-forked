---
title: TIL about how to update Raspberry Pi
date: 2021-09-19
published: false
tags: ['ssh', 'raspberrypi']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


1. Log into the raspberry pi

PS C:\> ssh pi@192.168.1.xxx
If you haven't changed your default password, it's `raspberry`.


2. Update the OS
```
sudo apt update
sudo apt full-upgrade
```

3. From there, you can update your pihole!

Related to: [SSH Pi Hole Update](https://til.heyitsrocky.com/posts/2021-05/2021-05-30-ssh-pi-hole-update/)

4. Finally, reboot if you do any updates.
```
sudo reboot
```



[Raspberry pi Documentation](https://www.raspberrypi.org/documentation/computers/os.html)
