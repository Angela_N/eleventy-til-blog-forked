---
title: TIL how a dev cut GTA Online load times by 70%
date: 2021-09-28
published: true
tags: ['gamedev', 'optimization', 'videogames']
series: false
canonical_url: false
description: "In all, t0st estimates that 1,984,531,500 checks take place (nearly two billion!), again taking up a ton of CPU time."
layout: layouts/post.njk
---

So: after struggling through a six minute load for GTA Online on his mid-range PC, t0st opened Task Manager the next time he loaded up the game and noticed something odd: after the one minute mark, his computer's CPU usage spiked up dramatically, but storage and network usage were nearly non-existent. That suggested that the lengthy load times weren't caused by Rockstar's servers or data being read off a drive - instead, something was running on the CPU. Something that required a ton of processing to complete, and only used a single thread.

Armed with this knowledge, t0st used a series of programming and debugging tools to discover two major issues.

First, the game was reading in a text file of all purchasable items in the game - and after every one of 63,000 items, it counts every character in the 10MB text file afresh. Doing this count once is no big deal, but doing it 63,000 times adds up to a whole lot of wasted CPU time.

Second, to prepare all of the item data that's been read in, the game records both the data associated with that item (eg its name, price, category, stats) and a hash of that item (essentially a calculated 'fingerprint' that uniquely identifies it). Each time the game stores an item from the list - which, remember, happens 63,000 times - it checks the hash value of the item being stored against the hash value of every other item that's already been stored.

At first, this doesn't take much time, but as the number of items successfully loaded into the game increases, this check takes longer and longer and longer. In all, t0st estimates that 1,984,531,500 checks take place (nearly two billion!), again taking up a ton of CPU time. The game does this to make sure that there are no duplicate items in the final list, but given that the list is completely empty to start with and the file being loaded in has no duplicates, the check is essentially pointless.

https://www.eurogamer.net/articles/digitalfoundry-2021-03-01-clever-programmer-cuts-gta-online-load-times-by-70-percent



[The source](https://nee.lv/2021/02/28/How-I-cut-GTA-Online-loading-times-by-70/)
