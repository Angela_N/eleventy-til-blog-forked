---
title: TIL about Constraint Validation API (forms)
date: 2021-09-11
published: true
tags: ['forms', 'html', 'css', 'mdn']
series: false
canonical_url: false
description: "Inputs are pretty powerful. Right out of the box, they can autocomplete, validate data, create minimum/maximum lengths, allow for patterns, and even provide state!"
layout: layouts/post.njk
---

Form inputs are interesting. There's a lot of mechanisms working really hard behind the scenes.

Inputs are pretty powerful. Right out of the box, they can autocomplete, validate data, create minimum/maximum lengths, allow for patterns, and even provide state!

Modern browsers have the ability to check that these constraints are being observed by users, and can warn them when those rules have been breached. This is known as the [Constraint Validation API](https://developer.mozilla.org/en-US/docs/Web/API/Constraint_validation)

Right out of the box - this works:
```
input::placeholder {  color: blue; }
input:invalid { color: red; }
input:valid { color: green; }
input:enabled { color: black; }
```

Via [The Complete Guide to HTML Forms and Constraint Validation](https://www.sitepoint.com/html-forms-constraint-validation-complete-guide/#cssvalidationstyling)

