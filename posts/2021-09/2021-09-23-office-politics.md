---
title: TIL dealing with office politics
date: 2021-09-23
published: true
tags: ['job', 'officepolitics', 'comments', 'reddit']
series: false
canonical_url: false
description: "One of the things not talked about often is office politics. Not just about identifying issues with tasks, but also figuring out how to handle issues within coworkers/team dynamics."
layout: layouts/post.njk
---


One of the things not talked about often is office politics. Not just about identifying issues with tasks, but also figuring out how to handle issues within coworkers/team dynamics.

Below are a few comments that I really enjoyed:

## Dealing with a percieved 'superstar'

> The last one I dealt with was actually a train wreck of broken builds and untested code commits. He would not do any of the work assigned to him but pick&choose any interesting features assigned to others. He would get it far enough across the line working weird hours that the rest of the team would be put under the gun to "just get it working". Half the time it was a ticket assigned to you that he decided to steal & fumble then hand the broken code back to finish.

> We finally put him on his own branch & let him code away for 6+ months until he had mangled his own code base so badly he couldn't get it running. Nor figure out how to merge to/from the rest of the teams work. The whole time he was convinced he was the protagonist working on the strategic rewrite to save the team. Did I mention he worked remotely, skipped meetings and refused to participate in L2/L3 rota?

> The team had burned down from a peak of 6 to just 3 - him remote, and me&peer in-office. The only way to finally end it was to go directly 2 levels up in management and present the story to him as - "you have a developer who is producing a net negative in terms of hours of work he does causing hours of work for other teammates, you can keep him if you want, but we will both leave, so you will have 0 people in-office and on L2/L3 rota as well". We also explained our containment method above and that the guy was basically being paid to code into a dead end now, but that at least was net-zero as opposed to causing the remaining 2 devs to have to do re-work.

[Via sjg284](https://www.reddit.com/r/ExperiencedDevs/comments/pslxda/comment/hdqrsvv/?utm_source=share&utm_medium=web2x&context=3)

## How to deal with a bad lead developer

> Last week he wrote a comment on other dev's PR: "why are you asking if you should use this? this is your PR, you're not here to question other developers...you should know what you're doing". And that was too much! Other developers say that this is just his personality, but I'm pretty sure they're afraid of saying something and things get bad for them somehow...

> Anyway, how to handle a baaaad lead developer? I'm thinking about leave the job and find something else...I went to the hospital last sunday because of stress, not because of my tasks, but he makes me really nervous and I had, for the first time in my life, an anxiety episode.

> Edit: Guys, thank you so much for all the messages and comments! It’s so sad that this happens more often than it should. Yesterday I talked to the CEO and he’ll talk to the guy, also, another team mate called me and said he’s close to leave the company! A few months ago other developer left us because of the same reason. Also, yesterday was a big day and we delivered something big, the CEO messaged me and told me how good my job was, that I should be proud…I said thank you and mentioned the whole situation. Let’s see :) But this morning I had a GREAT interview with another company 🤘🏻

[Via xchapstickx](https://www.reddit.com/r/ExperiencedDevs/comments/pslxda/how_to_handle_a_bad_lead_developer/)

## Solutions to dealing with a bad lead dev

> Bring it up to the CEO that the guy is an excellent engineer but a terrible lead. Suggests that they bring in someone else to manage the team (and him) while he can focus on his strengths as a principal engineer.

> Frame everything in terms of the company's growth. Say the lead is strong now but he is preventing others from growing and scaling the organization.

> It is important to make sure everyone else on your team feels the same way as you. If you are the only one complaining, they are not going to listen.

[Via mobjack](https://www.reddit.com/r/ExperiencedDevs/comments/pslxda/comment/hdqq2ei/?utm_source=share&utm_medium=web2x&context=3)


## Fixing problems behind the scenes

> But in my case this "bad lead developer" is one of the co-founders, and he is also, with me, the responsible for the whole backend and infrastructure of the startup. He doesn't have a "normal" schedule, works always during the night, deploys stuff that "makes sense for him" and sometimes crashes things in production, without my notice. In the morning, I receive the failure reports from the users, and have to fix the shit he did while I was sleeping. It's tough, man...

The response from ipainhpr

> Don't just quickly fix it. Make up some reason why you need his "help" to get it fixed. Allow users to suffer downtime. The users and the business will only care if they notice the pain. When you scramble to make everything OK they don't notice, and so won't care. If the company loses money as a result, it's his fault.

[Via nakatapt and ipainhpr](https://www.reddit.com/r/ExperiencedDevs/comments/pslxda/comment/hdrmlpm/?utm_source=share&utm_medium=web2x&context=3)


## Dealing with absolute emergencies

> The original quote: "Lack of planning on your part does not constitute an emergency on my part"

> Adjusted to your situation: "Failure to report issues when they happen does not constitute an emergency now because these same errors you've had for weeks now prevent you from working


[Via jeffery_f](https://www.reddit.com/r/sysadmin/comments/pstxuv/comment/hdrzs1p/?utm_source=share&utm_medium=web2x&context=3)

## If you f-up, be thoughtful about it

> Always had slight annoyance when someone said "while you work on this I'm going to go grab a coffee next door" just in case I needed them to log in or something

> but it completely changed when they finished the sentence with "what can I get you while I'm there?". Take your time! Also random gift cards and thank you notes were always nice.

[Via Gardobus](https://www.reddit.com/r/sysadmin/comments/pstxuv/comment/hdujztp/?utm_source=share&utm_medium=web2x&context=3)




