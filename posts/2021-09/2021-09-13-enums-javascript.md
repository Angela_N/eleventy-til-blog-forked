---
title: TIL using Object.freeze to simulate enums in Javascript
date: 2021-09-13
published: true
tags: ['javascript', 'datatype', 'objects']
series: false
canonical_url: false
description: "Enums are useful to assign names to the integral constants which makes a program easy to read and maintain. The keyword 'enum is used to declare an enumeration."
layout: layouts/post.njk
---

Enums are useful to assign names to the integral constants which makes a program easy to read and maintain. The keyword “enum” is used to declare an enumeration.

[Enum in C](https://www.tutorialspoint.com/enum-in-c#:~:text=Enumeration%20is%20a%20user%20defined,used%20to%20declare%20an%20enumeration.&text=The%20enum%20keyword%20is%20also,the%20variables%20of%20enum%20type.)

Javascript doesn't have native support for Enums.

But you can simulate it with `Object.freeze`

```js
const Direction = Object.freeze{
  Up: 'Up',
  Down: 'Down',
  Left: 'Left',
  Right: 'Right'
};
```

Via [3 TypeScript Tricks You Can Use in JavaScript](https://dev.to/masteringjs/3-typescript-tricks-you-can-use-in-javascript-1m75)
