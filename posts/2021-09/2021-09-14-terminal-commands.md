---
title: TIL how to see if terminal commands exist
date: 2021-09-14
published: true
tags: ['terminal', 'commands']
series: false
canonical_url: false
description: "Use the 'command -v' to see if the command exists. For example: '$ command -v git'"
layout: layouts/post.njk
---

Use the `command -v` to see if the command exists.

For example:

`$ command -v nvm`
nvm


`$ command -v git`
/usr/bin/git
