---
title: TIL not using the delete keyword for performance issues
date: 2021-09-09
published: true
tags: ['javascript', 'objects', 'performance']
series: false
canonical_url: false
description: "The delete keyword is used to remove a property from an object. Apparently, there is performance issues with `delete` with V8 Javascript Engine."
layout: layouts/post.njk
---

The delete keyword is used to remove a property from an object.

Apparently, there is performance issues with `delete` with V8 Javascript Engine. [Issue1](https://github.com/googleapis/google-api-nodejs-client/issues/375) and [issue2](https://stackoverflow.com/questions/43594092/slow-delete-of-object-properties-in-js-in-v8/44008788)


It's faster to just declare it as undefined.

```js
const object = {name:"Jane Doe", age:43};
object.age = undefined;
```

Via https://www.joyk.com/dig/detail/1594756891396192
