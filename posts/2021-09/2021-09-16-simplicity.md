---
title: TIL that sometimes, simplicity is best
date: 2021-09-16
published: true
tags: ['kiss', 'meme']
series: false
canonical_url: false
description: "As developers, we want to write clean code and readable code and functioning code and code that just works."
layout: layouts/post.njk
---

As developers, we want to write clean code and readable code and functioning code and code that just works.

![](https://i.imgur.com/Sp7YG66.png)
