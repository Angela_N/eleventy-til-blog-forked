---
title: TIL the benefits of standup
date: 2021-09-01
published: true
tags: ['agile', 'dev']
series: false
canonical_url: false
description: "Standups are actually useful for keeping an eye on the newbies."
layout: layouts/post.njk
---


There was a great post shared on r/programming recently.
(Software development topics I've changed my mind on after 6 years in the industry)[https://chriskiehl.com/article/thoughts-after-6-years]

This one stood out.

> Standups are actually useful for keeping an eye on the newbies.

The comments on reddit:
> Once we stop robotically announcing our task and started opening up about bottlenecks and issues, the juniors started doing the same and being a lot more transparent about their tasks.

> Standup is also GREAT at deconflicting peoples availability or giving people a heads up on what you need early so they can plan it into their day instead of being surprised later

Via Reddit and the blog post:
https://www.reddit.com/r/programming/comments/pdjnfr/software_development_topics_ive_changed_my_mind/
