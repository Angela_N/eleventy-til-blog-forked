---
title: TIL Technical Writing
date: 2021-09-20
published: true
tags: ['writing', 'dev', 'teaching']
series: false
canonical_url: false
description: "The goal of good technical writing is to explain in clear terms what a API is about, or how this piece of software works."
layout: layouts/post.njk
---

In a forum, I shared a free online class about Technical Writing from Google.
https://developers.google.com/tech-writing


A writer asked:
> TW course from Google say it's for "professional software engineers, computer science students, engineering-adjacent roles, such as product managers" which led me to go a different direction. Would you say it's a good course to take if you don't fall into one of those categories?

My Response:

From my side of things as a developer - technical writing is writing content for manuals/documentation. Like, the goal of good technical writing is to explain in clear terms what a API is about, or how this piece of software works. And the audience/styling could be highly technical like [EcmaScript Language Specs](https://262.ecma-international.org/5.1/) or much more down to each like the [MDN JavaScript Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

I took blogging classes years ago and done freelancing in writing/content marketing/book writing. There's a difference in writing styles between the two (content like blogs and newsletters are more promotional, persuasive and casual, like 'hey buddy check me out' or 'buy this product'. Or it has a casual "5 things to know about .map") That style doesn't work in documentation.

> Would you say it's a good course to take if you don't fall into one of those categories?

I think you need some sort of subject matter familiarity.

You don't have to be a master at the technology. Chances are, you'd work with the developer to understand it, then turn that information into something digestible for others. Look at the [React Docs](https://reactjs.org/docs/getting-started.html) as an example - for example [this Get Started Page](https://reactjs.org/docs/getting-started.html) (and it's [commit history](https://github.com/reactjs/reactjs.org/commits/main/content/docs/getting-started.md)). Much of the writing was started by Dan Abramov, a React developer. Then others came up to tighten up the language or make it more user-friendly. But I can't imagine a technical writer without basic React experience being able to come in and cleaning up the docs.
