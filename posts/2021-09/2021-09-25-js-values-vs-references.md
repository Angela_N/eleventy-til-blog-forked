---
title: TIL how Javascript passes data by value, not by reference
date: 2021-09-25
published: true
tags: ['javascript', 'primitives', 'gif']
series: false
canonical_url: false
description: "In JavaScript, primitive types like Number or String are passed by value, not by reference."
layout: layouts/post.njk
---

In JavaScript, primitive types like Number or String are passed by value, not by reference.

![](https://i.imgur.com/tctsqMn.gif)

If you want to keep the behavior unified across different data types in JavaScript, wrapping values inside an object.

Objects pass by reference. (Which creates a different problem when you're cloning objects!)
