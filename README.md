# Rocky's TIL
In web development, it's impossible to learn everything. But, just try to be 1% better every day.

> If you get one percent better each day for one year, you'll end up thirty-seven times better by the time you’re done. ~James Clear

![](img/tiny-gains-graph.jpg)
via [James Clear](https://jamesclear.com/continuous-improvement)


This blog is a example of that.

Every single day I'm surprised by something new.

Sometimes, it's something I learned a decade ago, but it surprises me again.

So I document it.


## Other sites with TIL:
This was inspired by [Hashrocket's TIL](https://til.hashrocket.com/).

[Go Make Things](https://gomakethings.com/articles/), another one, which started in January 19, 2011!

## Todos

[x] - Make header not suck.
[x] - Make footer also not suck.

### Design
```
[X] - [Design] fix the code block EBT-1
[X] - [Design] Center text so it's more like 'medium' looking or confluence looking.
[X] - [Design] Clean up Next/Previous pages
[] - [Design] Add a favicon
[] - [Design] Headers are ugly and chonky right now.
[] - [Design] New title! make the title h1 page real fancy. Maybe Loki intro?
[] - [Design] Fix these weird tag breaking. https://i.imgur.com/HkF0ryH.png
```

### Functionality
```
[x] - [Functionality] Add a tag cloud on the front page.
[X] - [Functionality] Make the front page show full content.
[] - [Functionality] When clicking on a page, add 'below-content' of similar tagged elements.
[] - [Functionality] When clicking on a page, add 'below-content' of series.
[] - [Functionality] Create a sales page.
[] - [Functionality] Add a contact us.
[] - [Functionality] Add a tag cloud in the center of content. (So 50 posts, tag cloud, 50 posts)
[] - [Functionality] Add a way to like pages.
[] - [Functionality] Add a way to track page views.
[] - [Functionality] Add it so the front page is broken into tags/categories.
[] - [Functionality] Headers should create a link
[] - [Functionality] Add a how long will it take to read bit. [reference 1]
[] - [Functionality] fetch and [cache speed score.](https://www.zachleat.com/web/lighthouse-in-footer/)
[] - [Functionality] Subscribe newsletter would be sweet but then I need a RSS feed.
[] - [Functionality] Add Google Analytics
```

### Bugs/Quality of Life
```
[x] - [bug] Figure out a way to solve the 'title' issue.
[x] - [bug] Footer should be 2019-2021
[x] - [bug] Change the date
[x] - [bug] The tag system is broken! :-(
[x] - [qol] move img/css to a /src folder.
[] - [bug] Tag count isn't correct. ([Example 1](https://i.imgur.com/1DlT5Dt.png))
[] - [bug] Should run a spell check on these `posts/`
[] - [bug] Change url (/posts/2021-08/2021-08-22-terminal-commands-on-one-page/) -> (/posts/2021-08/terminal-commands-on-one-page/)
[] - [qol] Import graphics into the site, rather than via imgur
[] - [qol] Add a Cypress testing library to check all links.
[] - [qol] Replace images to this https://www.11ty.dev/docs/plugins/image/
[x] - [qol] Replace all `js` with `javascript`
[] - [qol] add codepen tag
[] - [qol] Add more tags to everything.
```

### Issues references
1: look into
  https://www.11ty.dev/docs/quicktips/inline-js/
  and
  https://www.rebeccagracedesigns.com/blog/read-more-blog-arrow

## What is this built off of

This blog is build using [Eleventy](https://github.com/11ty/eleventy), a lightweight, framework-free static site generator. [![Build Status](https://travis-ci.org/11ty/eleventy-base-blog.svg?branch=master)](https://travis-ci.org/11ty/eleventy-base-blog)


### To Build Locally

Build and host locally for local development
```
npm run serve

// server method
npx eleventy --serve
```

Build automatically when a template changes:
```
npm run watch

// server method
npx eleventy --watch
```

Or in debug mode (Only if eleventy is acting up):
More info: https://www.11ty.dev/docs/debugging/
```
FOR WINDOWS:
npm run win-debug
```

### To Build for production

This currently lives on `til.heyitsrocky.com` using Netlify.

How it works is that the code on bitbucket gets piped to Netlify, which then runs the build command to generate the website.
Netlify hosts the code, site, for free.

### Random Issues and workarounds

#### YAMLException was thrown
```
`YAMLException` was thrown:
    YAMLException: can not read a block mapping entry; a multiline key may not be an implicit key at line 9, column 7:
        layout: layouts/post.njk
              ^
        at generateError (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:167:10)
        at throwError (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:173:9)
        at readBlockMapping (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1073:9)
        at composeNode (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1359:12)
        at readDocument (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1525:3)
        at loadDocuments (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1588:5)
        at load (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1614:19)
        at Object.safeLoad (G:\Development\eleventy-til-blog\node_modules\js-yaml\lib\js-yaml\loader.js:1637:10)
        at module.exports (G:\Development\eleventy-til-blog\node_modules\gray-matter\lib\parse.js:12:17)
        at parseMatter (G:\Development\eleventy-til-blog\node_modules\gray-matter\index.js:109:17)
Wrote 0 files in 1.67 seconds (v0.12.1)
```

This means that there's something wrong with the front matter.

```
title: TIL how Atari's Pitfall generates over 255 unique rooms
date: 2021-05-08
published: true
tags: ['gaming']
series: false
canonical_url: false
description: "How it was done was by allowing code to generate the room, rather than mapping the room out and saving the data."
layout: layouts/post.njk
```

Solution: https://github.com/11ty/eleventy/issues/1156

### Issues with

1. Must be lowercase. `ux` NOT `UX`
2. No spaces.
### Implementation Notes

Eleventy is a JS framework that turns `.md` files into a static website.
It uses [Nunjucks, a templating language for JS](https://mozilla.github.io/nunjucks/)

**Tutorials**

1. [Let's Learn Eleventy](https://www.netlify.com/blog/2020/04/09/lets-learn-eleventy-boost-your-jamstack-skills-with-11ty/)

2. [Eleventy Walkthrough](https://rphunt.github.io/eleventy-walkthrough/local-server.html)


**Content:**

* Use underscore to make it private, like `_name.md`. It will not build. To make it public, `name.md`

* `posts/` has the blog posts but really they can live in any directory. They need only the `post` tag to be added to this collection.

* Content can be any template format (blog posts needn't be markdown, for example). Configure your supported templates in `.eleventy.js` -> `templateFormats`.

* Because `css` and `png` are listed in `templateFormats` but are not supported template types, any files with these extensions will be copied without modification to the output (while keeping the same directory structure).

**Templates:**

* Add the `nav` tag to add a template to the top level site navigation. For example, this is in use on `index.njk` and `about/index.md`.

**Design:**

* `/css/[files]` is where css files live.

They are then imported within a `/_includes/layouts`

```html
    <link rel="stylesheet" href="{{ '/css/stripey.css' | url }}">
    <link rel="stylesheet" href="{{ '/css/header.css' | url }}">
    <link rel="stylesheet" href="{{ '/css/support.css' | url }}">
    <link rel="stylesheet" href="{{ '/css/prism-base16-monokai.dark.css' | url }}">
```


* The blog post feed template is in `feed/feed.njk`. This is also a good example of using a global data files in that it uses `_data/metadata.json`.

* This example uses three layouts:
  * `_includes/layouts/base.njk`: the top level HTML structure
  * `_includes/layouts/home.njk`: the home page template (wrapped into `base.njk`)
  * `_includes/layouts/post.njk`: the blog post template (wrapped into `base.njk`)


* `_includes/postlist.njk` is a Nunjucks include and is a reusable component used to display a list of all the posts. `index.njk` has an example of how to use it.

Very nice
